package spirits.lostandfound.controllers.assistant;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Window;
import spirits.lostandfound.database.SpiritsJDBC;
import spirits.lostandfound.models.Address;
import spirits.lostandfound.models.Luggage;
import spirits.lostandfound.models.LuggageLabel;
import spirits.lostandfound.models.Passenger;
import spirits.lostandfound.reference.*;
import spirits.lostandfound.util.LayoutHelper;

import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static spirits.lostandfound.reference.Strings.MAIN_MENU;

/**
 * Controller for the found luggage screen.
 *
 * @author IS102-3
 */
public class FoundLuggageLayout extends AssistantBaseController {

    private static final String CURRENT_SCREEN = Strings.FOUND_LUGGAGE;
    private static final Pattern TIME_REGEX = Pattern.compile("(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]");
    private static final Pattern PHONE_REGEX = Pattern.compile("^(?:0[0-9]{9,})?$");
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");

    @FXML
    private DatePicker datePicker;
    @FXML
    private TextField timeField;
    @FXML
    private ComboBox<Airport> airportComboBox;
    @FXML
    private TextField labelNumberField;
    @FXML
    private TextField labelFlightNumberField;
    @FXML
    private ComboBox<Airport> destinationComboBox;
    @FXML
    private TextField firstNameField;
    @FXML
    private TextField tussenvoegselField;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField phoneNumberField;
    @FXML
    private TextField emailField;
    @FXML
    private TextField streetField;
    @FXML
    private TextField zipcodeField;
    @FXML
    private TextField houseNumberField;
    @FXML
    private TextField cityField;
    @FXML
    private TextField countryField;
    @FXML
    private ComboBox<LuggageType> luggageTypeComboBox;
    @FXML
    private TextField brandField;
    @FXML
    private ComboBox<Color> colorComboBox;
    @FXML
    private TextArea specialField;

    /**
     * Found luggage is registered in the database
     *
     * @param actionEvent the event triggered by pressing the register button.
     */
    public void registerFoundLuggage(ActionEvent actionEvent) {
        boolean time = validateInput(TIME_REGEX, timeField);
        boolean email = validateEmail(emailField);
        boolean phoneNumber = validateInput(PHONE_REGEX, phoneNumberField);
        boolean color = validateColorBox();
        if (!time || !email || !phoneNumber || !color) {
            return;
        }
        Window window = timeField.getScene().getWindow();
        ResourceBundle resourceBundle = getResourceBundle();
        Optional<ButtonType> result = LayoutHelper.createDoubleActionPopup(resourceBundle.getString("popup_register_found_luggage_title"), resourceBundle.getString("popup_register_found_luggage_message"), window);
        if (result.isPresent() && result.get() == ButtonType.OK) {
            try {
                SpiritsJDBC jdbc = new SpiritsJDBC();

                long luggageId = addLuggage(jdbc);
                addLabel(luggageId, jdbc);
                if (hasPassenger()) {
                    long passengerId = addPassenger(jdbc);
                    long addressId = addAddress(jdbc);
                    jdbc.createAddressLink(passengerId, addressId, Instant.now(), null);
                    jdbc.createLuggageLink(luggageId, passengerId);
                }

                LayoutHelper.createSingleActionPopup(resourceBundle.getString("popup_tracking_nr_title"), String.valueOf(luggageId), window);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            openScreen(actionEvent, Layouts.SEARCH_LUGGAGE);
        }
    }

    /**
     * Validates information in the colorbox
     *
     * @return is it valid or not
     */
    private boolean validateColorBox() {
        if (colorComboBox.getValue() == Color.STANDARD) {
            setError(colorComboBox);
            return false;
        } else {
            clearError(colorComboBox);
            return true;
        }
    }

    /**
     * Adds luggage information to database
     *
     * @param jdbc {@link SpiritsJDBC} instance to use.
     * @return the luggageID for this luggage
     * @throws SQLException thrown when something goes wrong while inserting into the database.
     */
    private long addLuggage(SpiritsJDBC jdbc) throws SQLException {
        LuggageType luggageType = luggageTypeComboBox.getValue();
        String brand = getTextOrNull(brandField);
        Color color = colorComboBox.getValue();
        String airport = airportComboBox.getValue().getCode();
        String extra = getTextOrNull(specialField);
        Date lostSince = Date.valueOf(datePicker.getValue());
        return jdbc.insertLuggage(new Luggage(luggageType, brand, color, airport, extra, lostSince, true));
    }

    /**
     * Adds label information to database
     *
     * @param luggageId ID of luggage
     * @param jdbc      The database
     * @throws SQLException thrown when something goes wrong while inserting into the database.
     */
    private void addLabel(long luggageId, SpiritsJDBC jdbc) throws SQLException {
        String labelNumber = getTextOrNull(labelNumberField);
        if (labelNumber == null) {
            return;
        }
        String flightNumber = getTextOrNull(labelFlightNumberField);
        String destination = destinationComboBox.getValue().getCode();
        jdbc.insertLabel(new LuggageLabel(labelNumber, luggageId, flightNumber, destination));
    }

    /**
     * Check if there is enough information to identify a passenger.
     *
     * @return true if there is enough contact information to contact the passenger.
     */
    private boolean hasPassenger() {
        return !emailField.getText().isEmpty() || !phoneNumberField.getText().isEmpty() ||
                !zipcodeField.getText().isEmpty() && !houseNumberField.getText().isEmpty() && !countryField.getText().isEmpty() ||
                !streetField.getText().isEmpty() && !cityField.getText().isEmpty() && !countryField.getText().isEmpty() && !houseNumberField.getText().isEmpty();
    }

    /**
     * Adds passenger information to database
     *
     * @param jdbc {@link SpiritsJDBC} instance to use.
     * @return the passengerID for this passenger.
     * @throws SQLException thrown when something goes wrong while inserting into the database.
     */
    private long addPassenger(SpiritsJDBC jdbc) throws SQLException {
        String firstName = getTextOrNull(firstNameField);
        String tussenvoegsel = getTextOrNull(tussenvoegselField);
        String lastName = getTextOrNull(lastNameField);
        String phoneNumber = getTextOrNull(phoneNumberField);
        String email = getTextOrNull(emailField);
        return jdbc.insertPassenger(new Passenger(firstName, tussenvoegsel, lastName, phoneNumber, email));
    }

    /**
     * Adds address to database
     *
     * @param jdbc {@link SpiritsJDBC} instance to use.
     * @return adding addresses in to the database
     * @throws SQLException thrown when something goes wrong while inserting into the database.
     */
    private long addAddress(SpiritsJDBC jdbc) throws SQLException {
        String street = getTextOrNull(streetField);
        String zipcode = getTextOrNull(zipcodeField);
        String houseNumber = getTextOrNull(houseNumberField);
        String city = getTextOrNull(cityField);
        String country = getTextOrNull(countryField);
        return jdbc.insertAddress(new Address(street, zipcode, houseNumber, city, country));
    }

    /**
     * @param inputControl The input control to get text from.
     * @return A string or null when the string is empty.
     */
    private static String getTextOrNull(TextInputControl inputControl) {
        return inputControl.getText().isEmpty() ? null : inputControl.getText();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        setBreadCrumbs(resources, MAIN_MENU, getCurrentScreen());
        Airport localAirport = getCurrentUser().getAirport();

        LocalDateTime localDateTime = LocalDateTime.now();
        datePicker.setValue(localDateTime.toLocalDate());
        timeField.setText(localDateTime.format(TIME_FORMATTER));

        List<Airport> airports = Arrays.stream(Airport.values()).filter(d -> d != Airport.STANDARD).collect(Collectors.toList());
        airportComboBox.setItems(FXCollections.observableArrayList(airports));
        destinationComboBox.setItems(FXCollections.observableArrayList(airports));
        luggageTypeComboBox.setItems(FXCollections.observableArrayList(LuggageType.values()));
        colorComboBox.setItems(FXCollections.observableArrayList(Color.values()));

        luggageTypeComboBox.getItems().sort((o1, o2) -> {
            if (o1 == LuggageType.STANDARD) return -1;
            else return o1.toString().compareToIgnoreCase(o2.toString());
        });
        colorComboBox.getItems().sort((o1, o2) -> {
            if (o1 == Color.STANDARD) return -1;
            else return o1.toString().compareToIgnoreCase(o2.toString());
        });

        airportComboBox.setValue(localAirport);
        destinationComboBox.setValue(localAirport);
        luggageTypeComboBox.setValue(LuggageType.STANDARD);
        colorComboBox.setValue(Color.STANDARD);
        colorComboBox.valueProperty().addListener(o -> validateColorBox());
        timeField.focusedProperty().addListener(o -> validateInput(TIME_REGEX, timeField));
    }

    @Override
    protected boolean validateEmail(TextInputControl eMailField) {
        return emailField.getText().isEmpty() || super.validateEmail(eMailField);
    }

    @Override
    protected String getCurrentScreen() {
        return CURRENT_SCREEN;
    }


}
