package spirits.lostandfound.models;

/**
 * Stores a person.
 *
 * @author IS102-3
 */
public abstract class Person {
    protected final String firstName;
    protected final String tussenvoegsel;
    protected final String lastName;
    protected final String email;

    /**
     * A method for making a person.
     *
     * @param firstName     the first name of the person.
     * @param tussenvoegsel the insertion of the person.
     * @param lastName      the last name of the person.
     * @param eMail         the email of the person.
     */
    protected Person(String firstName, String tussenvoegsel, String lastName, String eMail) {
        this.email = eMail;
        this.firstName = firstName;
        this.lastName = lastName;
        this.tussenvoegsel = tussenvoegsel;
    }

    /**
     * Returns a name that only ever has one space between parts.
     *
     * @return name of this person.
     */
    public String getName() {
        return String.format("%s %s %s", getStringOrSpace(firstName), getStringOrSpace(tussenvoegsel), getStringOrSpace(lastName)).trim().replace("  ", " ");
    }

    /**
     * @param s
     * @return
     */
    private String getStringOrSpace(String s) {
        return s == null ? " " : s;
    }

    /**
     * getter for the email of the person.
     *
     * @return email of the person.
     */
    public String getEmail() {
        return email;
    }

    /**
     * getter for the first name of the person.
     *
     * @return first name of the person.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * getter for the insertion of the person.
     *
     * @return insertion of the person.
     */
    public String getTussenvoegsel() {
        return tussenvoegsel;
    }

    /**
     * getter for the last name of the person.
     *
     * @return last name of the person.
     */
    public String getLastName() {
        return lastName;
    }
}
