package spirits.lostandfound.models;

import spirits.lostandfound.reference.Airport;
import spirits.lostandfound.reference.UserType;

/**
 * Stores a user.
 *
 * @author IS102-3
 */
public class User extends Person {

    private static final long DEFAULT_ID = -1L;
    private final long id;
    private final String employeeNr;
    private final Airport airport;
    private final UserType type;

    /**
     * a method for constructing a user with an userID.
     *
     * @param id            the id of the user.
     * @param firstName     frist name of the user.
     * @param tussenvoegsel th insertion of the user.
     * @param lastName      the last name of the user.
     * @param email         the email of the user.
     * @param airport       the airport where the user works.
     * @param type          the function of the user.
     * @param employeeNr    the employee number of the user.
     */
    public User(Long id, String firstName, String tussenvoegsel, String lastName, String email, Airport airport, UserType type, String employeeNr) {
        super(firstName, tussenvoegsel, lastName, email);
        this.id = id;
        this.airport = airport;
        this.type = type;
        this.employeeNr = employeeNr;
    }

    /**
     * a method for constructing a user.
     *
     * @param firstName     first name of the user.
     * @param tussenvoegsel the insertion of the user.
     * @param lastName      the last name of the user.
     * @param airport       the airport where the user works.
     * @param type          the function of the user.
     */
    public User(String firstName, String tussenvoegsel, String lastName, Airport airport, UserType type) {
        this(DEFAULT_ID, firstName, tussenvoegsel, lastName, "", airport, type, "");
    }

    /**
     * Getter for the userID.
     *
     * @return userID.
     */
    public long getId() {
        return id;
    }

    /**
     * Getter for the employee number of the user.
     *
     * @return employee number of the user.
     */
    public String getEmployeeNr() {
        return employeeNr;
    }

    /**
     * Getter for the airport where the user works.
     *
     * @return airport where the user works.
     */
    public Airport getAirport() {
        return airport;
    }

    /**
     * Getter for the function of the user.
     *
     * @return function of the user.
     */
    public UserType getType() {
        return type;
    }
}
