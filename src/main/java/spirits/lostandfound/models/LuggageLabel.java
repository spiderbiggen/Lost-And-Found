package spirits.lostandfound.models;

/**
 * Stores a luggage label.
 *
 * @author IS102-3
 */
public class LuggageLabel {
    private String labelNumber;
    private long luggageId;
    private String flightNumber;
    private String destination;

    /**
     * A  method to make a luggage label.
     *
     * @param labelNumber  the number of the label
     * @param luggageId    the id of the linked luggage
     * @param flightNumber the flight number of the flight the luggage belongs to.
     * @param destination  the destination of the linked luggage.
     */
    public LuggageLabel(String labelNumber, long luggageId, String flightNumber, String destination) {
        this.labelNumber = labelNumber;
        this.luggageId = luggageId;
        this.flightNumber = flightNumber;
        this.destination = destination;
    }

    /**
     * Getter for the label number.
     *
     * @return the label number
     */
    public String getLabelNumber() {
        return labelNumber;
    }

    /**
     * getter for the id of the linked luggage.
     *
     * @return the id of the linked luggage.
     */
    public long getLuggageId() {
        return luggageId;
    }

    /**
     * getter for the flight number of the flight the luggage belongs to.
     *
     * @return the flight number of the flight the luggage belongs to.
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * getter for the destination of the linked luggage.
     *
     * @return the destination of the linked luggage.
     */
    public String getDestination() {
        return destination;
    }
}
