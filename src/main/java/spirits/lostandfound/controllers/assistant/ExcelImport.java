package spirits.lostandfound.controllers.assistant;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import spirits.lostandfound.database.SpiritsJDBC;
import spirits.lostandfound.models.Luggage;
import spirits.lostandfound.models.LuggageLabel;
import spirits.lostandfound.reference.Color;
import spirits.lostandfound.reference.LuggageType;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.poi.ss.usermodel.Row.MissingCellPolicy.CREATE_NULL_AS_BLANK;

/**
 * Class to import excel into the database.
 *
 * @author IS102-3
 */
public final class ExcelImport {

    private static final int FOUND_TRUE = 1;
    private static final Pattern TUSSENVOEGSEL_PATTERN = Pattern.compile(" (van der|van de|van den|van 't|van het|van|de|der|den) | ", Pattern.CASE_INSENSITIVE);

    /**
     * Default constructor for reading and saving ex cel into the database
     *
     * @param file reference to the selected file.
     */
    private ExcelImport(File file) throws ParseException {
        List<List<XSSFCell>> dataHolder = read(file);
        saveToDatabase(dataHolder);
    }

    /**
     * Get the data out of the excel.
     *
     * @param fileName excel file
     * @return the data in the excel
     */
    private static List<List<XSSFCell>> read(File fileName) {
        List<List<XSSFCell>> cellVectorHolder = new ArrayList<>();
        try {
            FileInputStream myInput = new FileInputStream(fileName);
            XSSFWorkbook myWorkBook = new XSSFWorkbook(myInput);
            XSSFSheet mySheet = myWorkBook.getSheetAt(0);

            List<XSSFCell> vector = new ArrayList<>();
            vector.add(mySheet.getRow(1).getCell(1));
            cellVectorHolder.add(vector);
            int i = 4;
            if (mySheet.getRow(i - 1).getPhysicalNumberOfCells() < 14) {
                i++;
            }
            int rowLength = mySheet.getPhysicalNumberOfRows();
            for (; i < rowLength - 1; i++) {
                try {
                    XSSFRow myRow = mySheet.getRow(i);
                    List<XSSFCell> cellStoreVector = new ArrayList<>();
                    cellStoreVector.add(myRow.getCell(1, CREATE_NULL_AS_BLANK));
                    cellStoreVector.add(myRow.getCell(3, CREATE_NULL_AS_BLANK));
                    cellStoreVector.add(myRow.getCell(4, CREATE_NULL_AS_BLANK));
                    cellStoreVector.add(myRow.getCell(6, CREATE_NULL_AS_BLANK));
                    cellStoreVector.add(myRow.getCell(8, CREATE_NULL_AS_BLANK));
                    cellStoreVector.add(myRow.getCell(12, CREATE_NULL_AS_BLANK));
                    cellStoreVector.add(myRow.getCell(13, CREATE_NULL_AS_BLANK));

                    cellVectorHolder.add(cellStoreVector);
                } catch (NullPointerException e) {
                    System.out.println(i);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cellVectorHolder;
    }

    public static boolean readExcel(File selectedFile) {
        try {
            new ExcelImport(selectedFile);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    /**
     * Saving excel in the database
     */
    private void saveToDatabase(List<List<XSSFCell>> dataHolder) throws ParseException {
        try (Connection connection = new SpiritsJDBC().createConnection()) {
            String iataCode = dataHolder.get(0).get(0).toString();
            //start at 4. The first 4 are information columns and rows
            connection.setAutoCommit(false);
            for (int i = 1; i < dataHolder.size(); i++) {
                Long trackingNr;
                Long passengerId;

                //get the IATA code
                List<XSSFCell> cellStoreVector = dataHolder.get(i);

                //formatting the date
                String st = cellStoreVector.get(0).toString();
                DateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
                //assigning the data from excel to variables
                Date dateFound = new Date(formatter.parse(st).getTime());
                LuggageType luggageType = LuggageType.fromString(cellStoreVector.get(1).toString());
                String brand = cellStoreVector.get(2).toString();

                cellStoreVector.get(3).setCellType(CellType.STRING);
                String label = cellStoreVector.get(3).toString();
                Color color = Color.fromString(cellStoreVector.get(4).toString());
                String passengerName = cellStoreVector.get(5).toString();
                String extra = cellStoreVector.get(6).toString();

                //Insert function
                trackingNr = insertLuggage(connection, new Luggage(luggageType, brand, color, iataCode, extra, dateFound, true));

                insertLabel(connection, new LuggageLabel(label, trackingNr, null, null));
                //if passenger has no name, don't insert
                //also don't link passenger and luggage
                if (!passengerName.isEmpty()) {

                    // Separate passenger name from living place
                    String[] parts = passengerName.split(", ");
                    String name = parts[0];
                    String city = parts[1];

                    // Remove double spaces and remove spaces before dots.
                    name = name.replaceAll(" +", " ").replaceAll("[ .]+\\.", ".");
                    String[] pars = name.split(TUSSENVOEGSEL_PATTERN.pattern());
                    String firstName = pars[0];
                    String lastName = pars[1];
                    String tussenvoegsel = null;

                    Matcher matcher = TUSSENVOEGSEL_PATTERN.matcher(name);
                    if (matcher.find()) {
                        tussenvoegsel = matcher.group(1);
                    }

                    //insert the passengers
                    passengerId = insertPassenger(connection, firstName, tussenvoegsel, lastName, city, dateFound);

                    //insert to link luggage and passenger
                    insertLuggagelink(connection, trackingNr, passengerId);
                    connection.commit();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Insert the excel data containing luggage informaton into the luggage table
     *
     * @param connection database
     */
    private Long insertLuggage(Connection connection, Luggage luggage) {
        Long luggageId = null;
        try (Statement statement = connection.createStatement()) {

            final String sql = "INSERT INTO luggage "
                    + "(lost_since, type, brand, color, extra, found, airport) "
                    + " VALUES ('" + luggage.getLostSince().toString() + "', '" + luggage.getType().name() + "',"
                    + "'" + luggage.getBrand() + "', '" + luggage.getColor().name() + "', '" + luggage.getExtra() + "', '" + FOUND_TRUE + "', '" + luggage.getAirport() + "');";

            statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    luggageId = generatedKeys.getLong(1);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return luggageId;
    }

    /**
     * Insert the excel data containing label informaton into the label table
     *
     * @param connection with database
     */
    private void insertLabel(Connection connection, LuggageLabel label) {
        if (label.getLabelNumber().isEmpty()) {
            return;
        }
        try (Statement statement = connection.createStatement()) {

            final String sql = "INSERT INTO labels "
                    + "(labelNr, tracking_nr) "
                    + " VALUES ('" + label.getLabelNumber() + "', " + label.getLuggageId() + ");";

            statement.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Insert the excel data containing passenger information into the passenger table
     *
     * @param connection    with database
     * @param firstName     a first name
     * @param tussenvoegsel a tussenvoegsel
     * @param lastName      a last name
     * @param city          a city
     * @param dateFound     a date
     */
    private Long insertPassenger(Connection connection, String firstName, String tussenvoegsel, String lastName, String city, Date dateFound) {
        //insert passenger info to passenger tables
        final String sql = "INSERT INTO passengers "
                + "(first_name, tussenvoegsel, last_name)"
                + " VALUES (?,?,?);";
        Long passengerId = null;
        try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, firstName);
            statement.setString(2, tussenvoegsel);
            statement.setString(3, lastName);

            statement.executeUpdate();
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    passengerId = generatedKeys.getLong(1);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        insertAddress(connection, passengerId, city, dateFound);
        return passengerId;
    }

    /**
     * Insert the excel data containing address information into the address table and link it to a passenger.
     *
     * @param connection  with database
     * @param passengerId the id of a passenger in the database
     * @param city        the city where the passenger lives.
     * @param dateFound   a date
     */
    private void insertAddress(Connection connection, Long passengerId, String city, Date dateFound) {
        if (passengerId == null) return;
        Long addressId = null;
        final String addressSql = "INSERT INTO addresses (city) VALUES (?);";
        try (PreparedStatement statement = connection.prepareStatement(addressSql, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, city);
            statement.executeUpdate();
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    addressId = generatedKeys.getLong(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (addressId == null) return;
        final String addressLinkSql = "INSERT INTO addresslink (passenger_id, address_id, from_date) VALUES (?, ?, ?);";
        try (PreparedStatement statement = connection.prepareStatement(addressLinkSql)) {
            statement.setLong(1, passengerId);
            statement.setLong(2, addressId);
            statement.setDate(3, dateFound);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Insert the excel data that is important
     * to be able to link passenger and luggage
     *
     * @param connection with the database
     */
    private void insertLuggagelink(Connection connection, Long trackingNr, Long passengerId) {
        if (trackingNr == null || passengerId == null) return;
        try (Statement statement = connection.createStatement()) {
            final String sql5 = "INSERT INTO luggagelink "
                    + "(tracking_nr, passenger_id) "
                    + " VALUES ('" + trackingNr + "', '" + passengerId + "');";

            statement.executeUpdate(sql5);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
