package spirits.lostandfound.util;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.Window;
import spirits.lostandfound.controllers.BaseController;
import spirits.lostandfound.reference.Airport;
import spirits.lostandfound.reference.Color;
import spirits.lostandfound.reference.LuggageType;
import spirits.lostandfound.reference.UserType;

import java.io.IOException;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * The methods in this class can be used to open screens in several ways.
 * The goal is to reduce duplicate code in other classes.
 *
 * @author Stefan Breetveld
 */
public final class LayoutHelper {

    private static final double DEFAULT_WIDTH = 900;
    private static final double DEFAULT_HEIGHT = 600;
    private static final double TITLE_BAR_OFFSET = 40;
    private static final String DEFAULT_TITLE = "Lost and Found";
    private static final Image IMAGE_ICON = new Image(LayoutHelper.class.getResourceAsStream("/images/koffer.png"));

    /**
     * The constructor of this class is private, because there are only static methods in this class.
     */
    private LayoutHelper() {
        // Only static methods
    }

    /**
     * Create a new scene with the default height, width and title.
     *
     * @param fxml      the path to the fxml file to be loaded.
     * @param mainStage the stage.
     * @return the controller associated with this view.
     * @throws IOException possible exception thrown by loading the fxml file
     * @see #createStageFromFxml(String, Stage, double, double, ResourceBundle, String)
     */
    public static BaseController createStageFromFxml(String fxml, Stage mainStage) throws IOException {
        ResourceBundle bundle = createResourceBundleForLocale(PropertiesHelper.getLanguage().getLocale());
        return createStageFromFxml(fxml, mainStage, DEFAULT_WIDTH, DEFAULT_HEIGHT, bundle, DEFAULT_TITLE);
    }

    /**
     * Create a resourceBundle for usage throughout the program.
     *
     * @param locale The desired locale.
     * @return A Resource bundle containing strings in the given locale.
     */
    public static ResourceBundle createResourceBundleForLocale(Locale locale) {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("localization/language", locale);
        UserType.setResourceBundle(resourceBundle);
        LuggageType.setResourceBundle(resourceBundle);
        Color.setResourceBundle(resourceBundle);
        Airport.setResourceBundle(resourceBundle);
        return resourceBundle;
    }

    /**
     * Create a new scene.
     *
     * @param fxml      the path to the fxml to be loaded.
     * @param mainStage the stage.
     * @param width     the width of the scene.
     * @param height    the height of the scene.
     * @param bundle    the language of the program.
     * @param title     the window title.
     * @return the controller associated with this view.
     * @throws IOException possible exception thrown by loading the fxml file
     */
    private static BaseController createStageFromFxml(String fxml, Stage mainStage, double width, double height, ResourceBundle bundle, String title) throws IOException {
        FXMLLoader loader = new FXMLLoader(LayoutHelper.class.getResource(fxml), bundle);
        Parent root = loader.load();
        BaseController controller = loader.getController();
        mainStage.setTitle(title);
        mainStage.setScene(new Scene(root, width, height));
        mainStage.setMinHeight(DEFAULT_HEIGHT + TITLE_BAR_OFFSET);
        mainStage.setMinWidth(DEFAULT_WIDTH);
        mainStage.getIcons().add(IMAGE_ICON);
        mainStage.show();
        return controller;
    }

    /**
     * Create a new scene with default title and width and height of the oldScene.
     *
     * @param fxml      the path to the fxml file to be loaded.
     * @param mainStage the stage.
     * @param oldScene  the old scene, used to get width and height.
     * @param bundle    the language of the program.
     * @return the controller associated with this view.
     * @throws IOException possible exception thrown by loading the fxml file
     * @see #createStageFromFxml(String, Stage, double, double, ResourceBundle, String)
     */
    public static BaseController createStageFromFxml(String fxml, Stage mainStage, Scene oldScene, ResourceBundle bundle) throws IOException {
        return createStageFromFxml(fxml, mainStage, oldScene.getWidth(), oldScene.getHeight(), bundle, DEFAULT_TITLE);
    }

    /**
     * Create an Information {@link Alert}.
     *
     * @param title   message title to be shown in the alert.
     * @param message the message to be shown
     * @param stage   the parent Stage.
     * @return optional response of the alert.
     */
    public static Optional<ButtonType> createSingleActionPopup(String title, String message, Window stage) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initOwner(stage);
        alert.setTitle(DEFAULT_TITLE);
        alert.setHeaderText(title);
        alert.setContentText(message);
        return alert.showAndWait();
    }

    /**
     * Create a Confirmation {@link Alert}.
     *
     * @param title   message title to be shown in the alert.
     * @param message the message to be shown
     * @param stage   the parent Stage.
     * @return optional response of the alert.
     */
    public static Optional<ButtonType> createDoubleActionPopup(String title, String message, Window stage) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.initOwner(stage);
        alert.setTitle(DEFAULT_TITLE);
        alert.setHeaderText(title);
        alert.setContentText(message);
        return alert.showAndWait();
    }

    /**
     * Create an Error {@link Alert}.
     *
     * @param title   message title to be shown in the alert.
     * @param message the message to be shown
     * @param stage   the parent Stage.
     * @return optional response of the alert.
     */
    public static Optional<ButtonType> createErrorPopup(String title, String message, Window stage) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initOwner(stage);
        alert.setTitle(DEFAULT_TITLE);
        alert.setHeaderText(title);
        alert.setContentText(message);
        return alert.showAndWait();
    }
}
