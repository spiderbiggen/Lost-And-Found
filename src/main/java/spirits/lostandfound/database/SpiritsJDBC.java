package spirits.lostandfound.database;

import spirits.lostandfound.models.*;
import spirits.lostandfound.reference.Airport;
import spirits.lostandfound.reference.Color;
import spirits.lostandfound.reference.LuggageType;
import spirits.lostandfound.reference.UserType;
import spirits.lostandfound.util.PropertiesHelper;
import spirits.lostandfound.util.SecurityHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.sql.Date;
import java.time.Instant;
import java.util.*;

/**
 * Handles Connection to the database and commonly used database queries.
 *
 * @author IS102-3
 */
public class SpiritsJDBC {

    private static final int SECONDS_TO_MILLIS = 1000;

    private static final String DATABASE_SERVER_URL_KEY = "database_server_url";
    private static final String DATABASE_NAME_KEY = "database_name";
    private static final String DATABASE_USERNAME_KEY = "database_username";
    private static final String DATABASE_PASSWORD_KEY = "database_password";
    private static final String DATABASE_SCRIPT_KEY = "database_script";

    private final static String DB_DRIVER_URL = "com.mysql.jdbc.Driver";
    private final static String DB_DRIVER_PREFIX = "jdbc:mysql://";
    private final static String DB_DRIVER_PARAMETERS = "?useSSL=false&characterEncoding=utf8";

    private final String dbName;
    private final String serverURL;
    private final String account;
    private final String password;

    /**
     * Constructor with default values.
     *
     * @see #SpiritsJDBC(String, String, String, String)
     */
    public SpiritsJDBC() {
        Properties settings = PropertiesHelper.getSettings();
        this.dbName = settings.getProperty(DATABASE_NAME_KEY);
        this.serverURL = settings.getProperty(DATABASE_SERVER_URL_KEY);
        this.account = settings.getProperty(DATABASE_USERNAME_KEY);
        this.password = settings.getProperty(DATABASE_PASSWORD_KEY);
    }

    /**
     * Constructor.
     *
     * @param dbName    The database to connect to.
     * @param serverURL The url where this database can be accessed.
     * @param account   The username to get access to the database.
     * @param password  The corresponding password.
     */
    public SpiritsJDBC(String dbName, String serverURL, String account, String password) {
        this.dbName = dbName;
        this.serverURL = serverURL;
        this.account = account;
        this.password = password;
    }

    /**
     * If the database does not yet exist create it and add default users.
     */
    public void initDB() {
        createSchema();
        //Can only load internal files
        String scriptName = PropertiesHelper.getSettings().getProperty(DATABASE_SCRIPT_KEY);
        List<String> querys = parseScript(getClass().getResourceAsStream(scriptName));
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            for (String query : querys) {
                statement.addBatch(query);
            }
            statement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Finished Creating database");
    }

    /**
     * If the database Schema does not yet exist create it.
     */
    private void createSchema() {
        try (Connection connection = createConnection(DB_DRIVER_PREFIX + serverURL + "/" + DB_DRIVER_PARAMETERS);
             Statement statement = connection.createStatement()) {
            statement.executeUpdate("CREATE SCHEMA IF NOT EXISTS " + dbName + ";");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Read a properly formatted sql script.
     *
     * @param stream the stream that contains the script.
     * @return a list of queries.
     */
    private List<String> parseScript(InputStream stream) {
        List<String> querys = new ArrayList<>();
        try (BufferedReader dbScript = new BufferedReader(new InputStreamReader(stream))) {
            String line = dbScript.readLine();
            StringBuilder query = new StringBuilder();
            while (line != null) {
                if (!line.trim().startsWith("#") && !line.trim().startsWith("--") && !line.isEmpty()) {
                    query.append(line).append(System.lineSeparator());
                    if (line.trim().endsWith(";")) {
                        querys.add(query.toString());
                        query = new StringBuilder();
                    }
                }
                line = dbScript.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return querys;
    }

    /**
     * Create a {@link Connection} to the database with default values.
     *
     * @return {@link Connection} that can be used to execute statements.
     * @throws SQLException When de server can't be accessed.
     * @see #createConnection(String)
     */
    public Connection createConnection() throws SQLException {
        //establish a connection to a named database on a specified server
        String connStr = DB_DRIVER_PREFIX + serverURL + "/" + dbName + DB_DRIVER_PARAMETERS;
        return createConnection(connStr);
    }

    /**
     * Create a {@link Connection}.
     *
     * @param connStr The string that defines the url to connect to.
     * @return {@link Connection} that can be used to execute statements.
     * @throws SQLException When de server can't be accessed.
     */
    private Connection createConnection(String connStr) throws SQLException {
        //verify that a proper JDBC driver has been installed and linked
        if (!selectDriver()) {
            throw new UnsupportedOperationException("Incorrect JDBC Driver");
        }
        return DriverManager.getConnection(connStr, account, password);
    }

    /**
     * Create a {@link Collection} of {@link Luggage} objects that have already been sent.
     *
     * @return A collection of luggage objects.
     * @throws SQLException When something goes wrong while executing the query.
     */
    public Collection<Luggage> getSentLuggage() throws SQLException {
        String sql = "SELECT * FROM luggage LEFT JOIN luggagelink ON luggage.tracking_nr = luggagelink.tracking_nr WHERE sent = TRUE;";
        Collection<Luggage> luggages = new ArrayList<>();
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {

            while (resultSet.next()) {
                long id = resultSet.getLong("tracking_nr");
                LuggageType type = LuggageType.valueOf(resultSet.getString("type"));
                String brand = resultSet.getString("brand");
                Color color = Color.valueOf(resultSet.getString("color"));
                String airport = resultSet.getString("location_found");
                boolean found = resultSet.getBoolean("found");
                luggages.add(new Luggage(id, type, brand, color, airport, null, null, found));
            }
        }
        return luggages;
    }

    /**
     * Return a {@link User} if the email and password match for any given account.
     *
     * @param eMail    the email used to log in.
     * @param password the password used to log in.
     * @return a user if email and password match otherwise returns null.
     * @throws SQLException when something goes wrong while executing the query.
     */
    public User getUser(String eMail, String password) throws SQLException {
        String sql = "SELECT first_name, Tussenvoegsel, last_name, Airport, Function FROM users WHERE EMail = ? AND Password = ?";
        try (Connection connection = createConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            String hashedPassword = SecurityHelper.securePassword(password, eMail);
            statement.setString(1, eMail);
            statement.setString(2, hashedPassword);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    String firstName = resultSet.getString("first_name");
                    String tussenvoegsel = resultSet.getString("tussenvoegsel");
                    String lastName = resultSet.getString("last_name");
                    Airport airport = Airport.fromCode(resultSet.getString("airport"));
                    UserType type = UserType.valueOf(resultSet.getString("function"));
                    return new User(firstName, tussenvoegsel, lastName, airport, type);
                }
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Insert passenger into database.
     *
     * @param passenger the passenger to insert
     * @return the passenger id.
     * @throws SQLException when there is a problem with inserting the passenger.
     */
    public long insertPassenger(Passenger passenger) throws SQLException {
        final String sql = "INSERT INTO passengers (first_name, tussenvoegsel, last_name, phone_number, email) VALUES (?, ?, ?, ?, ?)";
        try (Connection connection = createConnection();
             PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, passenger.getFirstName());
            ps.setString(2, passenger.getTussenvoegsel());
            ps.setString(3, passenger.getLastName());
            ps.setString(4, passenger.getPhoneNumber());
            ps.setString(5, passenger.getEmail());
            ps.executeUpdate();
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getLong(1);
                }
            }
        }
        throw new SQLException("Failed to insert passenger");
    }

    /**
     * Insert address into database.
     *
     * @param address the address to insert.
     * @return the address id.
     * @throws SQLException when there is a problem with inserting the address.
     */
    public long insertAddress(Address address) throws SQLException {
        final String sql = "INSERT INTO addresses (street, house_number, zipcode, city, country) VALUES (?, ?, ?, ?, ?)";
        try (Connection connection = createConnection();
             PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, address.getStreet());
            ps.setString(2, address.getNumber());
            ps.setString(3, address.getZipcode());
            ps.setString(4, address.getCity());
            ps.setString(5, address.getCountry());
            ps.executeUpdate();
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getLong(1);
                }
            }
        }
        throw new SQLException("Failed to insert address");
    }

    /**
     * Insert addresslink into the database.
     *
     * @param passengerId the passenger_id.
     * @param addressId   the address_id.
     * @param from        the date from when this address is valid for the user.
     * @param until       the date that indicates when this date becomes invalid.
     * @throws SQLException when a problem occurs during execution of the query.
     */
    public void createAddressLink(long passengerId, long addressId, Instant from, Instant until) throws SQLException {
        final String sql = "INSERT INTO addresslink (passenger_id, address_id, from_date, until_date) VALUES (?, ?, ?, ?)";
        Date fromDate = new Date(from.getEpochSecond() * SECONDS_TO_MILLIS);
        Date untilDate = until == null ? null : new Date(until.getEpochSecond() * SECONDS_TO_MILLIS);
        try (Connection connection = createConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, passengerId);
            ps.setLong(2, addressId);
            ps.setDate(3, fromDate);
            ps.setDate(4, untilDate);
            ps.executeUpdate();
        }
    }

    /**
     * Insert luggage into database.
     *
     * @param luggage the luggage to insert.
     * @return the luggage id.
     * @throws SQLException when there is a problem with inserting the luggage.
     */
    public long insertLuggage(Luggage luggage) throws SQLException {
        final String sql = "INSERT INTO luggage (type, brand, color, extra, lost_since, found, airport) VALUES (?, ?, ?, ?, ?, ?, ?)";
        try (Connection connection = createConnection();
             PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, luggage.getType().name());
            ps.setString(2, luggage.getBrand());
            ps.setString(3, luggage.getColor().name());
            ps.setString(4, luggage.getExtra());
            ps.setDate(5, luggage.getLostSince());
            ps.setBoolean(6, luggage.isFound());
            ps.setString(7, luggage.getAirport());
            ps.executeUpdate();
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getLong(1);
                }
            }
        }
        throw new SQLException("Failed to insert luggage");
    }

    /**
     * Insert luggagelink into database.
     *
     * @param luggageId   the tracking_nr.
     * @param passengerId the passenger_id
     * @throws SQLException when there is a problem with inserting the luggagelink.
     */
    public void createLuggageLink(long luggageId, long passengerId) throws SQLException {
        final String sql = "INSERT INTO luggagelink (tracking_nr, passenger_id) VALUES (?, ?)";
        try (Connection connection = createConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, luggageId);
            ps.setLong(2, passengerId);
            ps.executeUpdate();
        }
    }

    /**
     * Insert luggageLabel into database.
     *
     * @param luggageLabel the {@link LuggageLabel} to insert.
     * @throws SQLException when there is a problem with inserting the luggageLabel.
     */
    public void insertLabel(LuggageLabel luggageLabel) throws SQLException {
        final String sql = "INSERT INTO labels (labelNr, tracking_nr, flight_nr, destination) VALUES (?, ?, ?, ?)";
        try (Connection connection = createConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, luggageLabel.getLabelNumber());
            ps.setLong(2, luggageLabel.getLuggageId());
            ps.setString(3, luggageLabel.getFlightNumber());
            ps.setString(4, luggageLabel.getDestination());
            ps.executeUpdate();
        }
    }

    /**
     * elects proper loading of the named driver for database connections.
     * This is relevant if there are multiple drivers installed that match the JDBC type
     *
     * @return indicates whether a suitable driver is available
     */
    private Boolean selectDriver() {
        try {
            Class.forName(SpiritsJDBC.DB_DRIVER_URL);
            //Put all non-prefered drivers to the end, such that driver selection hits the first
            Enumeration<Driver> drivers = DriverManager.getDrivers();
            while (drivers.hasMoreElements()) {
                Driver d = drivers.nextElement();
                if (!d.getClass().getName().equals(SpiritsJDBC.DB_DRIVER_URL)) {   // move the driver to the end of the list
                    DriverManager.deregisterDriver(d);
                    DriverManager.registerDriver(d);
                }
            }
        } catch (ClassNotFoundException | SQLException ex) {
            return false;
        }
        return true;
    }
}
