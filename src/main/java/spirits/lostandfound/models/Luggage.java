package spirits.lostandfound.models;

import spirits.lostandfound.reference.Color;
import spirits.lostandfound.reference.LuggageType;

import java.sql.Date;

/**
 * Stores a luggage item.
 *
 * @author IS102-3
 */
public class Luggage {

    private static long highestId = 1;
    private final long trackingId;
    private LuggageType type;
    private String brand;
    private Color color;
    private String airport;
    private String extra;
    private Date lostSince;
    private boolean found;

    /**
     * A  method to make a luggage item.
     *
     * @param type      the type of the item.
     * @param brand     the brand of the item.
     * @param color     the color of the item.
     * @param airport   the airport where the item is.
     * @param extra     extra information about the item.
     * @param lostSince the date the item has been registered as lost.
     * @param found     whether the item has been found or not.
     */
    public Luggage(LuggageType type, String brand, Color color, String airport, String extra, Date lostSince, boolean found) {
        this(highestId++, type, brand, color, airport, extra, lostSince, found);
    }

    /**
     * A  method to make a luggage item with trackingID.
     *
     * @param trackindId The tracking id of the item.
     * @param type       the type of the item.
     * @param brand      the brand of the item.
     * @param color      the color of the item.
     * @param airport    the airport where the item is.
     * @param extra      extra information about the item.
     * @param lostSince  the date the item has been registered as lost.
     * @param found      whether the item has been found or not.
     */
    public Luggage(long trackindId, LuggageType type, String brand, Color color, String airport, String extra, Date lostSince, boolean found) {
        this.trackingId = trackindId;
        this.type = type;
        this.brand = brand;
        this.color = color;
        this.airport = airport;
        this.lostSince = lostSince;
        this.extra = extra;
        this.found = found;
    }

    /**
     * Getter for the trackingID of the item.
     *
     * @return trackingID
     */
    public long getTrackingId() {
        return trackingId;
    }

    /**
     * Getter for the type of the item.
     *
     * @return Type of the item
     */
    public LuggageType getType() {
        return type;
    }

    /**
     * Getter for the brand of the item
     *
     * @return type of the item
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Getter for the color of the item
     *
     * @return Color of the item
     */
    public Color getColor() {
        return color;
    }

    /**
     * Getter for the airport where the item is.
     *
     * @return Type of the item
     */
    public String getAirport() {
        return airport;
    }

    /**
     * A getter that returns whether the item is found or not.
     *
     * @return true if item is found, else false.
     */
    public boolean isFound() {
        return found;
    }

    /**
     * Getter for the extra information of the item
     *
     * @return extra information of the item
     */
    public String getExtra() {
        return extra;
    }

    /**
     * Getter for the date the item got lost
     *
     * @return the date the item got lost.
     */
    public Date getLostSince() {
        return lostSince;
    }
}
