package spirits.lostandfound.reference;

/**
 * This class contains constants used to know which page is selected and what the title of that page is.
 *
 * @author IS102-3
 */
public class Strings {
    public static final String MAIN_MENU = "page_assistant_main_menu";
    public static final String SEND_LUGGAGE = "page_assistant_send_luggage";
    public static final String SEND_LUGGAGE_SEND = "page_assistant_send_new_luggage";
    public static final String FOUND_LUGGAGE = "page_assistant_found_luggage";
    public static final String SEARCH_LUGGAGE = "page_assistant_search_luggage";
    public static final String LOST_LUGGAGE_LIST = "page_assistant_lost_luggage_list";
    public static final String REGISTER_LOST_LUGGAGE = "page_damage_register_lost_luggage";
    public static final String CLAIMS_IN_PROGRESS = "page_damage_claims_in_progress";
    public static final String ROLLENBEHEER = "page_admin_role_management";
    public static final String CREATE_USER = "page_admin_user_create";
    public static final String DELETE_USER = "page_admin_user_delete";
    public static final String EDIT_USER = "page_admin_user_edit";
    public static final String MISSING_LUGGAGE_PIE_CHART = "page_management_pie_chart";
    public static final String LOST_LUGGAGE_MONTH_AND_YEAR = "page_management_bar_charts";
}
