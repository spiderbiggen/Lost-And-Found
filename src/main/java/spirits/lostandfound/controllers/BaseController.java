package spirits.lostandfound.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import org.controlsfx.control.BreadCrumbBar;
import spirits.lostandfound.database.SpiritsJDBC;
import spirits.lostandfound.models.User;
import spirits.lostandfound.reference.Layouts;
import spirits.lostandfound.reference.UserType;
import spirits.lostandfound.util.LayoutHelper;
import spirits.lostandfound.util.LocalizedString;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

/**
 * Less duplicate code
 *
 * @author IS102-3
 */
public abstract class BaseController implements Initializable {

    /**
     * A regex expression to check if emails are valid.
     *
     * @see <a href="https://www.journaldev.com/638/java-email-validation-regex">Here</a>
     */
    private static final Pattern EMAIL_REGEX = Pattern.compile(
            "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
    );

    /**
     * A password must be at least 8 characters long at least one number one
     * lowercase char one uppercase char and one special character which can be
     * any of the following: @#$%^&+=!?.
     */
    private static final Pattern PASSWORD_REGEX = Pattern.compile(
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!?])(?=\\S+$).{8,}$"
    );

    private static User currentUser;

    @FXML
    protected BreadCrumbBar<LocalizedString> breadCrumbs;
    @FXML
    protected Label currentUserLabel;
    private ResourceBundle resourceBundle;

    /**
     * Get the viewport to center the image in the view.
     *
     * @param background the image which need to be scaled
     * @return viewport for image
     */
    protected static Rectangle2D getBackgroundViewport(ImageView background) {
        Image image = background.getImage();
        double width = image.getWidth();
        double height = image.getHeight();
        double yMult = background.getFitHeight() / height;
        double xMult = background.getFitWidth() / width;
        if (yMult > xMult) {
            xMult = xMult / yMult;
            yMult = 1;
        } else {
            yMult = yMult / xMult;
            xMult = 1;
        }
        double sizeX = width * xMult;
        double sizeY = height * yMult;
        if (sizeX <= 0 || sizeY <= 0) return new Rectangle2D(0, 0, 0, 0);
        return new Rectangle2D(width / 2 - sizeX / 2, height / 2 - sizeY / 2, sizeX, sizeY);
    }

    /**
     * @return The currently logged in user.
     */
    protected static User getCurrentUser() {
        return currentUser;
    }

    /**
     * Set The currently logged in user.
     *
     * @param newUser The user that logged in.
     */
    private static void setCurrentUser(User newUser) {
        currentUser = newUser;
    }

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        if (currentUserLabel != null) {
            currentUserLabel.setText(getCurrentUser().getName());
        }
        this.resourceBundle = resourceBundle;
    }

    /**
     * @return The resourceBundle used to initialize this screen.
     */
    public ResourceBundle getResourceBundle() {
        return resourceBundle;
    }

    /**
     * @param resourceBundle The new resource bundle.
     */
    public void setResourceBundle(ResourceBundle resourceBundle) {
        this.resourceBundle = resourceBundle;
    }

    /**
     * validates email addresses
     *
     * @param eMailField the email address that needs to be validated
     * @return true if the email address is validate else false
     */
    protected boolean validateEmail(TextInputControl eMailField) {
        return validateInput(EMAIL_REGEX, eMailField);
    }

    /**
     * validates input
     *
     * @param pattern regex
     * @param field   The to validate input
     * @return true if the input is validate else false
     */
    protected boolean validateInput(Pattern pattern, TextInputControl field) {
        if (pattern.matcher(field.getText()).matches()) {
            clearError(field);
            return true;
        } else {
            setError(field);
            return false;
        }
    }

    /**
     * Removes the error style
     *
     * @param field The screen that needs to have the error style removed
     */
    protected void clearError(Node field) {
        field.getStyleClass().removeAll("error");
    }

    /**
     * Sets the error style
     *
     * @param field the fields in which the error style needs to be set.
     */
    protected void setError(Node field) {
        field.getStyleClass().add("error");
    }

    /**
     * Log out the current user
     *
     * @param actionEvent pushing the logout button
     */
    @FXML
    protected final void logoutAction(ActionEvent actionEvent) {
        setCurrentUser(null);
        openScreen(actionEvent, Layouts.LOGIN);
    }

    /**
     * Open a screen with the {@link Stage} given by this actionEvent with the layout given by the resource.
     *
     * @param actionEvent pushing a button that needs to open a screen
     * @param resource    the path to the fxml file of the screen that needs to be opened
     * @return the controller associated with the new screen.
     */
    protected BaseController openScreen(ActionEvent actionEvent, String resource) {
        if (actionEvent.getSource() instanceof Node) {
            Node node = (Node) actionEvent.getSource();
            return openScreen(node, resource);
        }
        return null;
    }

    /**
     * Open a screen with the {@link Stage} given by this node with the layout given by the resource.
     *
     * @param node     the node from the previous method or the node that has been given
     * @param resource the path to the fxml file of the screen that needs to be opened
     * @return the controller associated with the new screen.
     */
    protected BaseController openScreen(Node node, String resource) {
        try {
            Scene scene = node.getScene();
            Stage stage = (Stage) scene.getWindow();
            return LayoutHelper.createStageFromFxml(resource, stage, scene, resourceBundle);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Set the Breadcrumbs.
     *
     * @param resourceBundle the {@link ResourceBundle} which contains the possible languages
     * @param keys           resource keys
     */
    protected void setBreadCrumbs(ResourceBundle resourceBundle, String... keys) {
        TreeItem<LocalizedString> old = null;
        Locale locale = resourceBundle.getLocale();
        for (String key : keys) {
            TreeItem<LocalizedString> child = new TreeItem<>(new LocalizedString(key, resourceBundle.getString(key), locale));
            if (old != null) {
                old.getChildren().add(child);
            }
            old = child;
        }
        this.breadCrumbs.setSelectedCrumb(old);
        Platform.runLater(() -> this.breadCrumbs.requestFocus());
    }

    /**
     * Send the user to the clicked page
     *
     * @param breadCrumbActionEvent click on a specific of the breadcrumbs
     */
    @FXML
    protected final void followBreadCrumb(BreadCrumbBar.BreadCrumbActionEvent breadCrumbActionEvent) {
        Object object = breadCrumbActionEvent.getSelectedCrumb().getValue();
        if (!(object instanceof LocalizedString)) return;

        LocalizedString selectedCrumb = (LocalizedString) object;
        if (selectedCrumb.getKey().equals(getCurrentScreen())) return;

        String resource = getResource(selectedCrumb);
        if (resource == null || resource.isEmpty()) return;

        openScreen(breadCrumbs, resource);
    }

    /**
     * Returns the name of the current screen.
     *
     * @return name of the current screen.
     */
    protected abstract String getCurrentScreen();

    /**
     * Gives the path to the screen that needs to be opened (breadcrumbs).
     *
     * @param pageTitle the crumb that is clicked
     * @return the path to the fxml file of the screen that needs to be opened
     * @throws IllegalArgumentException when the given crumb not associate with a resource
     */
    protected abstract String getResource(LocalizedString pageTitle);

    /**
     * Validates the password.
     *
     * @param passwordField The field in which the password is filled
     * @return true if password is validate else false
     */
    protected boolean validatePassword(TextInputControl passwordField) {
        return validateInput(PASSWORD_REGEX, passwordField);
    }

    protected UserType login(String email, String password) {
        try {
            User user = new SpiritsJDBC().getUser(email, password);
            if (user != null) {
                setCurrentUser(user);
                return user.getType();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
