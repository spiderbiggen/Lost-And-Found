package spirits.lostandfound.models;

/**
 * Stores a passenger address.
 *
 * @author IS102-3
 */
public class Address {

    private static long highest = 0;
    private final long addressId;
    private String street;
    private String number;
    private String zipcode;
    private String city;
    private String country;

    /**
     * A method to make an adress
     *
     * @param street  The street of the adress
     * @param number  The House number of the adress
     * @param zipcode The zipcode of the adress
     * @param city    The city of the adress
     * @param country the country of the adress.
     */
    public Address(String street, String number, String zipcode, String city, String country) {
        this(++highest, street, number, zipcode, city, country);
    }

    /**
     * A method to make an adress with an adressID.
     *
     * @param addressId The ID of the adress
     * @param street    The street of the adress
     * @param number    The House number of the adress
     * @param zipcode   The zipcode of the adress
     * @param city      The city of the adress
     * @param country   the country of the adress.
     */
    public Address(long addressId, String street, String number, String zipcode, String city, String country) {
        if (highest < addressId) highest = addressId;
        this.addressId = addressId;
        this.street = street;
        this.number = number;
        this.zipcode = zipcode;
        this.city = city;
        this.country = country;
    }

    /**
     * A getter to get the highest adressID.
     *
     * @return the highest adressID
     */
    public static long getHighest() {
        return highest;
    }

    /**
     * A setter for the highest adressID.
     *
     * @param highest the highest adressID.
     */
    public static void setHighest(long highest) {
        Address.highest = highest;
    }

    /**
     * A getter for adressID.
     *
     * @return adressID
     */
    public long getAddressId() {
        return addressId;
    }

    /**
     * A getter for the street of the adress.
     *
     * @return the street of the adress.
     */
    public String getStreet() {
        return street;
    }

    /**
     * A getter for the house number of the adress.
     *
     * @return the house number of the adress.
     */
    public String getNumber() {
        return number;
    }

    /**
     * A getter for the zipcode of the adress.
     *
     * @return the zipcode of the adress.
     */
    public String getZipcode() {
        return zipcode;
    }

    /**
     * A getter for the city of the adress.
     *
     * @return the city of the adress.
     */
    public String getCity() {
        return city;
    }

    /**
     * A getter for the country of the adress.
     *
     * @return the country of the adress.
     */
    public String getCountry() {
        return country;
    }
}
