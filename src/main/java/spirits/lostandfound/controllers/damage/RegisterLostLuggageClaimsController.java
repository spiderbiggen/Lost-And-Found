package spirits.lostandfound.controllers.damage;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import spirits.lostandfound.database.SpiritsJDBC;
import spirits.lostandfound.reference.Color;
import spirits.lostandfound.reference.Layouts;
import spirits.lostandfound.reference.LuggageType;
import spirits.lostandfound.reference.Strings;
import spirits.lostandfound.util.LayoutHelper;

import java.net.URL;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 * This controller allows the register lost luggage claims page to work.
 *
 * @author IS102-3
 */
public class RegisterLostLuggageClaimsController extends DamageBaseController implements Initializable {

    @FXML
    private TextField lostAndFoundID;
    @FXML
    private TextField passengerFirstName;
    @FXML
    private TextField passengerLastName;
    @FXML
    private TableView<Data> tableView;

    /**
     * allows the user to register lost luggage claims.
     *
     * @param event push button "register"
     */
    public void registerAction(ActionEvent event) {
        ResourceBundle resourceBundle = getResourceBundle();
        if (tableView.getSelectionModel().getSelectedItem() == null) {
            LayoutHelper.createSingleActionPopup(resourceBundle.getString("popup_claim_luggage_nothing_selected_title"), resourceBundle.getString("popup_claim_luggage_nothing_selected_message"), tableView.getScene().getWindow());
        } else {
            Data selectedItem = tableView.getSelectionModel().getSelectedItem();
            long trackingNrOfSelectedLuggage = selectedItem.getLostAndFounID();
            Optional<ButtonType> result = LayoutHelper.createDoubleActionPopup(resourceBundle.getString("popup_claim_luggage_title"), resourceBundle.getString("popup_claim_luggage_message") + trackingNrOfSelectedLuggage + "?", tableView.getScene().getWindow());
            if (result.isPresent() && result.get() == ButtonType.OK) {
                //language = MySQL
                String sql = "UPDATE luggagelink SET claimed = 1, claimed_date = ? WHERE tracking_nr = ?;";
                try (Connection connection = new SpiritsJDBC().createConnection();
                     PreparedStatement statement = connection.prepareStatement(sql)) {
                    statement.setLong(2, trackingNrOfSelectedLuggage);
                    statement.setObject(1, LocalDateTime.now());
                    statement.executeUpdate();
                    fillTable();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void fillTable() {
        tableView.getItems().clear();
        //language=MySQL
        String sql = "SELECT * FROM labels " +
                "LEFT JOIN luggage ON labels.tracking_nr = luggage.tracking_nr " +
                "LEFT JOIN luggagelink ON labels.tracking_nr = luggagelink.tracking_nr " +
                "LEFT JOIN passengers ON passengers.passenger_id = luggagelink.passenger_id " +
                "WHERE luggagelink.claimed != 1 AND luggage.tracking_nr LIKE CONCAT('%',?,'%') AND first_name LIKE CONCAT('%',?,'%') AND last_name LIKE CONCAT('%',?,'%');";

        try (Connection connection = new SpiritsJDBC().createConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, lostAndFoundID.getText());
            statement.setString(2, passengerFirstName.getText());
            statement.setString(3, passengerLastName.getText());
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    long lostAndFounID = resultSet.getInt("tracking_nr");
                    String firstName = resultSet.getString("first_name");
                    String tussenvoegsel = resultSet.getString("tussenvoegsel");
                    String lastName = resultSet.getString("last_name");
                    String passengerName = firstName + (tussenvoegsel == null ? "" : " " + tussenvoegsel) + " " + lastName;
                    String labelNumber = resultSet.getString("labelNr");
                    String color = resultSet.getString("color");
                    String type = resultSet.getString("type");
                    LocalDate dateLost = resultSet.getDate("lost_since").toLocalDate();
                    Date date = resultSet.getDate("sent_date");
                    long tooLate;
                    if (date != null) {
                        tooLate = date.toLocalDate().toEpochDay() - dateLost.toEpochDay();
                    } else {
                        tooLate = LocalDate.now().toEpochDay() - dateLost.toEpochDay();
                    }
                    tableView.getItems().add(new Data(lostAndFounID, passengerName, labelNumber, color, type, dateLost, null, tooLate));

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (tableView.getItems().isEmpty()) {
            ResourceBundle resourceBundle = getResourceBundle();
            LayoutHelper.createSingleActionPopup(resourceBundle.getString("popup_claim_luggage_no_items_title"),
                    resourceBundle.getString("popup_claim_luggage_no_items_message"), lostAndFoundID.getScene().getWindow());
        }
    }

    /**
     * allows user to search in the registered lost luggage
     *
     * @param event push button "search"
     */
    public void searchAction(ActionEvent event) {
        new Thread(this::fillTable).start();
    }

    /**
     * Opens the claims in progress screen.
     *
     * @param actionEvent push button "to progress"
     */
    public void toProgressButton(ActionEvent actionEvent) {
        openScreen(actionEvent, Layouts.CLAIMS_IN_PROGRESS);
    }


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
        setBreadCrumbs(rb, getCurrentScreen());

        for (TableColumn<Data, ?> tableColumn : tableView.getColumns()) {
            String propertyName = tableColumn.getId();
            if (propertyName != null && !propertyName.isEmpty()) {
                tableColumn.setCellValueFactory(new PropertyValueFactory<>(propertyName));
            }
        }

        tableView.setItems(FXCollections.observableArrayList());
    }

    @Override
    protected String getCurrentScreen() {
        return Strings.REGISTER_LOST_LUGGAGE;
    }

    public static class Data {
        private final long lostAndFounID;
        private final String passengerName;
        private final String labelNumber;
        private final Color color;
        private final LuggageType type;
        private final LocalDate dateLost;
        private final LocalDate dateDelivered;
        private final long timeTooLate;

        /**
         * Create an instance of the data object.
         *
         * @param lostAndFounID this creates an Id
         * @param passengerName this allows you to fill in the passenger name
         * @param labelNumber   this allows you to fill in the label number of the luggage
         * @param color         this allows you to pick the color of the lost luggage
         * @param type          this allows you to fill in the type of the lost luggage
         * @param dateLost      this allows you to pick the lost date of the lost lugge
         * @param dateDelivered this allows you to pick the delivered date of the lost luggage
         * @param timeTooLate   this allows you to see when the luggage was supposed to arrive.
         */
        public Data(long lostAndFounID, String passengerName, String labelNumber, String color, String type, LocalDate dateLost, LocalDate dateDelivered, long timeTooLate) {
            this.lostAndFounID = lostAndFounID;
            this.passengerName = passengerName;
            this.labelNumber = labelNumber;
            this.color = Color.valueOf(color);
            this.type = LuggageType.valueOf(type);
            this.dateLost = dateLost;
            this.dateDelivered = dateDelivered;
            this.timeTooLate = timeTooLate;
        }

        /**
         * Getter for the lost and found id.
         *
         * @return the lost and found id
         */
        public long getLostAndFounID() {
            return lostAndFounID;
        }

        /**
         * Getter for the passenger name
         *
         * @return the passenger name
         */
        public String getPassengerName() {
            return passengerName;
        }

        /**
         * Getter for the label number.
         *
         * @return the label number.
         */
        public String getLabelNumber() {
            return labelNumber;
        }

        /**
         * Getter for the color of the luggage.
         *
         * @return luggage color
         */
        public Color getColor() {
            return color;
        }

        /**
         * Getter for the luggage type.
         *
         * @return the luggage type
         */
        public LuggageType getType() {
            return type;
        }

        /**
         * Getter for the date when this luggage was lost.
         *
         * @return the lost date
         */
        public String getDateLost() {
            return dateLost.toString();
        }

        /**
         * Getter for the date when this luggage was delivered.
         *
         * @return get the date delivered
         */
        public String getDateDelivered() {
            String out = "";
            if (dateDelivered != null) {
                out = dateDelivered.toString();
            }
            return out;
        }

        /**
         * Getter for number of days too late.
         *
         * @return time too late
         */
        public String getTimeTooLate() {
            return String.valueOf(timeTooLate);
        }
    }
}

