CREATE TABLE IF NOT EXISTS addresses
(
  address_id   BIGINT AUTO_INCREMENT,
  street       TEXT        NULL,
  house_number TINYTEXT    NULL,
  zipcode      VARCHAR(10) NULL,
  city         TEXT        NULL,
  country      TEXT        NULL,
  PRIMARY KEY (address_id)
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE IF NOT EXISTS passengers
(
  passenger_id  BIGINT AUTO_INCREMENT,
  first_name    TEXT     NULL,
  tussenvoegsel TINYTEXT NULL,
  last_name     TEXT     NULL,
  phone_number  TEXT     NULL,
  email         TEXT     NULL,
  PRIMARY KEY (passenger_id)
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE IF NOT EXISTS airports
(
  iata_code VARCHAR(3) NOT NULL,
  name      TEXT       NULL,
  country   TEXT       NULL,
  city      TEXT       NULL,
  PRIMARY KEY (iata_code)
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE IF NOT EXISTS addresslink
(
  passenger_id BIGINT NOT NULL,
  address_id   BIGINT NOT NULL,
  from_date    DATE   NOT NULL,
  until_date   DATE   NULL,
  PRIMARY KEY (passenger_id, address_id),

  INDEX addresslink_addresses_address_id_fk (address_id),
  FOREIGN KEY (address_id) REFERENCES addresses (address_id),
  FOREIGN KEY (passenger_id) REFERENCES passengers (passenger_id)
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE IF NOT EXISTS luggage
(
  tracking_nr BIGINT AUTO_INCREMENT,
  lost_since  DATETIME            NULL,
  type        TEXT                NULL,
  brand       TEXT                NULL,
  color       TEXT                NULL,
  extra       LONGTEXT            NULL,
  found       TINYINT DEFAULT '0' NOT NULL,
  airport     VARCHAR(3)          NULL,
  PRIMARY KEY (tracking_nr),

  INDEX luggage_airports_iata_code_fk (airport),
  FOREIGN KEY (airport) REFERENCES airports (iata_code)
)
  ENGINE = InnoDB
  CHARSET = utf8;


CREATE TABLE IF NOT EXISTS labels
(
  labelNr     VARCHAR(12) NOT NULL,
  tracking_nr BIGINT      NOT NULL,
  flight_nr   TINYTEXT    NULL,
  destination VARCHAR(3)  NULL,
  PRIMARY KEY (labelNr, tracking_nr),

  INDEX tracking_nr (tracking_nr),
  INDEX destination (destination),
  CONSTRAINT labels_airports_iata_code_fk
  FOREIGN KEY (destination) REFERENCES airports (iata_code),
  FOREIGN KEY (tracking_nr) REFERENCES luggage (tracking_nr)
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE IF NOT EXISTS luggagelink
(
  tracking_nr  BIGINT              NOT NULL,
  passenger_id BIGINT              NOT NULL,
  sent         TINYINT DEFAULT '0' NOT NULL,
  sent_date    DATE                NULL,
  deliver_date DATE                NULL,
  claimed      TINYINT DEFAULT '0' NOT NULL,
  claimed_date DATE                NULL,
  PRIMARY KEY (tracking_nr, passenger_id),

  INDEX luggagelink_passengers_passenger_id_fk (passenger_id),
  FOREIGN KEY (tracking_nr) REFERENCES luggage (tracking_nr),
  FOREIGN KEY (passenger_id) REFERENCES passengers (passenger_id)
)
  ENGINE = InnoDB
  CHARSET = utf8;


CREATE TABLE IF NOT EXISTS users
(
  user_id       BIGINT AUTO_INCREMENT,
  employee_nr   TEXT                                                NULL,
  first_name    TEXT                                                NULL,
  tussenvoegsel TINYTEXT                                            NULL,
  last_name     TEXT                                                NULL,
  email         VARCHAR(255)                                        NOT NULL,
  password      TEXT                                                NOT NULL,
  airport       VARCHAR(3)                                          NULL,
  function      ENUM ('ADMIN', 'ASSISTANT', 'DAMAGE', 'MANAGEMENT') NULL,
  PRIMARY KEY (user_id),
  UNIQUE (email),
  INDEX users_airports_iata_code_fk (airport),
  FOREIGN KEY (airport) REFERENCES airports (iata_code)
)
  ENGINE = InnoDB
  CHARSET = utf8;

INSERT IGNORE INTO `airports`
VALUES
  ('ACE', 'Lanzarote Airport', 'Spain', 'Lanzarote'),
  ('ADB', 'Izmir Adnan', 'Turkey', 'Izmir'),
  ('AGP', 'Malaga Airport', 'Spain', 'Malaga'),
  ('AMS', 'Schiphol', 'The Netherlands', 'Amsterdam'),
  ('AYT', 'Antalya', 'Turkey', 'Antalya'),
  ('BJL', 'Banjul International Airport', 'Gambia', 'Banjul'),
  ('BJV', 'Milas-Bodrum Airport', 'Turkey', 'Bodrum'),
  ('BOJ', 'Burgas Airport', 'Bulgaria', 'Burgas'),
  ('CFU', 'Corfu Ioannis Kapodistrias', 'Greece', 'Corfu'),
  ('CTA', 'Catania International Airport', 'Italy', 'Catania'),
  ('DLM', 'Dalaman', 'Turkey', 'Dalaman'),
  ('DXB', 'Dubai International Airport', 'United Arab Emirates', 'Dubai'),
  ('ECN', 'Nicosia', 'Cyprus', 'Nicosia'),
  ('FAO', 'Faro Airport', 'Portugal', 'Faro'),
  ('FUE', 'Fuerteventura Airport', 'Spain', 'Fuerteventura'),
  ('GZP', 'Gazipasa - Alanya', 'Turkey', 'Gazipasa-Alanya'),
  ('HER', 'Heraklion Airport ', 'Greece', 'Heraklion'),
  ('HRG', 'Hurghada International Airport', 'Egypt', 'Hurghada'),
  ('IST', 'Istanboel Ataturk', 'Turkey', 'Istanbul'),
  ('KGS', 'Kos', 'Greece', 'Kos'),
  ('LPA', 'Gran Canaria Airport', 'Spain', 'Gran Canaria'),
  ('MJT', 'Mytilini Odysseas Elytis', 'Greece', 'Mytilene'),
  ('NBE', 'Enfidha–Hammamet', 'Tunisia', 'Enfidha'),
  ('OHD', 'St.Paul the Apostle', 'Macedonia', 'Ohrid'),
  ('PMI', 'Aeroport de Palma de Mallorca', 'Spain', 'Palma de Mallorca'),
  ('RAK', 'Menara', 'Morocco', 'Marrakech'),
  ('RHO', 'Rodos ', 'Greece', 'Rhodes'),
  ('SMI', 'Aristarchos of Samos', 'Greece', 'Samos'),
  ('TFO', 'Aeropuerto de Tenerife Sur', 'Spain', 'Tenerife'),
  ('ZTH', 'Dionysios Solomos', 'Greece', 'Zakynthos');

#  Default username - password: admin@corendon.nl - Admmin1!
#  Default username - password: assistant@corendon.nl - Assistant1!
#  Default username - password: damage@corendon.nl - Damage1!
#  Default username - password: management@corendon.nl - Management1!
INSERT IGNORE INTO `users` (user_id, employee_nr, first_name, tussenvoegsel, last_name, email, password, airport, function) VALUES
  (1, 51416798, 'Henk', NULL, 'Timmerman', 'admin@corendon.nl',
   '32310fea85442ebf59c22ccd42a85b450dabf4bd77b31c04b70393c3193dc8deece77e5606eec411d33f78d42bc91ada192f6e1b6de9c57e2cea40ad19e3e488',
   'AMS', 'ADMIN'),
  (2, 56489686, 'Henk', NULL, 'Smit', 'assistant@corendon.nl',
   '1330df1d8e9955cc0a80aab058e42a60d40c32893544fcc1286907956cc50cef4eff0031929e2585055af3c187e92b578d0aeaf671f1476eba21fa1ea53636eb',
   'AMS', 'ASSISTANT'),
  (3, 56489745,'Remi', 'de', 'Boer', 'damage@corendon.nl',
   '595e0770c43f51e485c132e47d3cb7751a28fa9cc3bac186179e62c2b4ef955ff1493dcbbecff5f62d0c0a012f987b3ef77418b421dc7e28662b51deed26836a',
   'BJV', 'DAMAGE'),
  (4, 56485648,'Karel', 'van', 'Duin', 'management@corendon.nl',
   'fba3368519335dffaee3522cd69108e96f079bc5ca04af09189fbf966070cc5ea6681e51f2fb50bf669180d809e936c2c74fe860eabe261e25b090ef310d0da2',
   'AYT', 'MANAGEMENT');