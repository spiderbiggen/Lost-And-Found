package spirits.lostandfound;

import javafx.application.Application;
import javafx.stage.Stage;
import spirits.lostandfound.controllers.assistant.ExcelImport;
import spirits.lostandfound.database.SpiritsJDBC;
import spirits.lostandfound.reference.Layouts;
import spirits.lostandfound.util.LayoutHelper;
import spirits.lostandfound.util.PropertiesHelper;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

public class LoadPerformanceTest extends Application {
    private static int repeat = 770;
    
    public static void main(String... args) {
        if (args.length == 1) {
            try {
                repeat = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                //do it xd
            }
        }
        PropertiesHelper.initProperties();
        String schema = "spirits_performance_test";
        PropertiesHelper.getSettings().setProperty("database_name", schema);
        PropertiesHelper.getSettings().setProperty("database_script", "/db.sql");
        // Reset Database
        try (Connection connection = new SpiritsJDBC().createConnection();
             Statement statement = connection.createStatement()) {
            statement.executeUpdate("DROP SCHEMA IF EXISTS " + schema + ";");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        new SpiritsJDBC().initDB();
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        File[] files = {
                new File(getClass().getResource("/test_excel/FYS-SE-Corendon_TestData_Antalya-v17.1.xlsx").getFile()),
                new File(getClass().getResource("/test_excel/FYS-SE-Corendon_TestData_Bodrum-v17.1.xlsx").getFile()),
                new File(getClass().getResource("/test_excel/FYS-SE-Corendon_TestData_Malaga-v17.1.xlsx").getFile()),
        };
        System.out.println(Arrays.toString(files));
        int counter = 0;
        for (int i = 0; i < repeat; i++) {
            for (File file : files) {
                ExcelImport.readExcel(file);
                counter++;
            }
            System.out.println("Loaded " + counter + " excel files into database.");
        }
        LayoutHelper.createStageFromFxml(Layouts.LOGIN, primaryStage);
    }
}
