package spirits.lostandfound.reference;

import spirits.lostandfound.util.LocalizedString;

import java.util.Arrays;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * This enum is used to select the type of luggage when registering it.
 *
 * @author IS102-3
 */
public enum LuggageType {
    STANDARD("Standard"),
    BACKPACK("Backpack", "Bagpack", "Mochila", "S\u0131rt \u00E7antas\u0131", "Rugzak", "\u062D\u0642\u064A\u0628\u0629 \u0638\u0647\u0631"),
    BAG("Bag", "Bolsa", "Canta", "\u00C7anta", "Zak", "\u062D\u0642\u064A\u0628\u0629"),
    BOX("Box", "Case", "Caja", "Kutu", "Doos", "\u0635\u0646\u062F\u0648\u0642"),
    BRIEFCASE("Brief case", "Briefcase", " Malet\u00EDn", "I\u015F \u00E7antas\u0131", "Aktentas", "Business Case", "\u062D\u0627\u0644\u0629 \u0648\u062C\u064A\u0632\u0629"),
    CHEST("Chest", "Pecho", "G\u00F6\u011F\u00FCs", "Kist", "\u0635\u062F\u0631"),
    SPORTS_BAG("Sports Bag", "Bolsa Deportiva", "Spor \u00E7antas\u0131", "Sporttas", "Sport tas", "\u062D\u0642\u064A\u0628\u0629 \u0631\u064A\u0627\u0636\u064A\u0629"),
    SUITCASE("Suitcase", "Maleta", "Bavul", "Koffer", "\u062D\u0642\u064A\u0628\u0629 \u0633\u0641\u0631"),
    OTHER("Other", "Otro", "Di\u011Fer", "Overig", "Anders", "\u0622\u062E\u0631");

    private final LocalizedString type;
    private final String[] localNames;

    /**
     * Enum constructor.
     *
     * @param type Default unlocalized name.
     */
    LuggageType(String... type) {
        this.type = new LocalizedString("type_" + name(), type[0]);
        this.localNames = type;
    }

    /**
     * Translate all luggage types to the new Locale.
     *
     * @param resourceBundle The resource bundle that has all the strings for the given locale.
     */
    public static void setResourceBundle(ResourceBundle resourceBundle) {
        Arrays.stream(values()).forEach(luggageType -> luggageType.type.setResourceBundle(resourceBundle));
    }

    /**
     * Converts a String to a LuggageType if possible defaults to Standard LuggageType.
     *
     * @param string A string that represents a LuggageType
     * @return A {@link LuggageType} object.
     */
    public static LuggageType fromString(final String string) {
        Optional<LuggageType> optionalType = Arrays.stream(values()).filter(type -> Arrays.stream(type.localNames)
                .anyMatch(s -> s.equalsIgnoreCase(string))).findFirst();
        LuggageType luggageType = LuggageType.STANDARD;
        if (optionalType.isPresent()) {
            luggageType = optionalType.get();
        }
        return luggageType;
    }

    @Override
    public String toString() {
        return type.getLocalized();
    }
}
