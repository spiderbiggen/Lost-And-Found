package spirits.lostandfound.controllers.admin;

import spirits.lostandfound.controllers.BaseController;
import spirits.lostandfound.reference.Layouts;
import spirits.lostandfound.util.LocalizedString;

import static spirits.lostandfound.reference.Strings.*;

/**
 * Less duplicate code.
 *
 * @author IS102-3
 */
public abstract class AdminBaseController extends BaseController {

    @Override
    protected String getResource(LocalizedString pageTitle) {
        switch (pageTitle.getKey()) {
            case MAIN_MENU:
                return Layouts.ADMIN_ROLE_MANAGEMENT;
            case CREATE_USER:
                return Layouts.CREATE_USER;
            case EDIT_USER:
                return Layouts.EDIT_USER;
            default:
                return null;
        }
    }
}
