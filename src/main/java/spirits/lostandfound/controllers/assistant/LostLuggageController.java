package spirits.lostandfound.controllers.assistant;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import spirits.lostandfound.database.SpiritsJDBC;
import spirits.lostandfound.reference.Airport;
import spirits.lostandfound.reference.Color;
import spirits.lostandfound.reference.Layouts;
import spirits.lostandfound.reference.LuggageType;
import spirits.lostandfound.util.LayoutHelper;
import spirits.lostandfound.util.PDFExport;
import spirits.lostandfound.util.SendMail;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Optional;
import java.util.ResourceBundle;

import static spirits.lostandfound.reference.Strings.MAIN_MENU;
import static spirits.lostandfound.reference.Strings.REGISTER_LOST_LUGGAGE;

/**
 * FXML Controller class
 *
 * @author IS102-3
 */
public class LostLuggageController extends AssistantBaseController {

    private static final String CURRENT_SCREEN = REGISTER_LOST_LUGGAGE;

    @FXML
    private DatePicker datePicker;
    @FXML
    private TextField timeField;
    @FXML
    private ComboBox<Airport> airportField;
    @FXML
    private TextField labelNumber;
    @FXML
    private TextField labelFlightNumber;
    @FXML
    private ComboBox<Airport> labelDestination;
    @FXML
    private TextField contactFirstName;
    @FXML
    private TextField contactTussenvoegsel;
    @FXML
    private TextField contactLastName;
    @FXML
    private TextField contactPhoneNumber;
    @FXML
    private TextField contactMail;
    @FXML
    private TextField contactStraat;
    @FXML
    private TextField contactZIP;
    @FXML
    private TextField contactHomenumber;
    @FXML
    private TextField contactCountry;
    @FXML
    private ComboBox<LuggageType> infoLuggageType;
    @FXML
    private TextField infoLuggageBrand;
    @FXML
    private ComboBox<Color> infoLuggageColor;
    @FXML
    private TextArea infoLuggageExtra;
    @FXML
    private Button submitLuggage;
    @FXML
    private TextField contactTown;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
        setBreadCrumbs(rb, MAIN_MENU, getCurrentScreen());
        datePicker.setValue(LocalDate.now());
        LocalTime currentTime = LocalTime.now();
        timeField.setText(String.format("%02d:%02d", currentTime.getHour(), currentTime.getMinute()));
        airportField.setItems(FXCollections.observableArrayList(Airport.values()));
        airportField.setValue(Airport.fromCode(getCurrentUser().getAirport().getCode()));

        infoLuggageType.setItems(FXCollections.observableArrayList(LuggageType.values()));
        infoLuggageType.getItems().sort((o1, o2) -> {
            if (o1 == LuggageType.STANDARD) return -1;
            else return o1.toString().compareToIgnoreCase(o2.toString());
        });
        infoLuggageType.setValue(LuggageType.STANDARD);
        infoLuggageColor.setItems(FXCollections.observableArrayList(Color.values()));
        infoLuggageColor.getItems().sort((o1, o2) -> {
            if (o1 == Color.STANDARD) return -1;
            else return o1.toString().compareToIgnoreCase(o2.toString());
        });
        infoLuggageColor.setValue(Color.STANDARD);

        labelDestination.setItems(FXCollections.observableArrayList(Airport.values()));
        labelDestination.setValue(Airport.AMSTERDAM);
    }

    /**
     * Get the current screen.
     *
     * @return current screen
     */
    @Override
    protected String getCurrentScreen() {
        return CURRENT_SCREEN;
    }

    /**
     * Submit Lost Luggage.
     *
     * @param event Push the submit button
     */
    @FXML
    private void submitLostLuggage(ActionEvent event) {
        ResourceBundle resourceBundle = getResourceBundle();
        //show popup
        Optional<ButtonType> result = LayoutHelper.createDoubleActionPopup(resourceBundle.getString("popup_register_lost_luggage_title"), resourceBundle.getString("popup_register_lost_luggage_message"), submitLuggage.getScene().getWindow());
        if (result.isPresent() && result.get() == ButtonType.OK) {

            int passengerId = 0;
            int addressId = 0;
            int trackingNr = 0;

            String currentUser = currentUserLabel.getText();

            LocalDate date = datePicker.getValue();
            LocalTime time = LocalTime.now();
            LocalDateTime datetime = LocalDateTime.of(date, time);

            String labelNr = labelNumber.getText();
            String flightNr = labelFlightNumber.getText();
            String destinationCode = "";
            String destination = "";
            if (labelDestination.getValue() != Airport.STANDARD) {
                destinationCode = labelDestination.getValue().getCode();
                destination = labelDestination.getValue().toString();
            }

            String firstName = contactFirstName.getText();
            String tussenvoegsel = contactTussenvoegsel.getText();
            String lastName = contactLastName.getText();
            String phoneNumber = contactPhoneNumber.getText();
            String mail = contactMail.getText();

            String straat = contactStraat.getText();
            String zip = contactZIP.getText();
            String homeNumber = contactHomenumber.getText();
            String town = contactTown.getText();
            String country = contactCountry.getText();

            String airport = airportField.getValue().toString();

            String type = "";
            if (infoLuggageType.getValue() != LuggageType.STANDARD) {
                type = infoLuggageType.getValue().name();
            }
            String brand = infoLuggageBrand.getText();
            String color = "";
            if (infoLuggageColor.getValue() != Color.STANDARD) {
                color = infoLuggageColor.getValue().name();
            }
            String extra = infoLuggageExtra.getText();

            SpiritsJDBC spiritsJDBC = new SpiritsJDBC();
            //try's for insert.
            try (Connection connection = spiritsJDBC.createConnection();
                 Statement statement = connection.createStatement()) {

                final String sql = "INSERT INTO luggage "
                        + "(lost_since, type, brand, color, extra, airport, found)"
                        + " VALUES ('" + date + "', '" + type + "',"
                        + "'" + brand + "', '" + color + "', '" + extra + "', '" + destinationCode + "', FALSE);";

                statement.executeUpdate(sql);
                System.out.println("Lost luggage is inserted");
                statement.close();
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }

            try (Connection connection = spiritsJDBC.createConnection();
                 Statement statement = connection.createStatement()) {

                final String sql1 = "INSERT INTO passengers "
                        + "(first_name, tussenvoegsel, last_name, phone_number, email)"
                        + " VALUES ('" + firstName + "', '" + tussenvoegsel + "',"
                        + "'" + lastName + "', '" + phoneNumber + "', '" + mail + "');";

                statement.executeUpdate(sql1);
                System.out.println("Passenger is inserted");

                statement.close();
                connection.close();

            } catch (SQLException ex) {
                ex.printStackTrace();
            }

            try (Connection connection = spiritsJDBC.createConnection();
                 Statement statement = connection.createStatement()) {

                final String sql2 = "INSERT INTO addresses "
                        + "(street, house_number, zipcode, city, country) "
                        + " VALUES ('" + straat + "', '" + homeNumber + "',"
                        + "'" + zip + "', '" + town + "', '" + country + "');";

                statement.executeUpdate(sql2);
                System.out.println("Adress is inserted");

                statement.close();
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try (Connection connection = spiritsJDBC.createConnection();
                 Statement statement = connection.createStatement()) {

                //get tracking nr from luggage halen.
                final String SqlVoorTrackingNr = "SELECT MAX(tracking_nr) AS last_id "
                        + "FROM luggage";

                ResultSet resultTrackingNr = statement.executeQuery(SqlVoorTrackingNr);
                while (resultTrackingNr.next()) {
                    trackingNr = resultTrackingNr.getInt("last_id");
                }

                final String sql3 = "INSERT INTO labels "
                        + "(labelNr, tracking_nr, flight_nr, destination) "
                        + " VALUES ('" + labelNr + "', '" + trackingNr + "', '" + flightNr + "',"
                        + "'" + destinationCode + "');";

                statement.executeUpdate(sql3);
                System.out.println("Label is inserted");

                statement.close();
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try (Connection connection = spiritsJDBC.createConnection();
                 Statement statement = connection.createStatement()) {

                //get passenger id and address id
                final String sqlVoorAdressId = "SELECT MAX(address_id) AS last_id "
                        + "FROM addresses";
                final String SqlVoorPassengerId = "SELECT MAX(passenger_id) AS last_id "
                        + "FROM passengers";

                ResultSet resultAdressId = statement.executeQuery(sqlVoorAdressId);
                while (resultAdressId.next()) {
                    addressId = resultAdressId.getInt("last_id");
                }
                ResultSet resultPassengerId = statement.executeQuery(SqlVoorPassengerId);
                while (resultPassengerId.next()) {
                    passengerId = resultPassengerId.getInt("last_id");
                }

                final String sql4 = "INSERT INTO addresslink "
                        + "(passenger_id, address_id, from_date) "
                        + " VALUES ('" + passengerId + "', '" + addressId + "',"
                        + "'" + datetime + "');";

                statement.executeUpdate(sql4);
                System.out.println("Adresslink is inserted");

                statement.close();
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try (Connection connection = spiritsJDBC.createConnection();
                 Statement statement = connection.createStatement()) {

                //get tracking nr from luggage.
                final String SqlVoorTrackingNr = "SELECT MAX(tracking_nr) AS last_id "
                        + "FROM luggage";
                final String SqlVoorPassengerId = "SELECT MAX(passenger_id) AS last_id "
                        + "FROM passengers";

                ResultSet resultTrackingNr = statement.executeQuery(SqlVoorTrackingNr);
                while (resultTrackingNr.next()) {
                    trackingNr = resultTrackingNr.getInt("last_id");
                }
                ResultSet resultPassengerId = statement.executeQuery(SqlVoorPassengerId);
                while (resultPassengerId.next()) {
                    passengerId = resultPassengerId.getInt("last_id");
                }

                final String sql5 = "INSERT INTO luggagelink "
                        + "(tracking_nr, passenger_id) "
                        + " VALUES ('" + trackingNr + "', '" + passengerId + "');";

                statement.executeUpdate(sql5);
                System.out.println("Luggagelink is inserted");

                statement.close();
                connection.close();

            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (mail != null && firstName != null && lastName != null) {
                    String fullname = tussenvoegsel.isEmpty() ? firstName + " " + lastName : firstName + " " + tussenvoegsel + " " + lastName;
                    new PDFExport(fullname, datetime, labelNr, flightNr,
                            destination, phoneNumber, mail, straat, zip,
                            homeNumber, town, country, airport, type,
                            brand, color, extra);
                    new SendMail(mail, fullname, currentUser, trackingNr);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        openScreen(event, Layouts.SEARCH_LUGGAGE);
    }
}