package spirits.lostandfound.reference;

import java.util.Locale;

/**
 * FXML Controller has the name of languages that we use in the programma.
 *
 * @author IS102-3
 */
public enum Language {
    ENGLISH("English", new Locale("en", "GB")),
    DUTCH("Nederlands", new Locale("NL", "nl")),
    TURKISH("T\u00FCrk", new Locale("tr", "TR")),
    SPANISH("Espa\u00F1ol", new Locale("es", "ES")),
    ARABIC("\u0627\u0644\u0639\u0631\u0628\u064A\u0629", new Locale("ar"));
    // العربية

    private final String name;
    private final Locale locale;

    /**
     * Enum Constructor.
     *
     * @param name   The name of the language. Preferably in the given locale.
     * @param locale The locale of the language.
     */
    Language(String name, Locale locale) {
        this.name = name;
        this.locale = locale;
    }

    /**
     * Get Locale
     *
     * @return locale
     */
    public Locale getLocale() {
        return locale;
    }

    @Override
    public String toString() {
        return name;
    }
}
