/*
 * Sending the luggage (doesnt work)
 */
package spirits.lostandfound.controllers.assistant;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import spirits.lostandfound.database.SpiritsJDBC;
import spirits.lostandfound.models.Luggage;
import spirits.lostandfound.models.LuggageLabel;
import spirits.lostandfound.models.Passenger;
import spirits.lostandfound.reference.Airport;
import spirits.lostandfound.reference.Layouts;
import spirits.lostandfound.reference.LuggageType;
import spirits.lostandfound.reference.Strings;
import spirits.lostandfound.util.LayoutHelper;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

import static spirits.lostandfound.reference.Strings.MAIN_MENU;

/**
 * @author IS102-3
 */
public class SendLuggageSendController extends AssistantBaseController {

    private static final String CURRENT_SCREEN = Strings.SEND_LUGGAGE_SEND;

    @FXML
    private TextField nameText;
    @FXML
    private TextField middleNameText;
    @FXML
    private TextField lastNameText;
    @FXML
    private DatePicker senddateText;
    @FXML
    private ComboBox<Airport> currentLocation_text;
    @FXML
    private ComboBox<Airport> destination_text;
    @FXML
    private ComboBox<String> delivery_combo;
    @FXML
    private ComboBox<LuggageType> luggageType_combo;
    @FXML
    private TextField bagsText;
    @FXML
    private TextField priceDeliveryText;
    @FXML
    private TextField totalText;
    @FXML
    private Button cancel;
    @FXML
    private Button sendDeLuggage;
    @FXML
    private Label priceInclVAT;
    @FXML
    private Label priceExclVAT;
    @FXML
    private Label totalPrice;

    private Luggage luggage;
    private LuggageLabel luggageLabel;
    private Passenger passenger;

    /**
     * Initialize view into default data
     *
     * @param url use and set the breadcrumbs
     * @param rb  use and set the breadcrumbs
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
        setBreadCrumbs(rb, MAIN_MENU, getCurrentScreen());
        senddateText.setValue(LocalDate.now());
        currentLocation_text.setItems(FXCollections.observableArrayList(Airport.values()));
        currentLocation_text.setValue(getCurrentUser().getAirport());
        destination_text.setItems(FXCollections.observableArrayList(Airport.values()));
        destination_text.setValue(Airport.STANDARD);

        luggageType_combo.setItems(FXCollections.observableArrayList(LuggageType.values()));
        luggageType_combo.setValue(LuggageType.STANDARD);

        ArrayList<String> deliveryMethods = new ArrayList<>();
        deliveryMethods.add("DHL");
        ResourceBundle resourceBundle = getResourceBundle();
        deliveryMethods.add(resourceBundle.getString("delivery_methods_corendon_flight"));
        deliveryMethods.add(resourceBundle.getString("delivery_methods_local_deliverer"));
        deliveryMethods.add(resourceBundle.getString("delivery_methods_pickup_by_owner"));

        delivery_combo.setItems(FXCollections.observableArrayList(deliveryMethods));
        delivery_combo.setValue(deliveryMethods.get(0));
    }

    /**
     * Getting the current screen
     *
     * @return current screen
     */
    @Override
    protected String getCurrentScreen() {
        return CURRENT_SCREEN;
    }

    /**
     * Set data from the luggage label on this screen.
     *
     * @param luggageLabel the new luggage label.
     */
    public void setLuggageLabel(LuggageLabel luggageLabel) {
        this.luggageLabel = luggageLabel;
        if (luggageLabel.getDestination() != null) {
            destination_text.setValue(Airport.fromCode(luggageLabel.getDestination()));
        }
    }

    /**
     * Sets data about this passenger on this screen.
     *
     * @param passenger the new passenger.
     */
    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
        nameText.setText(passenger.getFirstName());
        middleNameText.setText(passenger.getTussenvoegsel());
        lastNameText.setText(passenger.getLastName());
    }

    /**
     * Sets data about the luggage on this screen.
     *
     * @param luggage the new luggage.
     */
    public void setLuggage(Luggage luggage) {
        this.luggage = luggage;
        luggageType_combo.setValue(luggage.getType());
    }

    /**
     * Sending the luggage
     */
    @FXML
    private void sendLuggage(ActionEvent event) {
        ResourceBundle resourceBundle = getResourceBundle();
        long trackingNrOfSelectedLuggage = luggage.getTrackingId();
        Optional<ButtonType> result = LayoutHelper.createDoubleActionPopup(resourceBundle.getString("popup_send_luggage_title"), resourceBundle.getString("popup_send_luggage_message").replace("%", String.valueOf(trackingNrOfSelectedLuggage)), nameText.getScene().getWindow());
        if (result.isPresent() && result.get() == ButtonType.OK) {
            //language = MySQL
            String sql = "UPDATE luggagelink SET sent = 1, sent_date = ? WHERE tracking_nr = ?;";
            try (Connection connection = new SpiritsJDBC().createConnection();
                 PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setObject(1, LocalDateTime.now());
                statement.setLong(2, trackingNrOfSelectedLuggage);
                statement.executeUpdate();
                // TODO send email
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        openScreen(event, Layouts.SEARCH_LUGGAGE); //go back to homepage
    }
}
