package spirits.lostandfound;

import javafx.application.Application;
import javafx.stage.Stage;
import spirits.lostandfound.database.SpiritsJDBC;
import spirits.lostandfound.reference.Layouts;
import spirits.lostandfound.util.LayoutHelper;
import spirits.lostandfound.util.PropertiesHelper;

/**
 * The Entry point of the program.
 *
 * @author IS102-3
 */
public class Main extends Application {

    /**
     * The starting point of the program.
     *
     * @param args Anything. Unused.
     */
    public static void main(String[] args) {
        PropertiesHelper.initProperties();
        new Thread(() -> new SpiritsJDBC().initDB()).start();
        launch(args);
        PropertiesHelper.saveProperties();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        LayoutHelper.createStageFromFxml(Layouts.LOGIN, primaryStage);
    }

}
