package spirits.lostandfound.models;


/**
 * Stores a passenger.
 *
 * @author IS102-3
 */
public class Passenger extends Person {
    private static long highestId = 1;
    private final long passengerId;
    private final String phoneNumber;


    /**
     * A method for making a passenger
     *
     * @param firstName     the first name of the passenger.
     * @param tussenvoegsel the insertion of the passenger.
     * @param lastName      the last name of the passenger
     * @param phoneNumber   the phone number of the passenger
     * @param email         the email of the passenger.
     */
    public Passenger(String firstName, String tussenvoegsel, String lastName, String phoneNumber, String email) {
        this(highestId++, firstName, tussenvoegsel, lastName, phoneNumber, email);
    }

    /**
     * A method for making a passenger with a passengerID.
     *
     * @param passengerId   the id of the passenger
     * @param firstName     the first name of the passenger.
     * @param tussenvoegsel the insertion of the passenger.
     * @param lastName      the last name of the passenger
     * @param phoneNumber   the phone number of the passenger
     * @param email         the email of the passenger.
     */
    public Passenger(long passengerId, String firstName, String tussenvoegsel, String lastName, String phoneNumber, String email) {
        super(firstName, tussenvoegsel, lastName, email);
        this.passengerId = passengerId;
        this.phoneNumber = phoneNumber;
    }

    /**
     * Getter for the passengerID
     *
     * @return passengerID
     */
    public long getPassengerId() {
        return passengerId;
    }

    /**
     * Getter for the phone number of the passenger.
     *
     * @return phone number of the passenger.
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }


}
