package spirits.lostandfound.controllers.management;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import spirits.lostandfound.database.SpiritsJDBC;
import spirits.lostandfound.reference.Airport;
import spirits.lostandfound.reference.Layouts;
import spirits.lostandfound.reference.Strings;
import spirits.lostandfound.util.LocalizedString;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Controller for the missing luggage chart
 *
 * @author IS102-3
 */
public class MissingLuggageChart extends ManagementBaseController {

    private static LocalizedString WITHIN_3_DAYS = new LocalizedString("stats_found_days_3", "Found within 3 days");
    private static LocalizedString WITHIN_21_DAYS = new LocalizedString("stats_found_days_21", "Found within 21 days");
    private static LocalizedString NEVER_FOUND = new LocalizedString("stats_found_never", "Never found");

    @FXML
    private PieChart pieChart;
    @FXML
    private Label percentageWithin3Days;
    @FXML
    private Label percentageWithin21Days;
    @FXML
    private Label percentageNeverFound;
    @FXML
    private Label numberWithin3Days;
    @FXML
    private Label numberWithin21Days;
    @FXML
    private Label numberNeverFound;
    @FXML
    private Label numberTotal;
    @FXML
    private ComboBox<Airport> airportCb;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        setBreadCrumbs(resources, getCurrentScreen());

        WITHIN_3_DAYS.setResourceBundle(resources);
        WITHIN_21_DAYS.setResourceBundle(resources);
        NEVER_FOUND.setResourceBundle(resources);

        airportCb.setItems(FXCollections.observableArrayList(Airport.values()));
        airportCb.setValue(getCurrentUser().getAirport());

        updateChart(airportCb.getValue());
    }

    /**
     * Combobox event to change the diagram to a other airport
     *
     * @param event update the chart
     */
    @FXML
    private void airportChange(ActionEvent event) {
        updateChart(airportCb.getValue());
    }

    /**
     * Update and filling the chart to a other airport
     *
     * @param airport the value of the combobox
     */
    private void updateChart(Airport airport) {

        String iata_code = airport == Airport.STANDARD ? "%" : airport.getCode();
        String sql = "SELECT * FROM \n"
                + "	(SELECT COUNT(luggage.tracking_nr) as lessThan3 FROM luggage LEFT JOIN luggagelink ON luggage.tracking_nr = luggagelink.tracking_nr\n"
                + "		WHERE date(luggage.lost_since) + interval 3 day > date(luggagelink.deliver_date) AND luggage.airport LIKE '" + iata_code + "') as een,\n"
                + "	(SELECT COUNT(luggage.tracking_nr) as between3And21 FROM luggage LEFT JOIN luggagelink ON luggage.tracking_nr = luggagelink.tracking_nr\n"
                + "		WHERE date(luggage.lost_since) + interval 3 day < date(luggagelink.deliver_date) AND date(luggage.lost_since) + interval 21 day >= date(luggagelink.deliver_date) AND luggage.airport LIKE '" + iata_code + "') as twee,\n"
                + "	(SELECT COUNT(luggage.tracking_nr) as moreThan21 FROM luggage LEFT JOIN luggagelink ON luggage.tracking_nr = luggagelink.tracking_nr\n"
                + "		WHERE date(luggage.lost_since) + interval 21 day < date(ifnull(luggagelink.deliver_date, curdate())) AND luggage.airport LIKE '" + iata_code + "') as drie;";

        //Mock data
        long numberFoundWithin3Days = 0;
        long numberFoundWithin21Days = 0;
        long neverFound = 0;

        try (Connection connection = new SpiritsJDBC().createConnection();
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                numberFoundWithin3Days = resultSet.getLong("lessThan3");
                numberFoundWithin21Days = resultSet.getLong("between3And21");
                neverFound = resultSet.getLong("moreThan21");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        long totalMissing = numberFoundWithin3Days + numberFoundWithin21Days + neverFound;

        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList(
                new PieChart.Data(WITHIN_3_DAYS.getLocalized(), numberFoundWithin3Days),
                new PieChart.Data(WITHIN_21_DAYS.getLocalized(), numberFoundWithin21Days),
                new PieChart.Data(NEVER_FOUND.getLocalized(), neverFound)
        );
        pieChart.setData(pieChartData);

        percentageWithin3Days.setText(createPercentString(numberFoundWithin3Days, totalMissing));
        percentageWithin21Days.setText(createPercentString(numberFoundWithin21Days, totalMissing));
        percentageNeverFound.setText(createPercentString(neverFound, totalMissing));
        numberWithin3Days.setText(String.valueOf(numberFoundWithin3Days));
        numberWithin21Days.setText(String.valueOf(numberFoundWithin21Days));
        numberNeverFound.setText(String.valueOf(neverFound));
        numberTotal.setText(String.valueOf(totalMissing));
    }

    /**
     * Creating percentages for the piechart
     *
     * @param top   number found in 3/21 or never found luggage
     * @param total total that is missing
     * @return Percentages
     */
    private String createPercentString(long top, long total) {
        return String.format("%.1f%%", top * 100f / total);
    }

    /**
     * Getting the current screen
     *
     * @return the current screen
     */
    @Override
    protected String getCurrentScreen() {
        return Strings.MISSING_LUGGAGE_PIE_CHART;
    }

    @FXML
    private void showLostLuggageMonthAndYear(ActionEvent event) {
        openScreen(event, Layouts.LOST_LUGGAGE_MONTH_AND_YEAR);
    }
}
