package spirits.lostandfound.controllers.assistant;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import spirits.lostandfound.database.SpiritsJDBC;
import spirits.lostandfound.models.Luggage;
import spirits.lostandfound.models.LuggageLabel;
import spirits.lostandfound.models.Passenger;
import spirits.lostandfound.reference.*;
import spirits.lostandfound.util.LayoutHelper;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Controller for search luggage page
 *
 * @author IS102-3
 */
public class SearchLuggageController extends AssistantBaseController {

    @FXML
    public TextField lostAndFoundID;
    @FXML
    private ComboBox<Color> color;
    @FXML
    private ComboBox<Airport> destination;
    @FXML
    private CheckBox foundCheck;
    @FXML
    private CheckBox lostCheck;
    @FXML
    private TextField luggageLabel;
    @FXML
    private TextField flightNumber;
    @FXML
    private ComboBox<LuggageType> luggageType;
    @FXML
    private TextField brand;
    @FXML
    private TextField passengerFirstName;
    @FXML
    private TextField passengerLastName;
    @FXML
    private TextField insertion;
    @FXML
    private TableView<Data> tableView;
    @FXML
    private TableColumn<Data, String> trackingNumber;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
        setBreadCrumbs(rb, Strings.MAIN_MENU);

        color.setItems(FXCollections.observableArrayList(Color.values()));
        color.getItems().sort((o1, o2) -> {
            if (o1 == Color.STANDARD) return -1;
            else return o1.toString().compareToIgnoreCase(o2.toString());
        });
        color.setValue(Color.STANDARD);
        destination.setItems(FXCollections.observableArrayList(Airport.values()));
        destination.setValue(Airport.STANDARD);
        luggageType.setItems(FXCollections.observableArrayList(LuggageType.values()));
        luggageType.getItems().sort((o1, o2) -> {
            if (o1 == LuggageType.STANDARD) return -1;
            else return o1.toString().compareToIgnoreCase(o2.toString());
        });
        luggageType.setValue(LuggageType.STANDARD);

        foundCheck.setSelected(true);
        lostCheck.setSelected(true);


        for (TableColumn<Data, ?> tableColumn : tableView.getColumns()) {
            String propertyName = tableColumn.getId();
            if (propertyName != null && !propertyName.isEmpty()) {
                tableColumn.setCellValueFactory(new PropertyValueFactory<>(propertyName));
            }
        }
        fillTable();
    }

    /**
     * Return the title of the current screen.
     *
     * @return the title of the current screen.
     */
    @Override
    protected String getCurrentScreen() {
        return Strings.MAIN_MENU;
    }

    /**
     * When searched is pressed, fill the table with data
     *
     * @param actionevent Event fired by pressing the search button.
     */
    public void searchAction(ActionEvent actionevent) {
        tableView.getItems().clear();
        fillTable();
    }

    /**
     * Filling the table with the searched data
     */
    private void fillTable() {
        ResourceBundle resourceBundle = getResourceBundle();
        if (!foundCheck.isSelected() && !lostCheck.isSelected()) {
            LayoutHelper.createSingleActionPopup(resourceBundle.getString("popup_claim_luggage_nothing_selected_title"), resourceBundle.getString("popup_search_luggage_nothing_selected_message"), luggageLabel.getScene().getWindow());
        } else {
            //language=MySQL
            String sql = "SELECT * FROM labels RIGHT JOIN luggage ON luggage.tracking_nr = labels.tracking_nr " +
                    "LEFT JOIN luggagelink ON luggagelink.tracking_nr = labels.tracking_nr " +
                    "LEFT JOIN passengers ON passengers.passenger_id = luggagelink.passenger_id " +
                    "WHERE ifnull(labelNr,'') LIKE concat('%', ?, '%') AND ifnull(flight_nr, '') LIKE concat('%', ?, '%') " +
                    "AND ifnull(destination,'') LIKE concat('%', ?, '%') AND luggage.type LIKE concat('%', ?, '%') " +
                    "AND brand LIKE concat('%', ?, '%') AND color LIKE ? AND ifnull(first_name,'') LIKE CONCAT('%',?,'%') " +
                    "AND ifnull(tussenvoegsel,'') LIKE CONCAT('%', ?, '%') AND ifnull(last_name,'') LIKE CONCAT('%',?,'%') " +
                    "AND found LIKE ? AND luggage.tracking_nr LIKE CONCAT('%',?,'%');";

            try (Connection connection = new SpiritsJDBC().createConnection();
                 PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, luggageLabel.getText());
                statement.setString(2, flightNumber.getText());

                statement.setString(3, destination.getValue() == Airport.STANDARD ? "" : destination.getValue().getCode());
                statement.setString(4, luggageType.getValue() == LuggageType.STANDARD ? "" : luggageType.getValue().name());
                statement.setString(5, brand.getText());

                statement.setString(6, color.getValue() == Color.STANDARD ? "%" : color.getValue().name());
                statement.setString(7, passengerFirstName.getText());
                statement.setString(8, insertion.getText());
                statement.setString(9, passengerLastName.getText());
                if (foundCheck.isSelected() && lostCheck.isSelected()) {
                    statement.setString(10, "%");
                } else {
                    statement.setBoolean(10, foundCheck.isSelected());
                }
                statement.setString(11, lostAndFoundID.getText());
                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        String luggageLabel = resultSet.getString("labelNr");
                        String flightNumber = resultSet.getString("flight_nr");
                        String destination = resultSet.getString("destination");
                        String type = resultSet.getString("type");
                        String brand = resultSet.getString("brand");
                        String color = resultSet.getString("color");
                        String currentLocation = resultSet.getString("airport");
                        String firstName = resultSet.getString("first_name");
                        String tussenvoegsel = resultSet.getString("tussenvoegsel");
                        String lastName = resultSet.getString("last_name");
                        long trackingNumber = resultSet.getLong("luggage.tracking_nr");
                        boolean found = resultSet.getBoolean("found");
                        String passengerName = (firstName == null ? "" : firstName) + (tussenvoegsel == null ? "" : " " + tussenvoegsel) + (lastName == null ? "" : " " + lastName);
                        tableView.getItems().add(new Data(luggageLabel, flightNumber, destination, type, brand, color, currentLocation, firstName, tussenvoegsel, lastName, trackingNumber, passengerName, found));
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Open the lost luggage page.
     *
     * @param actionEvent pushing the button "register lost luggage".
     */
    @FXML
    private void openLostLuggage(ActionEvent actionEvent) {
        openScreen(actionEvent, Layouts.LOST_LUGGAGE_FORM);
    }

    /**
     * Open the send luggage page
     *
     * @param actionEvent Pushing the button "Send luggage".
     */
    @FXML
    private void openSendLuggage(ActionEvent actionEvent) {
        ResourceBundle resourceBundle = getResourceBundle();
        if (tableView.getSelectionModel().getSelectedItem() == null) {
            LayoutHelper.createSingleActionPopup(resourceBundle.getString("popup_claim_luggage_nothing_selected_title"), resourceBundle.getString("popup_claim_luggage_nothing_selected_message"), tableView.getScene().getWindow());
        } else {
            Data data = tableView.getSelectionModel().getSelectedItem();
            if (data.isFound()) {
                if (data.getFirstName() == null) {
                    LayoutHelper.createSingleActionPopup(resourceBundle.getString("popup_search_luggage_no_passenger_title"), resourceBundle.getString("popup_search_luggage_no_passenger_message"), luggageLabel.getScene().getWindow());
                } else {
                    Luggage luggage = new Luggage(data.getTrackingNumber(), data.getTypeTv(), data.getBrandTv(), data.getColorTv(), data.getLocationTv(), null, null, data.isFound());
                    LuggageLabel label = new LuggageLabel(data.getLugageLabelTv(), -1, data.flightNumberTv, data.getDestinationTv());
                    Passenger passenger = new Passenger(data.getFirstName(), data.getTussenvoegsel(), data.getLastName(), null, null);
                    SendLuggageSendController controller = (SendLuggageSendController) openScreen(actionEvent, Layouts.SEND_LUGGAGE_SEND);
                    controller.setLuggageLabel(label);
                    controller.setPassenger(passenger);
                    controller.setLuggage(luggage);
                }
            } else {
                LayoutHelper.createSingleActionPopup(resourceBundle.getString("popup_search_luggage_not_found_title"), resourceBundle.getString("popup_search_luggage_not_found_message"), luggageLabel.getScene().getWindow());
            }
        }
    }

    /**
     * Open the found luggage page.
     *
     * @param actionEvent pushing the button "found luggage".
     */
    @FXML
    private void openFoundLuggage(ActionEvent actionEvent) {
        openScreen(actionEvent, Layouts.FOUND_LUGGAGE_FORM);
    }

    /**
     * starting the import of an excelsheet.
     *
     * @param event pushing the button "Import excel luggage".
     */
    @FXML
    private void toExcelImport(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Excel (*.xlsx)", "*.xlsx"));
        List<File> selectedFiles = fileChooser.showOpenMultipleDialog(null);
        ResourceBundle resourceBundle = getResourceBundle();
        if (selectedFiles == null) {
            System.out.println("Excel import is gecanceled.");
        } else {
            System.out.println(selectedFiles);
            StringBuilder stringBuilder = new StringBuilder(System.lineSeparator());
            for (File file : selectedFiles) {
                stringBuilder.append(file.getName()).append(System.lineSeparator());
            }
            Optional<ButtonType> input = LayoutHelper.createDoubleActionPopup(resourceBundle.getString("popup_import_luggage_title"),
                    resourceBundle.getString("popup_import_luggage_message").replace("*%*", stringBuilder.toString()),
                    luggageLabel.getScene().getWindow());
            if (input.isPresent() && input.get() == ButtonType.OK) {
                boolean success = true;
                for (File file : selectedFiles) {
                    success &= ExcelImport.readExcel(file);
                }
                String textKey = success ? "popup_import_luggage_success" : "popup_import_luggage_failure";
                LayoutHelper.createSingleActionPopup(resourceBundle.getString("popup_import_luggage_title"), resourceBundle.getString(textKey), luggageLabel.getScene().getWindow());
            }
        }
    }

    /**
     * Data class to store results from the database query. These results can
     * then be shown in a {@link TableView}.
     */
    public class Data {

        private final String lugageLabelTv;
        private final String flightNumberTv;
        private final String destinationTv;
        private final LuggageType typeTv;
        private final String brandTv;
        private final Color colorTv;
        private final String locationTv;
        private final String firstName;
        private final String tussenvoegsel;
        private final String lastName;
        private final long trackingNumber;
        private final String passengerName;
        private final boolean found;


        /**
         * The contructor for this data.
         *
         * @param lugageLabelTv  the luggage label.
         * @param flightNumberTv the flight number.
         * @param destinationTv  the destination.
         * @param typeTv         the luggage type.
         * @param brandTv        the brand.
         * @param colorTv        the color.
         * @param locationTv     the location.
         * @param firstName      the first name of the passenger.
         * @param tussenvoegsel  the tussenvoegsel of the passenger.
         * @param lastName       the last name of the passenger.
         * @param passengerName  the full name of the passenge.
         * @param trackingNumber the tracking number of the luggage.
         * @param found          if the luggage has been found.
         */
        public Data(String lugageLabelTv, String flightNumberTv, String destinationTv, String typeTv, String brandTv, String colorTv, String locationTv, String firstName, String tussenvoegsel, String lastName, long trackingNumber, String passengerName, boolean found) {
            this.lugageLabelTv = lugageLabelTv;
            this.flightNumberTv = flightNumberTv;
            this.destinationTv = destinationTv;
            this.typeTv = LuggageType.valueOf(typeTv);
            this.brandTv = brandTv;
            this.colorTv = Color.valueOf(colorTv);
            this.locationTv = locationTv;
            this.firstName = firstName;
            this.tussenvoegsel = tussenvoegsel;
            this.lastName = lastName;
            this.trackingNumber = trackingNumber;
            this.passengerName = passengerName;
            this.found = found;
        }

        /**
         * Getter for the luggage label.
         *
         * @return The luggage label.
         */
        public String getLugageLabelTv() {
            return lugageLabelTv;
        }

        /**
         * Getter for the flight number.
         *
         * @return the flight number.
         */
        public String getFlightNumberTv() {
            return flightNumberTv;
        }

        /**
         * Getter for the destination.
         *
         * @return the destination code
         */
        public String getDestinationTv() {
            return destinationTv;
        }

        /**
         * Getter for the type.
         *
         * @return the luggage type
         */
        public LuggageType getTypeTv() {
            return typeTv;
        }

        /**
         * Getter for the brand.
         *
         * @return the brand.
         */
        public String getBrandTv() {
            return brandTv;
        }

        /**
         * Getter for the color.
         *
         * @return the color.
         */
        public Color getColorTv() {
            return colorTv;
        }

        /**
         * Getter for the location
         *
         * @return the location.
         */
        public String getLocationTv() {
            return locationTv;
        }

        /**
         * Getter for the first name.
         *
         * @return the first name
         */
        public String getFirstName() {
            return firstName;
        }

        /**
         * Getter for the tussenvoegsel.
         *
         * @return the tussenvoegsel.
         */
        public String getTussenvoegsel() {
            return tussenvoegsel;
        }

        /**
         * Getter for the last name.
         *
         * @return the last name.
         */
        public String getLastName() {
            return lastName;
        }

        /**
         * Getter for the tracking number.
         *
         * @return the tracking number
         */
        public long getTrackingNumber() {
            return trackingNumber;
        }

        /**
         * Getter for the full name of the passenger.
         *
         * @return the full name
         */
        public String getPassengerName() {
            return passengerName;
        }

        /**
         * Returns if the luggage is found.
         *
         * @return true if found false otherwise.
         */
        public boolean isFound() {
            return found;
        }
    }

}
