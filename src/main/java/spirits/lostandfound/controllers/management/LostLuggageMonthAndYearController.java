package spirits.lostandfound.controllers.management;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import spirits.lostandfound.database.SpiritsJDBC;
import spirits.lostandfound.reference.Strings;
import spirits.lostandfound.util.LocalizedString;
import spirits.lostandfound.util.PropertiesHelper;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormatSymbols;
import java.util.*;
import java.util.stream.Collectors;

import static spirits.lostandfound.reference.Strings.MISSING_LUGGAGE_PIE_CHART;

/**
 * FXML Controller class
 *
 * @author IS102-3
 */
public class LostLuggageMonthAndYearController extends ManagementBaseController {

    private final LocalizedString PER_YEAR = new LocalizedString("stats_show_per_year", "Year");
    private final LocalizedString PER_MONTH = new LocalizedString("stats_show_per_month", "Month");

    @FXML
    private ComboBox<LocalizedString> dataRangeSelection;
    @FXML
    private Label yearLabel;
    @FXML
    private ComboBox<Integer> yearSelection;
    @FXML
    private RadioButton pieChartToggle;
    @FXML
    private RadioButton barChartToggle;
    @FXML
    private Pane graphContainer;
    @FXML
    private ToggleGroup toggleGroup;

    private Chart chart;
    private DateFormatSymbols symbols = new DateFormatSymbols(PropertiesHelper.getLanguage().getLocale());

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        setBreadCrumbs(resources, MISSING_LUGGAGE_PIE_CHART, getCurrentScreen());

        PER_YEAR.setResourceBundle(resources);
        PER_MONTH.setResourceBundle(resources);

        ObservableList<LocalizedString> list = FXCollections.observableArrayList(PER_YEAR, PER_MONTH);
        dataRangeSelection.setItems(list);
        dataRangeSelection.setValue(PER_YEAR);

        toggleGroup.selectedToggleProperty().addListener(this::onGraphTypeChanged);
        toggleGroup.selectToggle(pieChartToggle);
    }

    @Override
    protected String getCurrentScreen() {
        return Strings.LOST_LUGGAGE_MONTH_AND_YEAR;
    }

    /**
     * Set the visibility of the year selection label and combobox.
     *
     * @param visible true if it should be visible false if not.
     */
    private void setYearSelectionVisibility(boolean visible) {
        yearLabel.setVisible(visible);
        yearSelection.setVisible(visible);
    }

    /**
     * Set the data in the chart.
     *
     * @param data The data.
     */
    private void setData(List<Map.Entry<String, Integer>> data) {
        if (chart instanceof PieChart) {
            ((PieChart) chart).getData().clear();
            List<PieChart.Data> pieData = data.stream()
                    .map(entry -> new PieChart.Data(entry.getKey(), (double) entry.getValue()))
                    .collect(Collectors.toList());
            ((PieChart) chart).setData(FXCollections.observableArrayList(pieData));
        } else if (chart instanceof BarChart) {
            XYChart.Series<String, Integer> series = new XYChart.Series<>();
            data.forEach(datum -> series.getData().add(new XYChart.Data<>(String.valueOf(datum.getKey()), datum.getValue())));
            ((BarChart) chart).getData().clear();
            ((BarChart<String, Integer>) chart).getData().add(series);
        }
    }

    /**
     * Set the data in the chart.
     *
     * @param data The data.
     */
    private void setDataPerMonth(List<Map.Entry<Integer, Integer>> data) {
        List<Map.Entry<String, Integer>> stringData = new ArrayList<>();
        if (chart instanceof PieChart) {
            stringData = data.stream()
                    .map(entry -> new AbstractMap.SimpleEntry<>(symbols.getMonths()[entry.getKey() - 1], entry.getValue()))
                    .collect(Collectors.toList());
        } else if (chart instanceof BarChart) {
            stringData = new ArrayList<>();
            int lastMonth = 0;
            for (Map.Entry<Integer, Integer> datum : data) {
                int month = datum.getKey();
                for (int i = lastMonth + 1; i < month; i++) {
                    stringData.add(new AbstractMap.SimpleEntry<>(symbols.getMonths()[i - 1], 0));
                }
                lastMonth = month;
                stringData.add(new AbstractMap.SimpleEntry<>(symbols.getMonths()[datum.getKey() - 1], datum.getValue()));
            }
            for (int i = lastMonth + 1; i <= 12; i++) {
                stringData.add(new AbstractMap.SimpleEntry<>(symbols.getMonths()[i - 1], 0));
            }
        }
        setData(stringData);
    }

    /**
     * @return A list of years that contain data sorted from high to low.
     */
    private List<Integer> getYearsWithData() {
        //language=MySQL
        List<Integer> years = new ArrayList<>();
        final String sql = "SELECT DISTINCT YEAR(lost_since) AS year FROM luggage ORDER BY YEAR(lost_since) DESC;";
        try (Connection connection = new SpiritsJDBC().createConnection();
             ResultSet resultSet = connection.createStatement().executeQuery(sql)) {
            while (resultSet.next()) {
                years.add(resultSet.getInt("year"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return years;
    }

    /**
     * Called whenever either of the radio buttons is clicked
     *
     * @param o unused Observable.
     */
    private void onGraphTypeChanged(Observable o) {
        Toggle i = toggleGroup.getSelectedToggle();
        if (i.equals(pieChartToggle)) {
            chart = new PieChart();
        } else if (i.equals(barChartToggle)) {
            CategoryAxis xAxis = new CategoryAxis();
            NumberAxis yAxis = new NumberAxis();
            chart = new BarChart<>(xAxis, yAxis);
        }
        chart.setAnimated(false);
        chart.setLegendVisible(false);
        graphContainer.getChildren().clear();
        graphContainer.getChildren().add(chart);
        AnchorPane.setTopAnchor(chart, 0.0);
        AnchorPane.setBottomAnchor(chart, 0.0);
        AnchorPane.setLeftAnchor(chart, 0.0);
        AnchorPane.setRightAnchor(chart, 0.0);
        onDataRangeChange(null);
    }

    /**
     * Called whenever the value of the DataRange combo box changes.
     *
     * @param actionEvent Event fired by changing the combo box changes value.
     */
    @FXML
    private void onDataRangeChange(ActionEvent actionEvent) {
        if (dataRangeSelection.getValue().equals(PER_YEAR)) {
            setYearSelectionVisibility(false);
            getDataPerYear();
        } else if (dataRangeSelection.getValue().equals(PER_MONTH)) {
            setYearSelectionVisibility(true);
            List<Integer> years = getYearsWithData();
            if (years.isEmpty()) return;
            Integer year = yearSelection.getValue();
            yearSelection.setItems(FXCollections.observableArrayList(years));
            yearSelection.setValue(year == null ? years.get(0) : year);
        }
    }

    /**
     * Called whenever the value of the yearSelection combo box changes.
     *
     * @param actionEvent Event fired by changing the combo box changes value.
     */
    @FXML
    private void onYearChange(ActionEvent actionEvent) {
        getDataPerMonth();
    }

    /**
     * Gets data from the database grouped by year.
     */
    private void getDataPerYear() {
        String sql = "SELECT YEAR(lost_since) AS year, COUNT(tracking_nr) AS amount FROM luggage GROUP BY YEAR(lost_since);";

        List<Map.Entry<String, Integer>> series = new ArrayList<>();

        try (Connection connection = new SpiritsJDBC().createConnection();
             ResultSet resultSet = connection.createStatement().executeQuery(sql)) {
            while (resultSet.next()) {
                series.add(new AbstractMap.SimpleEntry<>(resultSet.getString("year"), resultSet.getInt("amount")));
            }
            setData(series);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets data from the database grouped by month. But only for the selected year.
     */
    private void getDataPerMonth() {
        Integer year = yearSelection.getValue();
        if (year == null) return;
        String sql = "SELECT MONTH(lost_since) AS month, COUNT(tracking_nr) AS amount FROM luggage WHERE YEAR(lost_since) = " + year + " GROUP BY month ORDER BY month ASC ;";

        List<Map.Entry<Integer, Integer>> series = new ArrayList<>();

        try (Connection connection = new SpiritsJDBC().createConnection();
             ResultSet resultSet = connection.createStatement().executeQuery(sql)) {
            while (resultSet.next()) {
                int month = resultSet.getInt("month");
                series.add(new AbstractMap.SimpleEntry<>(month, resultSet.getInt("amount")));
            }
            setDataPerMonth(series);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
