package spirits.lostandfound.util;

import spirits.lostandfound.controllers.BaseController;
import spirits.lostandfound.reference.Language;

import java.io.*;
import java.util.Properties;

public class PropertiesHelper {
    private static final String LANGUAGE_KEY = "language";
    private static Properties settings = new Properties();

    /**
     * Get Language.
     *
     * @return The language the user selected.
     */
    public static Language getLanguage() {
        return Language.valueOf(settings.getProperty(LANGUAGE_KEY));
    }

    /**
     * Set the language to the language selected by the user.
     *
     * @param language The new language.
     */
    public static void setLanguage(Language language) {
        settings.setProperty(LANGUAGE_KEY, language.name());
    }

    /**
     * Return the raw properties object.
     *
     * @return all properties.
     */
    public static Properties getSettings() {
        return settings;
    }

    /**
     * Initializes the properties with default values and custom values
     */
    public static void initProperties() {
        try {
            Properties defaultProps = new Properties();
            defaultProps.load(BaseController.class.getResourceAsStream("/settings.properties"));
            settings = new Properties(defaultProps);
            File file = new File("custom.properties");
            if (file.exists()) {
                FileInputStream fileInputStream = new FileInputStream(file);
                settings.load(fileInputStream);
                fileInputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Save a new properties file.
     */
    public static void saveProperties() {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("custom.properties"), "utf-8"))) {
            settings.store(writer, "Config");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
