/*
 * Creating a pdf of the lost luggage page
 */
package spirits.lostandfound.util;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Create a pdf with data in the corendon style.
 *
 * @author IS102-3
 */
public class PDFExport {

    /**
     * Create a pdf with the given parameters.
     *
     * @param fullName    The full name of the client
     * @param datetime    The current time
     * @param labelNr     The label nr
     * @param flightNr    The flight number
     * @param destination The destination
     * @param phoneNumber The phone number of the client
     * @param mail        The email address of the client.
     * @param street      The street the client lives on.
     * @param zip         The zip code of the client.
     * @param homeNumber  The home number of the client.
     * @param town        The town the client.
     * @param country     The country of the client.
     * @param airport     The airport this report is send from.
     * @param type        The type of lost luggage.
     * @param brand       The brand of the lost luggage.
     * @param color       The color of the lost luggage.
     * @param extra       Extra information about the luggage.
     * @throws IOException Thrown when the file cannot be saved.
     */
    public PDFExport(String fullName, LocalDateTime datetime, String labelNr,
                     String flightNr, String destination, String phoneNumber, String mail,
                     String street, String zip, String homeNumber, String town,
                     String country, String airport, String type, String brand,
                     String color, String extra) throws IOException {

        String[] informationLabelsInPDF = {"Full name", "Label number",
                "Flight number", "Airport", "Phone number", "Mail", "Street",
                "HomeNumber", "Zip", "Town", "Country", "Airport of registration", "Luggage type",
                "Luggage brand", "Lugagge color", "Luggage extra's"};

        String[] informationInPDF = {fullName, labelNr, flightNr,
                destination, phoneNumber, mail, street, homeNumber, zip, town,
                country, airport, type, brand, color, extra};

        //Create document and a page
        PDDocument document = new PDDocument();
        PDPage page = new PDPage();
        document.addPage(page);

        //PDF information
        PDDocumentInformation pdd = document.getDocumentInformation();
        pdd.setTitle("Registered lost luggage information");
        pdd.setAuthor("Corendon");

        //Selecting image
        PDImageXObject pdfImage = PDImageXObject.createFromFile("src/main/resources/images/corendon_logo.png", document);

        //Creating the PDPageContentStream object
        PDPageContentStream contentStream = new PDPageContentStream(document, page);

        //Drawing the image in the PDF document
        contentStream.drawImage(pdfImage, 120, 700);
        System.out.println("Image inserted");

        //Placing Lost Luggage Registration Form text under image
        contentStream.beginText();
        contentStream.setFont(PDType1Font.TIMES_ROMAN, 32);
        contentStream.newLineAtOffset(80, 650);
        String lostFormText = "Lost Luggage Registration Form";
        contentStream.showText(lostFormText);
        contentStream.newLine();
        System.out.println("Title has been inserted");
        contentStream.endText();

        //All of the info that has been given
        contentStream.beginText();
        contentStream.setFont(PDType1Font.TIMES_ROMAN, 16);
        //Width between lines
        contentStream.setLeading(20.5f);
        contentStream.newLineAtOffset(80, 550);

        String text2 = "Date and time: " + datetime;
        contentStream.showText(text2);
        contentStream.newLine();

        //Looping through all the data in the form
        for (int i = 0; i < informationLabelsInPDF.length; i++) {
            String text = informationLabelsInPDF[i] + ": " + informationInPDF[i];
            contentStream.showText(text);
            contentStream.newLine();
        }

        contentStream.endText();
        System.out.println("Content added");

        //Ending the content stream
        contentStream.close();

        //Save the results and ensure that the document is properly closed
        try {
            File pdfDirectory = new File("pdf");
            if (!pdfDirectory.exists()) {
                pdfDirectory.mkdir();
            }
            document.save("pdf/" + fullName + "_RegisteredLuggage_" + LocalDate.now() + ".pdf");
            System.out.println("Document saved.");
        } catch (IOException ex) {
            Logger.getLogger(PDFExport.class.getName()).log(Level.SEVERE, null, ex);
        }
        document.close();
    }
}
