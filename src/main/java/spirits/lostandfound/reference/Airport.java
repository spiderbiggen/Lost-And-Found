package spirits.lostandfound.reference;

import java.util.Arrays;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * FXML Controller has the names of the airports.
 *
 * @author IS102-3
 */
public enum Airport {
    STANDARD("All", "STAND"),
    AMSTERDAM("Amsterdam", "AMS"),
    ANTALYA("Antalya", "AYT"),
    BANJUL("Banjul", "BJL"),
    BODRUM("Bodrum", "BJV"),
    BURGAS("Burgas", "BOJ"),
    CATANIA("Catania", "CTA"),
    CORFU("Corfu Ioannis Kapodistrias", "CFU"),
    DALAMAN("Dalaman", "DLM"),
    DUBAI("Dubai International Airport", "DXB"),
    ENFIDHA("Enfidha-Hammamet", "NBE"),
    FARO("Faro Airport", "FAO"),
    FUERTEVENTURA("Fuerteventura Airport", "FUE"),
    GAZIPASA("Gazipasa - Alanya", "GZP"),
    GRANCANARIA("Gran Canaria Airport", "LPA"),
    HERAKLION("Heraklion Airport", "HER"),
    HURGHADA("Hurghada International Airport", "HRG"),
    ISTANBUL("Istanboel Ataturk", "IST"),
    IZMIR("Izmir Adnan", "ADB"),
    KOS("Kos", "KGS"),
    LANZAROTE("Lanzarote Airport", "ACE"),
    MALAGA("Malaga Airport", "AGP"),
    MARRAKECH("Menera", "RAK"),
    MYTILENE("Mytilini Doysseas Elytis", "MJT"),
    NICOSIA("Nicosia", "ECN"),
    OHRID("St.Paul the Apostle", " OHD"),
    PALMADEMALLORCA("Aeroport de Palma de Mallorca", "PMI"),
    RHODES("Rodos", "RHO"),
    SAMOS("Aristachos of Samos", "SMI"),
    TENERIFE("Aeropuerto de Tenerife Sur", "TFO"),
    ZAKYNTHOS("Dionysios Solomos", "ZTH");

    public static final String AIRPORTS_STANDARD_KEY = "airports_standard";
    private final String code;
    private String name;

    /**
     * Enum constructor.
     *
     * @param name The name of the airport.
     * @param code The IATA Code of the airport.
     */
    Airport(String name, String code) {
        this.name = name;
        this.code = code;
    }

    /**
     * Translate all airports to the new Locale.
     *
     * @param resourceBundle The resource bundle that has all the strings for the given locale.
     */
    public static void setResourceBundle(ResourceBundle resourceBundle) {
        if (resourceBundle.containsKey(AIRPORTS_STANDARD_KEY)) {
            STANDARD.name = resourceBundle.getString(AIRPORTS_STANDARD_KEY);
        }
    }

    /**
     * Returns an Airport when the code matches an Airport.
     *
     * @param code 3 character IATA Code
     * @return an Airport
     */
    public static Airport fromCode(final String code) {
        Optional<Airport> airport = Arrays.stream(Airport.values()).filter(d -> d.code.equals(code)).findFirst();
        return airport.orElseThrow(() -> new IllegalArgumentException("No enum constant."));
    }

    /**
     * Get name.
     *
     * @return The name of the airport.
     */
    public String getName() {
        return name;
    }

    /**
     * Get code.
     *
     * @return the Iata code of the airport.
     */
    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return name;
    }
}
