package spirits.lostandfound.controllers.damage;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import spirits.lostandfound.database.SpiritsJDBC;
import spirits.lostandfound.reference.Strings;

import java.net.URL;
import java.sql.*;
import java.time.LocalDate;
import java.util.ResourceBundle;

import static spirits.lostandfound.reference.Strings.REGISTER_LOST_LUGGAGE;

/**
 * FXML Controller class
 *
 * @author IS102-3
 */
public class ClaimsInProgressController extends DamageBaseController implements Initializable {

    @FXML
    private TableView<Data> tableView;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
        setBreadCrumbs(rb, REGISTER_LOST_LUGGAGE, getCurrentScreen());

        for (TableColumn<Data, ?> tableColumn : tableView.getColumns()) {
            String propertyName = tableColumn.getId();
            if (propertyName != null && !propertyName.isEmpty()) {
                tableColumn.setCellValueFactory(new PropertyValueFactory<>(propertyName));
            }
        }

        tableView.setItems(FXCollections.observableArrayList());

        fillTable();
    }

    /**
     * Fills table with information about luggage, luggagelink and passengers
     */
    private void fillTable() {

        //language=MySQL
        String sql = "SELECT * FROM labels \n" +
                "\n" +
                "LEFT JOIN luggage ON labels.tracking_nr = luggage.tracking_nr \n" +
                "LEFT JOIN luggagelink ON labels.tracking_nr = luggagelink.tracking_nr \n" +
                "LEFT JOIN passengers ON passengers.passenger_id = luggagelink.passenger_id\n" +
                "\n" +
                "WHERE luggagelink.claimed = 1;";

        try (Connection connection = new SpiritsJDBC().createConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    long lostAndFoundID = resultSet.getLong("tracking_nr");
                    String firstName = resultSet.getString("first_name");
                    String tussenvoegsel = resultSet.getString("tussenvoegsel");
                    String lastName = resultSet.getString("last_name");
                    String passengerName = firstName + (tussenvoegsel == null ? "" : " " + tussenvoegsel) + " " + lastName;
                    String labelNumber = resultSet.getString("labelNr");
                    LocalDate dateLost = resultSet.getDate("lost_since").toLocalDate();
                    Date date = resultSet.getDate("sent_date");
                    long tooLate;
                    if (date != null) {
                        LocalDate dateDelivered = date.toLocalDate();
                        tooLate = dateDelivered.toEpochDay() - dateLost.toEpochDay();
                    } else {
                        tooLate = LocalDate.now().toEpochDay() - dateLost.toEpochDay();
                    }
                    LocalDate inProgressSince = resultSet.getDate("claimed_date").toLocalDate();
                    tableView.getItems().add(new Data(lostAndFoundID, passengerName, labelNumber, tooLate, inProgressSince));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected String getCurrentScreen() {
        return Strings.CLAIMS_IN_PROGRESS;
    }

    /**
     * Holds data used to display data in the table.
     */
    public static class Data {
        private final long lostAndFoundID;
        private final String passengerName;
        private final String labelNumber;
        private final long timeTooLate;
        private final LocalDate inProgressSince;

        /**
         * Constructor.
         *
         * @param lostAndFoundID  The id of the luggage.
         * @param passengerName   The name associated with the luggage.
         * @param labelNumber     The label assoicated with luggage.
         * @param timeTooLate     The number of days between date lost and date received.
         * @param inProgressSince The date when this luggage was claimed.
         */
        public Data(long lostAndFoundID, String passengerName, String labelNumber, long timeTooLate, LocalDate inProgressSince) {
            this.lostAndFoundID = lostAndFoundID;
            this.passengerName = passengerName;
            this.labelNumber = labelNumber;
            this.timeTooLate = timeTooLate;
            this.inProgressSince = inProgressSince;
        }

        /**
         * gets lost and found ID
         *
         * @return lost and found ID
         */
        public long getLostAndFoundID() {
            return lostAndFoundID;
        }

        /**
         * gets passenger name
         *
         * @return passenger name
         */
        public String getPassengerName() {
            return passengerName;
        }

        /**
         * gets label number
         *
         * @return label number
         */
        public String getLabelNumber() {
            return labelNumber;
        }

        /**
         * gets time too late
         *
         * @return time too late
         */
        public long getTimeTooLate() {
            return timeTooLate;
        }

        /**
         * gets in progress since
         *
         * @return in progress since
         */
        public LocalDate getInProgressSince() {
            return inProgressSince;
        }
    }


}
