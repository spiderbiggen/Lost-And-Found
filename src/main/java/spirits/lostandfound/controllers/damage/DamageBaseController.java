package spirits.lostandfound.controllers.damage;

import spirits.lostandfound.controllers.BaseController;
import spirits.lostandfound.reference.Layouts;
import spirits.lostandfound.reference.Strings;
import spirits.lostandfound.util.LocalizedString;

/**
 * Less duplicate code.
 *
 * @author IS102-3
 */
public abstract class DamageBaseController extends BaseController {

    @Override
    protected String getResource(LocalizedString pageTitle) {
        switch (pageTitle.getKey()) {
            case Strings.REGISTER_LOST_LUGGAGE:
                return Layouts.REGISTER_LOST_LUGGAGE_CLAIMS;
            case Strings.CLAIMS_IN_PROGRESS:
                return Layouts.CLAIMS_IN_PROGRESS;
            default:
                return null;
        }
    }

}
