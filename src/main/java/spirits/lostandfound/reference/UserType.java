package spirits.lostandfound.reference;

import spirits.lostandfound.util.LocalizedString;

import java.util.Arrays;
import java.util.ResourceBundle;

/**
 * Enum used to keep track of the possible user types and their possible actions.
 *
 * @author IS102-3
 */
public enum UserType {
    STANDARD(null, "Standard"),
    ASSISTANT(Layouts.SEARCH_LUGGAGE, "Assistant"),
    MANAGEMENT(Layouts.MISSING_LUGGAGE_CHART, "Management"),
    DAMAGE(Layouts.REGISTER_LOST_LUGGAGE_CLAIMS, "Damage"),
    ADMIN(Layouts.ADMIN_ROLE_MANAGEMENT, "Admin");

    public final String homePage;
    public final LocalizedString key;

    /**
     * Enum constructor.
     *
     * @param homePage The page that opens when the user logs in.
     * @param name     The name of this user type.
     */
    UserType(final String homePage, final String name) {
        this.homePage = homePage;
        this.key = new LocalizedString("usertype_" + name(), name);
    }

    /**
     * Translate all user types to the new Locale.
     *
     * @param resourceBundle The resource bundle that has all the strings for the given locale.
     */
    public static void setResourceBundle(ResourceBundle resourceBundle) {
        Arrays.stream(values()).forEach(userType -> userType.key.setResourceBundle(resourceBundle));
    }

    @Override
    public String toString() {
        return key.getLocalized();
    }

}
