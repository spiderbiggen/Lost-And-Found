package spirits.lostandfound.controllers.admin;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import spirits.lostandfound.database.SpiritsJDBC;
import spirits.lostandfound.models.User;
import spirits.lostandfound.reference.Airport;
import spirits.lostandfound.reference.Layouts;
import spirits.lostandfound.reference.UserType;
import spirits.lostandfound.util.LayoutHelper;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import static spirits.lostandfound.reference.Strings.MAIN_MENU;

/**
 * FXML Controller class for all role management.
 *
 * @author IS102-3
 */
public class RoleManagementController extends AdminBaseController {

    @FXML
    private TextField firstName;
    @FXML
    private TextField lastName;
    @FXML
    private ComboBox<UserType> functionCb;
    @FXML
    private ComboBox<Airport> airportCb;
    @FXML
    private TextField tussenvoegsel;
    @FXML
    private TextField employeeNrField;
    @FXML
    private TableView<User> tableView;
    private ResourceBundle resourceBundle;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
        setBreadCrumbs(rb, MAIN_MENU);
        UserType.setResourceBundle(rb);
        resourceBundle = rb;

        for (TableColumn<User, ?> tableColumn : tableView.getColumns()) {
            String propertyName = tableColumn.getId();
            if (propertyName != null && !propertyName.isEmpty()) {
                tableColumn.setCellValueFactory(new PropertyValueFactory<>(propertyName));
            }
        }
        airportCb.setItems(FXCollections.observableArrayList(Airport.values()));
        airportCb.setValue(getCurrentUser().getAirport());
        functionCb.setItems(FXCollections.observableArrayList(UserType.values()));
        functionCb.setValue(UserType.STANDARD);
        buildData();
    }

    @Override
    protected String getCurrentScreen() {
        return MAIN_MENU;
    }

    /**
     * Open the create user screen.
     *
     * @param actionEvent the event fired by the Create button.
     */
    @FXML
    private void createUserAction(ActionEvent actionEvent) {
        openScreen(actionEvent, Layouts.CREATE_USER);
    }

    /**
     * Open the edit user screen with the currently selected user.
     *
     * @param actionEvent the event fired by the Edit button.
     */
    @FXML
    private void editUserAction(ActionEvent actionEvent) {
        User user = tableView.getSelectionModel().getSelectedItem();
        if (user == null) return;
        ((EditUserController) openScreen(actionEvent, Layouts.EDIT_USER)).setUser(user);
    }

    /**
     * Delete a user from the database.
     *
     * @param actionEvent the event fired by the delete button.
     * @throws SQLException when the delete query fails.
     */
    @FXML
    private void deleteUserAction(ActionEvent actionEvent) throws SQLException {
        User user = tableView.getSelectionModel().getSelectedItem();
        if (user == null) return;
        String message = resourceBundle.getString("popup_delete_user_message").replace("%s", user.getName());
        Optional<ButtonType> confirm = LayoutHelper.createDoubleActionPopup(resourceBundle.getString("page_admin_user_delete"), message, tableView.getScene().getWindow());
        if (confirm.isPresent() && confirm.get() == ButtonType.OK) {
            String sql = "DELETE FROM users WHERE user_id = ?";
            try (Connection connection = new SpiritsJDBC().createConnection();
                 PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setLong(1, user.getId());
                statement.executeUpdate();
            }
            buildData();
        }
    }

    /**
     * Refresh the tableview with users that correspond to the entered data.
     *
     * @param event the event fired by the search button.
     */
    @FXML
    private void searchUserAction(ActionEvent event) {
        buildData();
    }

    /**
     * Run the database query on a separate thread.
     */
    private void buildData() {
        new Thread(new BuildData()).start();
    }

    /**
     * Class to run database query on a different thread.
     *
     * @author IS102-3
     */
    private class BuildData implements Runnable {

        @Override
        public void run() {
            tableView.setItems(FXCollections.observableArrayList());
            String sql = "SELECT * FROM users \n"
                    + "WHERE ifnull(first_name, '') LIKE concat('%', ?, '%') \n"
                    + "AND ifnull(tussenvoegsel,'') LIKE concat('%', ?, '%') \n"
                    + "AND ifnull(last_name, '') LIKE concat('%', ?, '%') \n"
                    + "AND ifnull(function, '') LIKE concat('%', ?, '%')  \n"
                    + "AND ifnull(airport, '') LIKE concat('%', ?, '%') \n"
                    + "AND ifnull(employee_nr, '') LIKE concat('%', ?, '%')";

            try (Connection connection = new SpiritsJDBC().createConnection();
                 PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, firstName.getText());
                statement.setString(2, tussenvoegsel.getText());
                statement.setString(3, lastName.getText());
                statement.setString(4, functionCb.getValue() == UserType.STANDARD ? "" : functionCb.getValue().toString());
                statement.setString(5, airportCb.getValue() == Airport.STANDARD ? "" : airportCb.getValue().getCode());
                statement.setString(6, employeeNrField.getText());

                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        long userId = resultSet.getLong("user_id");
                        String firstName = resultSet.getString("first_name");
                        String tussenvoegsel = resultSet.getString("tussenvoegsel");
                        String lastName = resultSet.getString("last_name");
                        Airport airport = Airport.fromCode(resultSet.getString("airport"));
                        String email = resultSet.getString("email");
                        UserType function = UserType.valueOf(resultSet.getString("function"));
                        String employeeNr = resultSet.getString("employee_nr");

                        tableView.getItems().add(new User(userId, firstName, tussenvoegsel, lastName, email, airport, function, employeeNr));
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
