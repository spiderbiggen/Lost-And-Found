package spirits.lostandfound.controllers.assistant;

import javafx.fxml.Initializable;
import spirits.lostandfound.controllers.BaseController;
import spirits.lostandfound.reference.Layouts;
import spirits.lostandfound.util.LocalizedString;

import static spirits.lostandfound.reference.Strings.*;

/**
 * Less duplicate code.
 *
 * @author IS102-3
 */
public abstract class AssistantBaseController extends BaseController implements Initializable {

    @Override
    protected String getResource(LocalizedString pageTitle) {
        switch (pageTitle.getKey()) {
            case MAIN_MENU:
                return Layouts.SEARCH_LUGGAGE;
            case REGISTER_LOST_LUGGAGE:
                return Layouts.LOST_LUGGAGE_FORM;
            case SEND_LUGGAGE_SEND:
                return Layouts.SEND_LUGGAGE_SEND;
            default:
                return null;
        }
    }

}
