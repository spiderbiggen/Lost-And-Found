package spirits.lostandfound.controllers.admin;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import spirits.lostandfound.database.SpiritsJDBC;
import spirits.lostandfound.reference.Airport;
import spirits.lostandfound.reference.Layouts;
import spirits.lostandfound.reference.Strings;
import spirits.lostandfound.reference.UserType;
import spirits.lostandfound.util.LayoutHelper;
import spirits.lostandfound.util.SecurityHelper;

import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.ResourceBundle;

import static spirits.lostandfound.reference.Strings.MAIN_MENU;


/**
 * FXML Controller class for creating a user.
 *
 * @author IS102-3
 */
public class CreateUserController extends AdminBaseController {

    @FXML
    private TextField firstNameField;
    @FXML
    private TextField tussenvoegselField;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField emailField;
    @FXML
    private TextField passwordField;

    @FXML
    private ComboBox<UserType> functionCb;
    @FXML
    private TextField employeeNrField;
    @FXML
    private ComboBox<Airport> airportCb;

    private ResourceBundle resourceBundle;

    /**
     * Called when the create button is clicked. inserts the given data into the database as a user. Only when email and password are valid.
     *
     * @param event the event fired by the button.
     */
    @FXML
    private void createUser(ActionEvent event) {
        if (!validatePassword(passwordField) | !validateEmail(emailField)) return;

        Optional<ButtonType> confirm = LayoutHelper.createDoubleActionPopup(resourceBundle.getString("page_admin_user_create"), "", firstNameField.getScene().getWindow());
        if (confirm.isPresent() && confirm.get() == ButtonType.OK) {

            try (Connection connection = new SpiritsJDBC().createConnection();
                 Statement statement = connection.createStatement()) {
                String first = firstNameField.getText();
                String last = lastNameField.getText();
                String tussenvoegsel = this.tussenvoegselField.getText();
                String email = emailField.getText();
                String password = SecurityHelper.securePassword(passwordField.getText(), email);
                String employeeNr = employeeNrField.getText();
                String airport = airportCb.getValue().getCode();
                String function = functionCb.getValue().name();

                final String sql = "INSERT INTO users (employee_nr, first_name, tussenvoegsel, last_name, email,password, airport, function) VALUES ('" + employeeNr + "', '" + first + "','" + tussenvoegsel + "', '" + last + "', '" + email + "', '" + password + "','" + airport + "','" + function + "')";

                statement.executeUpdate(sql);
            } catch (SQLException ex) {
                LayoutHelper.createErrorPopup(resourceBundle.getString("page_admin_user_create"), resourceBundle.getString("popup_create_user_error_message"), firstNameField.getScene().getWindow());
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            openScreen(event, Layouts.ADMIN_ROLE_MANAGEMENT);
        }
    }

    @Override
    protected String getCurrentScreen() {
        return Strings.CREATE_USER;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        setBreadCrumbs(resources, MAIN_MENU, getCurrentScreen());
        resourceBundle = resources;
        airportCb.setItems(FXCollections.observableArrayList(Airport.values()));
        functionCb.setItems(FXCollections.observableArrayList(UserType.values()));
    }
}
