package spirits.lostandfound.reference;

/**
 * Contains Constants that contain the relative path to fxml resources.
 *
 * @author IS102-3
 */
public class Layouts {

    public static final String LOGIN = "/fxml/loginLayout.fxml";
    public static final String PASSWORD_LOST = "/fxml/passwordLost.fxml";
    public static final String LOST_LUGGAGE_FORM = "/fxml/assistant/lostLuggageLayout.fxml";
    public static final String FOUND_LUGGAGE_FORM = "/fxml/assistant/foundLuggageLayout.fxml";
    public static final String SEARCH_LUGGAGE = "/fxml/assistant/searchLuggage.fxml";
    public static final String SEND_LUGGAGE_SEND = "/fxml/assistant/sendLuggageSend.fxml";
    public static final String REGISTER_LOST_LUGGAGE_CLAIMS = "/fxml/damage/registerLostLuggageClaims.fxml";
    public static final String CLAIMS_IN_PROGRESS = "/fxml/damage/claimsInProgress.fxml";
    public static final String EDIT_USER = "/fxml/admin/editUser.fxml";
    public static final String ADMIN_ROLE_MANAGEMENT = "/fxml/admin/roleManagementUser.fxml";
    public static final String CREATE_USER = "/fxml/admin/createUser.fxml";
    public static final String MISSING_LUGGAGE_CHART = "/fxml/management/missingLuggageChart.fxml";
    public static final String LOST_LUGGAGE_MONTH_AND_YEAR = "/fxml/management/lostLuggageMonthAndYear.fxml";
}
