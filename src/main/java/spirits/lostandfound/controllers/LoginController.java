package spirits.lostandfound.controllers;

import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import spirits.lostandfound.reference.Language;
import spirits.lostandfound.reference.Layouts;
import spirits.lostandfound.reference.UserType;
import spirits.lostandfound.util.LayoutHelper;
import spirits.lostandfound.util.LocalizedString;
import spirits.lostandfound.util.PropertiesHelper;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * FXML Controller for logging in and resetting your password.
 */
public class LoginController extends BaseController implements Initializable {

    @FXML
    private VBox vBox;
    @FXML
    private ImageView background;
    @FXML
    private BorderPane bPane;
    @FXML
    private HBox hBox;
    @FXML
    private TextField eMailField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Hyperlink passwordForgottenLabel;
    @FXML
    private Button loginButton;
    @FXML
    private ComboBox<Language> languageDropdown;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        // Add change listener to the text field.
        eMailField.focusedProperty().addListener((obs, oV, nV) -> {
            if (!nV) validateEmail(eMailField);
        });
        if (passwordField != null) {
            passwordField.focusedProperty().addListener((obs, oV, nV) -> {
                if (!nV) validatePassword(passwordField);
            });
        }
        Platform.runLater(() -> {
            loginButton.requestFocus();
            eMailField.setFocusTraversable(true);
            if (passwordField != null) {
                passwordField.setFocusTraversable(true);
            }
        });
        languageDropdown.setItems(FXCollections.observableArrayList(Language.values()));
        languageDropdown.setValue(PropertiesHelper.getLanguage());

        background.fitWidthProperty().bind(bPane.widthProperty().subtract(vBox.widthProperty()));
        background.fitHeightProperty().bind(bPane.heightProperty().subtract(hBox.heightProperty()));

        InvalidationListener viewportChanged = o -> background.setViewport(getBackgroundViewport(background));
        background.fitWidthProperty().addListener(viewportChanged);
        background.fitHeightProperty().addListener(viewportChanged);
    }

    //no breadcrumbs
    @Override
    protected String getCurrentScreen() {
        throw new UnsupportedOperationException();
    }

    //no breadcrumbs
    @Override
    protected String getResource(LocalizedString pageTitle) {
        throw new UnsupportedOperationException();
    }

    /**
     * Try to log the user into the application
     *
     * @param actionEvent The event fired by the Login button.
     */
    @FXML
    private void loginAction(ActionEvent actionEvent) {
        final String email = eMailField.getText().toLowerCase();
        final String password = passwordField.getText();
        final boolean validMail = validateEmail(eMailField);
        final boolean validPassword = validatePassword(passwordField);
        if (!validMail || !validPassword) {
            return;
        }
        UserType userType = login(email, password);
        if (userType == null) {
            setError(eMailField);
            setError(passwordField);
        } else {
            openScreen(actionEvent, userType.homePage);
        }
    }

    /**
     * Open the forgot password screen.
     *
     * @param actionEvent the event fired by clicking on the forgot password hyperlink.
     */
    @FXML
    private void clickedForgotPassword(ActionEvent actionEvent) {
        LoginController controller = (LoginController) openScreen(actionEvent, Layouts.PASSWORD_LOST);
        controller.setEmail(eMailField.getText());
    }

    /**
     * Sets the email address that should appear in the emailField.
     *
     * @param email any valid email address.
     */
    public void setEmail(String email) {
        this.eMailField.setText(email);
    }

    /**
     * Sends an email to the user to reset their password. Only if the email is valid.
     *
     * @param actionEvent The event fired by pressing the reset password button.
     */
    @FXML
    private void resetPasswordAction(ActionEvent actionEvent) {
        if (!validateEmail(eMailField)) return;
        ResourceBundle resourceBundle = getResourceBundle();
        Optional<ButtonType> result = LayoutHelper.createDoubleActionPopup(resourceBundle.getString("password_reset"),
                String.format(resourceBundle.getString("reset_password_popup"), eMailField.getText().trim()), eMailField.getScene().getWindow());

        if (result.isPresent() && result.get() == ButtonType.OK) {
            // SEND EMAIL
            System.out.printf("Sent reset mail to: %s.%n", eMailField.getText().trim());
            LoginController controller = (LoginController) openScreen(actionEvent, Layouts.LOGIN);
            controller.setEmail(eMailField.getText());
        }
    }

    /**
     * Change the language of the application when the user selects a different language from the dropdown.
     *
     * @param actionEvent the event fired by changing the language combobox.
     */
    @FXML
    private void languageSelected(ActionEvent actionEvent) {
        Language language = languageDropdown.getValue();
        PropertiesHelper.setLanguage(language);
        setResourceBundle(LayoutHelper.createResourceBundleForLocale(language.getLocale()));
        loadLocaleSpecificStrings();
    }

    /**
     * Change the language of the login and password reset screens without reloading.
     */
    private void loadLocaleSpecificStrings() {
        ResourceBundle resourceBundle = getResourceBundle();
        eMailField.setPromptText(resourceBundle.getString("contact_email"));
        passwordField.setPromptText(resourceBundle.getString("password"));
        passwordForgottenLabel.setText(resourceBundle.getString("password_forgotten"));
        loginButton.setText(resourceBundle.getString("button_login"));
    }
}
