package spirits.lostandfound.controllers.management;

import spirits.lostandfound.controllers.BaseController;
import spirits.lostandfound.reference.Layouts;
import spirits.lostandfound.reference.Strings;
import spirits.lostandfound.util.LocalizedString;

/**
 * Reduces redundant code in Management sub classes.
 *
 * @author IS102-3
 */
public abstract class ManagementBaseController extends BaseController {

    @Override
    protected String getResource(LocalizedString pageTitle) {
        switch (pageTitle.getKey()) {
            case Strings.LOST_LUGGAGE_MONTH_AND_YEAR:
                return Layouts.LOST_LUGGAGE_MONTH_AND_YEAR;
            case Strings.MISSING_LUGGAGE_PIE_CHART:
                return Layouts.MISSING_LUGGAGE_CHART;
            default:
                return null;
        }
    }
}
