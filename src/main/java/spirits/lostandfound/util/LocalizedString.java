package spirits.lostandfound.util;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Stores a localizedString with unlocalized key, to make it easier to compare Strings
 *
 * @author IS102-3
 */
public class LocalizedString {
    private final String key;
    private String localized;
    private Locale locale;

    /**
     * Constructor.
     *
     * @param key            The key used to create the localized String.
     * @param resourceBundle The {@link ResourceBundle} that has the translations for the given key.
     */
    public LocalizedString(String key, ResourceBundle resourceBundle) {
        this(key, resourceBundle.getString(key), resourceBundle.getLocale());
    }

    /**
     * Constructor.
     *
     * @param key       The key used to create the localized String.
     * @param localized The localized String.
     * @param locale    The {@link Locale} used to create localized String.
     */
    public LocalizedString(String key, String localized, Locale locale) {
        this(key);
        this.localized = localized;
        this.locale = locale;
    }

    /**
     * Constructor.
     *
     * @param key The key used to create the localized String.
     */
    public LocalizedString(String key) {
        this.key = key.toLowerCase();
    }

    /**
     * Constructor. Will default to English Locale.
     *
     * @param key       The key used to create the localized String.
     * @param localized The localized String.
     */
    public LocalizedString(String key, String localized) {
        this(key, localized, Locale.ENGLISH);
    }

    /**
     * If the key exists in the given resourcebundle replace the localization.
     *
     * @param resourceBundle the resourcebundle for a given locale.
     */
    public void setResourceBundle(ResourceBundle resourceBundle) {
        if (resourceBundle.containsKey(key)) {
            this.localized = resourceBundle.getString(key);
            this.locale = resourceBundle.getLocale();
        }
    }

    /**
     * @return The localized String.
     */
    public String getLocalized() {
        return localized;
    }

    /**
     * @return The {@link Locale} used to create localized String.
     */
    public Locale getLocale() {
        return locale;
    }

    @Override
    public int hashCode() {
        return getKey().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocalizedString that = (LocalizedString) o;
        return getKey().equals(that.getKey());
    }

    /**
     * @return The key used to create the localized String.
     */
    public String getKey() {
        return key;
    }

    @Override
    public String toString() {
        return localized;
    }
}
