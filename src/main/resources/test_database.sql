-- MySQL dump 10.13  Distrib 5.7.20, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: lost_and_found_test
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresses` (
  `address_id`   BIGINT(20) NOT NULL AUTO_INCREMENT,
  `street`       TEXT,
  `house_number` TINYTEXT,
  `zipcode`      VARCHAR(10)         DEFAULT NULL,
  `city`         TEXT,
  `country`      TEXT,
  PRIMARY KEY (`address_id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 12
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresses`
--

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses`
  DISABLE KEYS */;
INSERT INTO `addresses` (`address_id`, `street`, `house_number`, `zipcode`, `city`, `country`)
VALUES (10, NULL, NULL, NULL, NULL, NULL), (11, '', '', '', '', '');
/*!40000 ALTER TABLE `addresses`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addresslink`
--

DROP TABLE IF EXISTS `addresslink`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresslink` (
  `passenger_id` BIGINT(20) NOT NULL,
  `address_id`   BIGINT(20) NOT NULL,
  `from_date`    DATE       NOT NULL,
  `until_date`   DATE DEFAULT NULL,
  PRIMARY KEY (`passenger_id`, `address_id`),
  KEY `addresslink_addresses_address_id_fk` (`address_id`),
  CONSTRAINT `addresslink_addresses_address_id_fk` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`address_id`),
  CONSTRAINT `addresslink_passengers_passenger_id_fk` FOREIGN KEY (`passenger_id`) REFERENCES `passengers` (`passenger_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresslink`
--

LOCK TABLES `addresslink` WRITE;
/*!40000 ALTER TABLE `addresslink`
  DISABLE KEYS */;
INSERT INTO `addresslink` (`passenger_id`, `address_id`, `from_date`, `until_date`)
VALUES (248283, 10, '2018-01-15', NULL), (248284, 11, '2018-01-15', NULL);
/*!40000 ALTER TABLE `addresslink`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `airports`
--

DROP TABLE IF EXISTS `airports`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `airports` (
  `iata_code` VARCHAR(3) NOT NULL,
  `name`      TEXT,
  `country`   TEXT,
  `city`      TEXT,
  PRIMARY KEY (`iata_code`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `airports`
--

LOCK TABLES `airports` WRITE;
/*!40000 ALTER TABLE `airports`
  DISABLE KEYS */;
INSERT INTO `airports` (`iata_code`, `name`, `country`, `city`)
VALUES
  ('ACE', 'Lanzarote Airport', 'Spain', 'Lanzarote'), ('ADB', 'Izmir Adnan', 'Turkey', 'Izmir'),
  ('AGP', 'Malaga Airport', 'Spain', 'Malaga'), ('AMS', 'Schiphol', 'The Netherlands', 'Amsterdam'),
  ('AYT', 'Antalya', 'Turkey', 'Antalya'), ('BJL', 'Banjul International Airport', 'Gambia', 'Banjul'),
  ('BJV', 'Milas-Bodrum Airport', 'Turkey', 'Bodrum'), ('BOJ', 'Burgas Airport', 'Bulgaria', 'Burgas'),
  ('CFU', 'Corfu Ioannis Kapodistrias', 'Greece', 'Corfu'),
  ('CTA', 'Catania International Airport', 'Italy', 'Catania'), ('DLM', 'Dalaman', 'Turkey', 'Dalaman'),
  ('DXB', 'Dubai International Airport', 'United Arab Emirates', 'Dubai'), ('ECN', 'Nicosia', 'Cyprus', 'Nicosia'),
  ('FAO', 'Faro Airport', 'Portugal', 'Faro'), ('FUE', 'Fuerteventura Airport', 'Spain', 'Fuerteventura'),
  ('GZP', 'Gazipasa - Alanya', 'Turkey', 'Gazipasa-Alanya'), ('HER', 'Heraklion Airport ', 'Greece', 'Heraklion'),
  ('HRG', 'Hurghada International Airport', 'Egypt', 'Hurghada'), ('IST', 'Istanboel Ataturk', 'Turkey', 'Istanbul'),
  ('KGS', 'Kos', 'Greece', 'Kos'), ('LPA', 'Gran Canaria Airport', 'Spain', 'Gran Canaria'),
  ('MJT', 'Mytilini Odysseas Elytis', 'Greece', 'Mytilene'), ('NBE', 'Enfidha–Hammamet', 'Tunisia', 'Enfidha'),
  ('OHD', 'St.Paul the Apostle', 'Macedonia', 'Ohrid'),
  ('PMI', 'Aeroport de Palma de Mallorca', 'Spain', 'Palma de Mallorca'), ('RAK', 'Menara', 'Morocco', 'Marrakech'),
  ('RHO', 'Rodos ', 'Greece', 'Rhodes'), ('SMI', 'Aristarchos of Samos', 'Greece', 'Samos'),
  ('TFO', 'Aeropuerto de Tenerife Sur', 'Spain', 'Tenerife'), ('ZTH', 'Dionysios Solomos', 'Greece', 'Zakynthos');
/*!40000 ALTER TABLE `airports`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `labels`
--

DROP TABLE IF EXISTS `labels`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `labels` (
  `labelNr`     VARCHAR(12) NOT NULL,
  `tracking_nr` BIGINT(20)  NOT NULL,
  `flight_nr`   TINYTEXT,
  `destination` VARCHAR(3) DEFAULT NULL,
  PRIMARY KEY (`labelNr`, `tracking_nr`),
  KEY `tracking_nr` (`tracking_nr`),
  KEY `destination` (`destination`),
  CONSTRAINT `labels_airports_iata_code_fk` FOREIGN KEY (`destination`) REFERENCES `airports` (`iata_code`),
  CONSTRAINT `labels_luggage_tracking_nr_fk` FOREIGN KEY (`tracking_nr`) REFERENCES `luggage` (`tracking_nr`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `labels`
--

LOCK TABLES `labels` WRITE;
/*!40000 ALTER TABLE `labels`
  DISABLE KEYS */;
INSERT INTO `labels` (`labelNr`, `tracking_nr`, `flight_nr`, `destination`)
VALUES
  ('', 399494, NULL, NULL), ('', 399495, NULL, NULL), ('', 399496, NULL, NULL), ('', 399497, NULL, NULL),
  ('', 399498, NULL, NULL), ('', 399499, NULL, NULL), ('', 399500, NULL, NULL), ('', 399501, NULL, NULL),
  ('', 399502, NULL, NULL), ('', 399503, NULL, NULL), ('', 399504, NULL, NULL), ('', 399505, NULL, NULL),
  ('', 399506, NULL, NULL), ('', 399507, NULL, NULL), ('', 399508, NULL, NULL), ('', 399509, NULL, NULL),
  ('', 399510, NULL, NULL), ('', 399511, NULL, NULL), ('', 399512, NULL, NULL), ('', 399550, NULL, NULL),
  ('', 399551, NULL, NULL), ('', 399552, NULL, NULL), ('', 399553, NULL, NULL), ('', 399554, NULL, NULL),
  ('', 399555, NULL, NULL), ('', 399556, NULL, NULL), ('', 399557, NULL, NULL), ('', 399558, NULL, NULL),
  ('', 399559, NULL, NULL), ('', 399560, NULL, NULL), ('', 399561, NULL, NULL), ('', 399562, NULL, NULL),
  ('', 399563, NULL, NULL), ('', 399564, NULL, NULL), ('', 399565, NULL, NULL), ('', 399566, NULL, NULL),
  ('', 399567, NULL, NULL), ('', 399568, NULL, NULL), ('', 399569, NULL, NULL), ('1153481443', 399466, NULL, NULL),
  ('1153481443', 399522, NULL, NULL), ('1228852049', 399602, NULL, NULL), ('12345', 399619, '12345', 'BJV'),
  ('12345', 399620, '12345', 'BJV'), ('12345', 399621, '', 'AMS'), ('1287253569', 399570, NULL, NULL),
  ('1297047756', 399467, NULL, NULL), ('1297047756', 399523, NULL, NULL), ('1321391290', 399468, NULL, NULL),
  ('1321391290', 399524, NULL, NULL), ('1432102498', 399613, NULL, NULL), ('1510036710', 399606, NULL, NULL),
  ('1557534916', 399469, NULL, NULL), ('1557534916', 399525, NULL, NULL), ('1688722916', 399470, NULL, NULL),
  ('1688722916', 399526, NULL, NULL), ('1812850798', 399611, NULL, NULL), ('1816584362', 399595, NULL, NULL),
  ('1957629307', 399471, NULL, NULL), ('1957629307', 399527, NULL, NULL), ('1963627893', 399472, NULL, NULL),
  ('1963627893', 399528, NULL, NULL), ('1963674652', 399609, NULL, NULL), ('2057628850', 399586, NULL, NULL),
  ('2098310448', 399616, NULL, NULL), ('2771896151', 399473, NULL, NULL), ('2771896151', 399529, NULL, NULL),
  ('2798016522', 399590, NULL, NULL), ('2905609937', 399582, NULL, NULL), ('2973839061', 399474, NULL, NULL),
  ('2973839061', 399530, NULL, NULL), ('3125977624', 399573, NULL, NULL), ('3217712035', 399475, NULL, NULL),
  ('3217712035', 399531, NULL, NULL), ('3260024106', 399476, NULL, NULL), ('3260024106', 399532, NULL, NULL),
  ('3299609395', 399477, NULL, NULL), ('3299609395', 399533, NULL, NULL), ('3309446657', 399580, NULL, NULL),
  ('3341356738', 399593, NULL, NULL), ('3479694767', 399579, NULL, NULL), ('3668297125', 399603, NULL, NULL),
  ('3794786696', 399478, NULL, NULL), ('3794786696', 399534, NULL, NULL), ('3811025314', 399576, NULL, NULL),
  ('4067442816', 399585, NULL, NULL), ('4177204091', 399598, NULL, NULL), ('4497537549', 399479, NULL, NULL),
  ('4497537549', 399535, NULL, NULL), ('4677517071', 399588, NULL, NULL), ('4811246270', 399480, NULL, NULL),
  ('4811246270', 399536, NULL, NULL), ('4996467985', 399584, NULL, NULL), ('5364334705', 399481, NULL, NULL),
  ('5364334705', 399537, NULL, NULL), ('5432047955', 399571, NULL, NULL), ('5472393034', 399596, NULL, NULL),
  ('5577194021', 399618, NULL, NULL), ('5703242384', 399482, NULL, NULL), ('5703242384', 399538, NULL, NULL),
  ('5707997828', 399574, NULL, NULL), ('5794600426', 399587, NULL, NULL), ('5877130095', 399483, NULL, NULL),
  ('5877130095', 399539, NULL, NULL), ('5941005772', 399484, NULL, NULL), ('5941005772', 399540, NULL, NULL),
  ('5955243509', 399485, NULL, NULL), ('5955243509', 399541, NULL, NULL), ('5955555012', 399599, NULL, NULL),
  ('6005962577', 399605, NULL, NULL), ('6105657827', 399604, NULL, NULL), ('6175011250', 399486, NULL, NULL),
  ('6175011250', 399542, NULL, NULL), ('6327958189', 399487, NULL, NULL), ('6327958189', 399543, NULL, NULL),
  ('6377992003', 399488, NULL, NULL), ('6377992003', 399544, NULL, NULL), ('6409963930', 399591, NULL, NULL),
  ('6430684502', 399612, NULL, NULL), ('6544610514', 399577, NULL, NULL), ('6644903667', 399608, NULL, NULL),
  ('6758695111', 399607, NULL, NULL), ('6895742082', 399489, NULL, NULL), ('6895742082', 399545, NULL, NULL),
  ('6985899650', 399597, NULL, NULL), ('7477818216', 399592, NULL, NULL), ('7500811482', 399575, NULL, NULL),
  ('7512744468', 399581, NULL, NULL), ('7620963089', 399490, NULL, NULL), ('7620963089', 399546, NULL, NULL),
  ('7666720070', 399615, NULL, NULL), ('7686938228', 399491, NULL, NULL), ('7686938228', 399547, NULL, NULL),
  ('7710147083', 399614, NULL, NULL), ('7801764125', 399617, NULL, NULL), ('7936798551', 399589, NULL, NULL),
  ('7975308223', 399492, NULL, NULL), ('7975308223', 399548, NULL, NULL), ('8677569471', 399600, NULL, NULL),
  ('8729699133', 399578, NULL, NULL), ('8731882468', 399601, NULL, NULL), ('9330259675', 399610, NULL, NULL),
  ('9358669274', 399572, NULL, NULL), ('9470662339', 399594, NULL, NULL), ('9868434229', 399583, NULL, NULL),
  ('9896064347', 399493, NULL, NULL), ('9896064347', 399549, NULL, NULL);
/*!40000 ALTER TABLE `labels`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `luggage`
--

DROP TABLE IF EXISTS `luggage`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `luggage` (
  `tracking_nr` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `lost_since`  DATETIME            DEFAULT NULL,
  `type`        TEXT,
  `brand`       TEXT,
  `color`       TEXT,
  `extra`       LONGTEXT,
  `found`       TINYINT(4) NOT NULL DEFAULT '0',
  `airport`     VARCHAR(3)          DEFAULT NULL,
  PRIMARY KEY (`tracking_nr`),
  KEY `luggage_airports_iata_code_fk` (`airport`),
  CONSTRAINT `luggage_airports_iata_code_fk` FOREIGN KEY (`airport`) REFERENCES `airports` (`iata_code`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 399622
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `luggage`
--

LOCK TABLES `luggage` WRITE;
/*!40000 ALTER TABLE `luggage`
  DISABLE KEYS */;
INSERT INTO `luggage` (`tracking_nr`, `lost_since`, `type`, `brand`, `color`, `extra`, `found`, `airport`)
VALUES
  (399466, '2016-06-17 00:00:00', 'BOX', '', 'CREAM', '', 1, 'AYT'),
  (399467, '2016-09-07 00:00:00', 'SUITCASE', 'Perry Mackin', 'BLUE_GREEN', 'holywood sticker', 1, 'AYT'),
  (399468, '2016-07-04 00:00:00', 'BRIEFCASE', 'Eastsport', 'PINK', 'red-bull sticker', 1, 'AYT'),
  (399469, '2016-09-09 00:00:00', 'SPORTS_BAG', 'Baggallini', 'OLIVE', '', 1, 'AYT'),
  (399470, '2015-11-25 00:00:00', 'BAG', 'Baggallini', 'BLACK', 'Orange stripes', 1, 'AYT'),
  (399471, '2016-09-10 00:00:00', 'SUITCASE', 'Ivy', 'ORANGE', '', 1, 'AYT'),
  (399472, '2016-09-09 00:00:00', 'SUITCASE', 'Nautica', 'YELLOW', 'many scratches', 1, 'AYT'),
  (399473, '2015-10-20 00:00:00', 'BRIEFCASE', 'Ivy', 'BLUE', '', 1, 'AYT'),
  (399474, '2016-09-08 00:00:00', 'BOX', '', 'VIOLET', 'chain lock', 1, 'AYT'),
  (399475, '2016-08-23 00:00:00', 'SUITCASE', 'Travel Gear', 'LIGHT_BROWN', 'ajax stickers', 1, 'AYT'),
  (399476, '2016-03-13 00:00:00', 'BACKPACK', 'Hedgren', 'RED', 'football stickers', 1, 'AYT'),
  (399477, '2016-09-02 00:00:00', 'SPORTS_BAG', 'Fjallraven', 'LIGHT_BROWN', '', 1, 'AYT'),
  (399478, '2016-01-17 00:00:00', 'SUITCASE', 'Glove It', 'VIOLET', '', 1, 'AYT'),
  (399479, '2016-09-04 00:00:00', 'BRIEFCASE', 'Glove It', 'PURPLE', '', 1, 'AYT'),
  (399480, '2016-08-24 00:00:00', 'BAG', 'Fjallraven', 'GRAY', 'Orange stripes', 1, 'AYT'),
  (399481, '2016-08-31 00:00:00', 'BACKPACK', 'Travel Gear', 'WHITE', '', 1, 'AYT'),
  (399482, '2016-07-19 00:00:00', 'BAG', 'Samsonite', 'BROWN', 'Bicycle stickers', 1, 'AYT'),
  (399483, '2016-08-11 00:00:00', 'BRIEFCASE', 'Baggallini', 'DARK_BLUE', 'red-bull sticker', 1, 'AYT'),
  (399484, '2016-07-25 00:00:00', 'BOX', '', 'LIGHT_GREEN', '', 1, 'AYT'),
  (399485, '2016-07-15 00:00:00', 'SUITCASE', 'Everest', 'LIGHT_BLUE', '', 1, 'AYT'),
  (399486, '2016-08-06 00:00:00', 'SPORTS_BAG', 'Samsonite', 'DARK_BROWN', '', 1, 'AYT'),
  (399487, '2016-09-08 00:00:00', 'BACKPACK', 'Perry Mackin', 'GREEN', '', 1, 'AYT'),
  (399488, '2016-09-10 00:00:00', 'BRIEFCASE', 'Everest', 'LIGHT_BLUE', '', 1, 'AYT'),
  (399489, '2016-06-18 00:00:00', 'SPORTS_BAG', 'Briggs', 'DARK_RED', '', 1, 'AYT'),
  (399490, '2016-05-24 00:00:00', 'SUITCASE', 'Hedgren', 'CREAM', '', 1, 'AYT'),
  (399491, '2016-09-10 00:00:00', 'BAG', 'AmeriLeather', 'DARK_BLUE', 'Olympic rings', 1, 'AYT'),
  (399492, '2016-04-13 00:00:00', 'BAG', 'Delsey', 'BLUE', 'Olympic rings', 1, 'AYT'),
  (399493, '2016-09-10 00:00:00', 'SPORTS_BAG', 'AmeriLeather', 'DARK_BROWN', 'BRT television sticker', 1, 'AYT'),
  (399494, '2016-09-01 00:00:00', 'BOX', '', 'PINK', '', 1, 'AYT'),
  (399495, '2016-09-06 00:00:00', 'BOX', '', 'LIGHT_GREEN', '', 1, 'AYT'),
  (399496, '2016-09-07 00:00:00', 'BAG', 'Eastsport', 'DARK_GRAY', 'Orange stripes', 1, 'AYT'),
  (399497, '2016-09-08 00:00:00', 'SPORTS_BAG', 'Eastsport', 'WHITE', '', 1, 'AYT'),
  (399498, '2016-09-09 00:00:00', 'BOX', '', 'GRAY', 'red name tag', 1, 'AYT'),
  (399499, '2016-09-09 00:00:00', 'BACKPACK', 'Nautica', 'OLIVE', '', 1, 'AYT'),
  (399500, '2015-12-25 00:00:00', 'BOX', '', 'LIGHT_GRAY', '', 1, 'AYT'),
  (399501, '2016-09-09 00:00:00', 'BRIEFCASE', 'Hedgren', 'BROWN', 'ajax stickers', 1, 'AYT'),
  (399502, '2016-09-10 00:00:00', 'BOX', '', 'PURPLE', '', 1, 'AYT'),
  (399503, '2016-09-10 00:00:00', 'BACKPACK', 'Glove It', 'BLUE_GREEN', '', 1, 'AYT'),
  (399504, '2016-09-10 00:00:00', 'BOX', '', 'DARK_GRAY', 'frech national flag sticker', 1, 'AYT'),
  (399505, '2016-02-10 00:00:00', 'BOX', '', 'LIGHT_GRAY', '', 1, 'AYT'),
  (399506, '2016-04-27 00:00:00', 'BACKPACK', 'Ivy', 'RED', '', 1, 'AYT'),
  (399507, '2016-04-30 00:00:00', 'BOX', '', 'BLACK', 'broken lock', 1, 'AYT'),
  (399508, '2015-12-25 00:00:00', 'SPORTS_BAG', 'Delsey', 'GREEN', '', 1, 'AYT'),
  (399509, '2016-05-03 00:00:00', 'BRIEFCASE', 'Fjallraven', 'ORANGE', '', 1, 'AYT'),
  (399510, '2016-05-14 00:00:00', 'BOX', '', 'DARK_GREEN', 'duvel sticker', 1, 'AYT'),
  (399511, '2016-06-04 00:00:00', 'BAG', 'Briggs', 'YELLOW', 'Olympic rings', 1, 'AYT'),
  (399512, '2016-07-09 00:00:00', 'BOX', '', 'DARK_RED', '', 1, 'AYT'),
  (399513, '2016-08-17 00:00:00', 'BACKPACK', 'Everest', 'DARK_GREEN', '', 1, 'AYT'),
  (399522, '2016-06-17 00:00:00', 'BOX', '', 'CREAM', '', 1, 'AYT'),
  (399523, '2016-09-07 00:00:00', 'SUITCASE', 'Perry Mackin', 'BLUE_GREEN', 'holywood sticker', 1, 'AYT'),
  (399524, '2016-07-04 00:00:00', 'BRIEFCASE', 'Eastsport', 'PINK', 'red-bull sticker', 1, 'AYT'),
  (399525, '2016-09-09 00:00:00', 'SPORTS_BAG', 'Baggallini', 'OLIVE', '', 1, 'AYT'),
  (399526, '2015-11-25 00:00:00', 'BAG', 'Baggallini', 'BLACK', 'Orange stripes', 1, 'AYT'),
  (399527, '2016-09-10 00:00:00', 'SUITCASE', 'Ivy', 'ORANGE', '', 1, 'AYT'),
  (399528, '2016-09-09 00:00:00', 'SUITCASE', 'Nautica', 'YELLOW', 'many scratches', 1, 'AYT'),
  (399529, '2015-10-20 00:00:00', 'BRIEFCASE', 'Ivy', 'BLUE', '', 1, 'AYT'),
  (399530, '2016-09-08 00:00:00', 'BOX', '', 'VIOLET', 'chain lock', 1, 'AYT'),
  (399531, '2016-08-23 00:00:00', 'SUITCASE', 'Travel Gear', 'LIGHT_BROWN', 'ajax stickers', 1, 'AYT'),
  (399532, '2016-03-13 00:00:00', 'BACKPACK', 'Hedgren', 'RED', 'football stickers', 1, 'AYT'),
  (399533, '2016-09-02 00:00:00', 'SPORTS_BAG', 'Fjallraven', 'LIGHT_BROWN', '', 1, 'AYT'),
  (399534, '2016-01-17 00:00:00', 'SUITCASE', 'Glove It', 'VIOLET', '', 1, 'AYT'),
  (399535, '2016-09-04 00:00:00', 'BRIEFCASE', 'Glove It', 'PURPLE', '', 1, 'AYT'),
  (399536, '2016-08-24 00:00:00', 'BAG', 'Fjallraven', 'GRAY', 'Orange stripes', 1, 'AYT'),
  (399537, '2016-08-31 00:00:00', 'BACKPACK', 'Travel Gear', 'WHITE', '', 1, 'AYT'),
  (399538, '2016-07-19 00:00:00', 'BAG', 'Samsonite', 'BROWN', 'Bicycle stickers', 1, 'AYT'),
  (399539, '2016-08-11 00:00:00', 'BRIEFCASE', 'Baggallini', 'DARK_BLUE', 'red-bull sticker', 1, 'AYT'),
  (399540, '2016-07-25 00:00:00', 'BOX', '', 'LIGHT_GREEN', '', 1, 'AYT'),
  (399541, '2016-07-15 00:00:00', 'SUITCASE', 'Everest', 'LIGHT_BLUE', '', 1, 'AYT'),
  (399542, '2016-08-06 00:00:00', 'SPORTS_BAG', 'Samsonite', 'DARK_BROWN', '', 1, 'AYT'),
  (399543, '2016-09-08 00:00:00', 'BACKPACK', 'Perry Mackin', 'GREEN', '', 1, 'AYT'),
  (399544, '2016-09-10 00:00:00', 'BRIEFCASE', 'Everest', 'LIGHT_BLUE', '', 1, 'AYT'),
  (399545, '2016-06-18 00:00:00', 'SPORTS_BAG', 'Briggs', 'DARK_RED', '', 1, 'AYT'),
  (399546, '2016-05-24 00:00:00', 'SUITCASE', 'Hedgren', 'CREAM', '', 1, 'AYT'),
  (399547, '2016-09-10 00:00:00', 'BAG', 'AmeriLeather', 'DARK_BLUE', 'Olympic rings', 1, 'AYT'),
  (399548, '2016-04-13 00:00:00', 'BAG', 'Delsey', 'BLUE', 'Olympic rings', 1, 'AYT'),
  (399549, '2016-09-10 00:00:00', 'SPORTS_BAG', 'AmeriLeather', 'DARK_BROWN', 'BRT television sticker', 1, 'AYT'),
  (399550, '2016-09-01 00:00:00', 'BOX', '', 'PINK', '', 1, 'AYT'),
  (399551, '2016-09-06 00:00:00', 'BOX', '', 'LIGHT_GREEN', '', 1, 'AYT'),
  (399552, '2016-09-07 00:00:00', 'BAG', 'Eastsport', 'DARK_GRAY', 'Orange stripes', 1, 'AYT'),
  (399553, '2016-09-08 00:00:00', 'SPORTS_BAG', 'Eastsport', 'WHITE', '', 1, 'AYT'),
  (399554, '2016-09-09 00:00:00', 'BOX', '', 'GRAY', 'red name tag', 1, 'AYT'),
  (399555, '2016-09-09 00:00:00', 'BACKPACK', 'Nautica', 'OLIVE', '', 1, 'AYT'),
  (399556, '2015-12-25 00:00:00', 'BOX', '', 'LIGHT_GRAY', '', 1, 'AYT'),
  (399557, '2016-09-09 00:00:00', 'BRIEFCASE', 'Hedgren', 'BROWN', 'ajax stickers', 1, 'AYT'),
  (399558, '2016-09-10 00:00:00', 'BOX', '', 'PURPLE', '', 1, 'AYT'),
  (399559, '2016-09-10 00:00:00', 'BACKPACK', 'Glove It', 'BLUE_GREEN', '', 1, 'AYT'),
  (399560, '2016-09-10 00:00:00', 'BOX', '', 'DARK_GRAY', 'frech national flag sticker', 1, 'AYT'),
  (399561, '2016-02-10 00:00:00', 'BOX', '', 'LIGHT_GRAY', '', 1, 'AYT'),
  (399562, '2016-04-27 00:00:00', 'BACKPACK', 'Ivy', 'RED', '', 1, 'AYT'),
  (399563, '2016-04-30 00:00:00', 'BOX', '', 'BLACK', 'broken lock', 1, 'AYT'),
  (399564, '2015-12-25 00:00:00', 'SPORTS_BAG', 'Delsey', 'GREEN', '', 1, 'AYT'),
  (399565, '2016-05-03 00:00:00', 'BRIEFCASE', 'Fjallraven', 'ORANGE', '', 1, 'AYT'),
  (399566, '2016-05-14 00:00:00', 'BOX', '', 'DARK_GREEN', 'duvel sticker', 1, 'AYT'),
  (399567, '2016-06-04 00:00:00', 'BAG', 'Briggs', 'YELLOW', 'Olympic rings', 1, 'AYT'),
  (399568, '2016-07-09 00:00:00', 'BOX', '', 'DARK_RED', '', 1, 'AYT'),
  (399569, '2016-08-17 00:00:00', 'BACKPACK', 'Everest', 'DARK_GREEN', '', 1, 'AYT'),
  (399570, '2016-09-12 00:00:00', 'BRIEFCASE', 'Samsonite', 'PINK', '', 1, 'AGP'),
  (399571, '2016-09-12 00:00:00', 'SPORTS_BAG', 'Eastsport', 'DARK_RED', '', 1, 'AGP'),
  (399572, '2016-09-12 00:00:00', 'OTHER', '', 'CREAM', 'aluminio', 1, 'AGP'),
  (399573, '2016-09-12 00:00:00', 'BRIEFCASE', 'Perry Mackin', 'WHITE', 'candado', 1, 'AGP'),
  (399574, '2016-09-12 00:00:00', 'BOX', '', 'DARK_BROWN', '', 1, 'AGP'),
  (399575, '2016-09-12 00:00:00', 'BAG', 'Hedgren', 'LIGHT_GRAY', '', 1, 'AGP'),
  (399576, '2016-09-12 00:00:00', 'CHEST', '', 'LIGHT_GREEN', 'aluminio', 1, 'AGP'),
  (399577, '2016-09-12 00:00:00', 'SPORTS_BAG', 'Baggallini', 'GREEN', 'etiqueta de nombre roja', 1, 'AGP'),
  (399578, '2016-09-11 00:00:00', 'CHEST', '', 'PURPLE', '', 1, 'AGP'),
  (399579, '2016-09-11 00:00:00', 'BAG', 'AmeriLeather', 'OLIVE', '', 1, 'AGP'),
  (399580, '2016-09-11 00:00:00', 'BAG', 'Glove It', 'OLIVE', 'rayas anaranjadas', 1, 'AGP'),
  (399581, '2016-09-11 00:00:00', 'BOX', '', 'RED', '', 1, 'AGP'),
  (399582, '2016-09-11 00:00:00', 'OTHER', '', 'CREAM', '', 1, 'AGP'),
  (399583, '2016-09-10 00:00:00', 'SUITCASE', 'Samsonite', 'BLUE', '', 1, 'AGP'),
  (399584, '2016-09-10 00:00:00', 'CHEST', '', 'PURPLE', '', 1, 'AGP'),
  (399585, '2016-09-10 00:00:00', 'BOX', '', 'RED', 'Praxis', 1, 'AGP'),
  (399586, '2016-09-10 00:00:00', 'SUITCASE', 'Hedgren', 'YELLOW', '', 1, 'AGP'),
  (399587, '2016-09-09 00:00:00', 'CHEST', '', 'BLACK', '', 1, 'AGP'),
  (399588, '2016-09-09 00:00:00', 'BACKPACK', 'Nautica', 'BROWN', '', 1, 'AGP'),
  (399589, '2016-09-09 00:00:00', 'OTHER', '', 'GRAY', '', 1, 'AGP'),
  (399590, '2016-09-08 00:00:00', 'BOX', '', 'BLUE_GREEN', 'candado', 1, 'AGP'),
  (399591, '2016-09-08 00:00:00', 'BACKPACK', 'Fjallraven', 'DARK_BLUE', '', 1, 'AGP'),
  (399592, '2016-09-07 00:00:00', 'BAG', 'Glove It', 'LIGHT_BLUE', 'keukenhof pegatina', 1, 'AGP'),
  (399593, '2016-09-04 00:00:00', 'SUITCASE', 'Fjallraven', 'BLUE', 'etiqueta de nombre roja', 1, 'AGP'),
  (399594, '2016-09-02 00:00:00', 'OTHER', '', 'VIOLET', '', 1, 'AGP'),
  (399595, '2016-09-01 00:00:00', 'BRIEFCASE', 'Perry Mackin', 'PINK', '', 1, 'AGP'),
  (399596, '2016-08-31 00:00:00', 'SPORTS_BAG', 'Eastsport', 'DARK_RED', '', 1, 'AGP'),
  (399597, '2016-08-24 00:00:00', 'BACKPACK', 'Nautica', 'ORANGE', 'rayas anaranjadas', 1, 'AGP'),
  (399598, '2016-08-23 00:00:00', 'SUITCASE', 'Baggallini', 'YELLOW', 'carre pegatina', 1, 'AGP'),
  (399599, '2016-08-11 00:00:00', 'SUITCASE', 'Ivy', 'CREAM', '', 1, 'AGP'),
  (399600, '2016-08-06 00:00:00', 'CHEST', '', 'BLACK', '', 1, 'AGP'),
  (399601, '2016-07-25 00:00:00', 'BRIEFCASE', 'Glove It', 'WHITE', '', 1, 'AGP'),
  (399602, '2016-07-19 00:00:00', 'SPORTS_BAG', 'Delsey', 'LIGHT_BROWN', '', 1, 'AGP'),
  (399603, '2016-07-15 00:00:00', 'BOX', '', 'DARK_BROWN', '', 1, 'AGP'),
  (399604, '2016-06-18 00:00:00', 'BACKPACK', 'Hedgren', 'BROWN', '', 1, 'AGP'),
  (399605, '2016-06-17 00:00:00', 'BAG', 'Fjallraven', 'LIGHT_GRAY', '', 1, 'AGP'),
  (399606, '2016-06-04 00:00:00', 'OTHER', '', 'GRAY', 'anillos olímpicos', 1, 'AGP'),
  (399607, '2016-05-24 00:00:00', 'CHEST', '', 'LIGHT_GREEN', '', 1, 'AGP'),
  (399608, '2016-05-14 00:00:00', 'BRIEFCASE', 'Delsey', 'DARK_GREEN', '', 1, 'AGP'),
  (399609, '2016-05-03 00:00:00', 'SPORTS_BAG', 'Briggs', 'GREEN', 'Praxis', 1, 'AGP'),
  (399610, '2016-04-27 00:00:00', 'BACKPACK', 'Briggs', 'DARK_BLUE', '', 1, 'AGP'),
  (399611, '2016-04-13 00:00:00', 'BAG', 'Everest', 'LIGHT_BLUE', 'rayas anaranjadas', 1, 'AGP'),
  (399612, '2016-03-13 00:00:00', 'SUITCASE', 'Ivy', 'DARK_GRAY', '', 1, 'AGP'),
  (399613, '2016-02-10 00:00:00', 'OTHER', '', 'VIOLET', '', 1, 'AGP'),
  (399614, '2016-01-17 00:00:00', 'BACKPACK', 'AmeriLeather', 'ORANGE', 'rayas anaranjadas', 1, 'AGP'),
  (399615, '2015-12-25 00:00:00', 'BOX', '', 'BLUE_GREEN', '', 1, 'AGP'),
  (399616, '2015-12-25 00:00:00', 'SPORTS_BAG', 'Fjallraven', 'LIGHT_BROWN', '', 1, 'AGP'),
  (399617, '2015-11-25 00:00:00', 'SUITCASE', 'Travel Gear', 'DARK_GRAY', 'rayas anaranjadas', 1, 'AGP'),
  (399618, '2015-10-20 00:00:00', 'BRIEFCASE', 'Ivy', 'DARK_GREEN', 'etiqueta de nombre roja', 1, 'AGP'),
  (399619, '2018-01-15 00:00:00', 'SUITCASE', 'Samsonite', 'BLACK', NULL, 1, 'AMS'),
  (399620, '2018-01-15 00:00:00', 'SUITCASE', 'Samsonite', 'GREEN', NULL, 1, 'AMS'),
  (399621, '2018-01-15 00:00:00', 'SUITCASE', '', 'WHITE', '', 0, 'AMS');
/*!40000 ALTER TABLE `luggage`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `luggagelink`
--

DROP TABLE IF EXISTS `luggagelink`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `luggagelink` (
  `tracking_nr`  BIGINT(20) NOT NULL,
  `passenger_id` BIGINT(20) NOT NULL,
  `sent`         TINYINT(4) NOT NULL DEFAULT '0',
  `sent_date`    DATETIME            DEFAULT NULL,
  `deliver_date` DATETIME            DEFAULT NULL,
  `claimed`      TINYINT(4) NOT NULL DEFAULT '0',
  `claimed_date` DATETIME            DEFAULT NULL,
  PRIMARY KEY (`tracking_nr`, `passenger_id`),
  KEY `luggagelink_passengers_passenger_id_fk` (`passenger_id`),
  CONSTRAINT `luggagelink_luggage_tracking_nr_fk` FOREIGN KEY (`tracking_nr`) REFERENCES `luggage` (`tracking_nr`),
  CONSTRAINT `luggagelink_passengers_passenger_id_fk` FOREIGN KEY (`passenger_id`) REFERENCES `passengers` (`passenger_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `luggagelink`
--

LOCK TABLES `luggagelink` WRITE;
/*!40000 ALTER TABLE `luggagelink`
  DISABLE KEYS */;
INSERT INTO `luggagelink` (`tracking_nr`, `passenger_id`, `sent`, `sent_date`, `deliver_date`, `claimed`, `claimed_date`)
VALUES
  (399466, 248179, 1, '2018-01-14 20:33:04', NULL, 0, NULL), (399467, 248180, 0, NULL, NULL, 0, NULL),
  (399468, 248181, 0, NULL, NULL, 0, NULL), (399469, 248182, 0, NULL, NULL, 0, NULL),
  (399470, 248183, 0, NULL, NULL, 0, NULL), (399471, 248184, 0, NULL, NULL, 0, NULL),
  (399472, 248185, 0, NULL, NULL, 0, NULL), (399473, 248186, 0, NULL, NULL, 0, NULL),
  (399474, 248187, 0, NULL, NULL, 0, NULL), (399475, 248188, 0, NULL, NULL, 0, NULL),
  (399476, 248189, 0, NULL, NULL, 0, NULL), (399478, 248190, 0, NULL, NULL, 0, NULL),
  (399479, 248191, 0, NULL, NULL, 1, '2018-01-15 13:40:11'), (399480, 248192, 0, NULL, NULL, 0, NULL),
  (399483, 248193, 0, NULL, NULL, 0, NULL), (399485, 248194, 0, NULL, NULL, 0, NULL),
  (399486, 248195, 0, NULL, NULL, 0, NULL), (399488, 248196, 0, NULL, NULL, 0, NULL),
  (399490, 248197, 0, NULL, NULL, 0, NULL), (399492, 248198, 0, NULL, NULL, 0, NULL),
  (399496, 248199, 0, NULL, NULL, 0, NULL), (399497, 248200, 0, NULL, NULL, 0, NULL),
  (399499, 248201, 0, NULL, NULL, 0, NULL), (399500, 248202, 0, NULL, NULL, 0, NULL),
  (399501, 248203, 0, NULL, NULL, 0, NULL), (399503, 248204, 0, NULL, NULL, 0, NULL),
  (399504, 248205, 0, NULL, NULL, 0, NULL), (399506, 248206, 0, NULL, NULL, 0, NULL),
  (399508, 248207, 0, NULL, NULL, 0, NULL), (399511, 248208, 0, NULL, NULL, 0, NULL),
  (399512, 248209, 0, NULL, NULL, 0, NULL), (399513, 248210, 0, NULL, NULL, 0, NULL),
  (399522, 248220, 0, NULL, NULL, 0, NULL), (399523, 248221, 0, NULL, NULL, 0, NULL),
  (399524, 248222, 0, NULL, NULL, 0, NULL), (399525, 248223, 0, NULL, NULL, 0, NULL),
  (399526, 248224, 0, NULL, NULL, 0, NULL), (399527, 248225, 0, NULL, NULL, 0, NULL),
  (399528, 248226, 0, NULL, NULL, 0, NULL), (399529, 248227, 0, NULL, NULL, 0, NULL),
  (399530, 248228, 0, NULL, NULL, 0, NULL), (399531, 248229, 0, NULL, NULL, 0, NULL),
  (399532, 248230, 0, NULL, NULL, 0, NULL), (399534, 248231, 0, NULL, NULL, 0, NULL),
  (399535, 248232, 0, NULL, NULL, 0, NULL), (399536, 248233, 0, NULL, NULL, 0, NULL),
  (399539, 248234, 0, NULL, NULL, 0, NULL), (399541, 248235, 0, NULL, NULL, 0, NULL),
  (399542, 248236, 0, NULL, NULL, 0, NULL), (399544, 248237, 0, NULL, NULL, 0, NULL),
  (399546, 248238, 0, NULL, NULL, 0, NULL), (399548, 248239, 0, NULL, NULL, 0, NULL),
  (399552, 248240, 0, NULL, NULL, 0, NULL), (399553, 248241, 0, NULL, NULL, 0, NULL),
  (399555, 248242, 0, NULL, NULL, 0, NULL), (399556, 248243, 0, NULL, NULL, 0, NULL),
  (399557, 248244, 0, NULL, NULL, 0, NULL), (399559, 248245, 0, NULL, NULL, 0, NULL),
  (399560, 248246, 0, NULL, NULL, 0, NULL), (399562, 248247, 0, NULL, NULL, 0, NULL),
  (399564, 248248, 0, NULL, NULL, 0, NULL), (399567, 248249, 0, NULL, NULL, 0, NULL),
  (399568, 248250, 0, NULL, NULL, 0, NULL), (399569, 248251, 0, NULL, NULL, 0, NULL),
  (399570, 248252, 0, NULL, NULL, 0, NULL), (399571, 248253, 0, NULL, NULL, 0, NULL),
  (399573, 248254, 0, NULL, NULL, 0, NULL), (399574, 248255, 0, NULL, NULL, 0, NULL),
  (399575, 248256, 0, NULL, NULL, 0, NULL), (399577, 248257, 0, NULL, NULL, 0, NULL),
  (399579, 248258, 0, NULL, NULL, 0, NULL), (399580, 248259, 0, NULL, NULL, 0, NULL),
  (399582, 248260, 0, NULL, NULL, 0, NULL), (399583, 248261, 0, NULL, NULL, 0, NULL),
  (399587, 248262, 0, NULL, NULL, 0, NULL), (399588, 248263, 0, NULL, NULL, 0, NULL),
  (399590, 248264, 0, NULL, NULL, 0, NULL), (399592, 248265, 0, NULL, NULL, 0, NULL),
  (399593, 248266, 0, NULL, NULL, 0, NULL), (399597, 248267, 0, NULL, NULL, 0, NULL),
  (399598, 248268, 0, NULL, NULL, 0, NULL), (399599, 248269, 0, NULL, NULL, 0, NULL),
  (399600, 248270, 0, NULL, NULL, 0, NULL), (399603, 248271, 0, NULL, NULL, 0, NULL),
  (399605, 248272, 0, NULL, NULL, 0, NULL), (399606, 248273, 0, NULL, NULL, 0, NULL),
  (399607, 248274, 0, NULL, NULL, 0, NULL), (399610, 248275, 0, NULL, NULL, 0, NULL),
  (399611, 248276, 0, NULL, NULL, 0, NULL), (399612, 248277, 0, NULL, NULL, 0, NULL),
  (399614, 248278, 0, NULL, NULL, 0, NULL), (399615, 248279, 0, NULL, NULL, 0, NULL),
  (399616, 248280, 0, NULL, NULL, 0, NULL), (399617, 248281, 0, NULL, NULL, 0, NULL),
  (399618, 248282, 0, NULL, NULL, 0, NULL), (399620, 248283, 0, NULL, NULL, 0, NULL),
  (399621, 248284, 0, NULL, NULL, 0, NULL);
/*!40000 ALTER TABLE `luggagelink`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passengers`
--

DROP TABLE IF EXISTS `passengers`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passengers` (
  `passenger_id`  BIGINT(20) NOT NULL AUTO_INCREMENT,
  `first_name`    TEXT,
  `tussenvoegsel` TINYTEXT,
  `last_name`     TEXT,
  `phone_number`  TEXT,
  `email`         TEXT,
  PRIMARY KEY (`passenger_id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 248285
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passengers`
--

LOCK TABLES `passengers` WRITE;
/*!40000 ALTER TABLE `passengers`
  DISABLE KEYS */;
INSERT INTO `passengers` (`passenger_id`, `first_name`, `tussenvoegsel`, `last_name`, `phone_number`, `email`)
VALUES (248179, 'P.', '', 'Curie', NULL, NULL), (248180, 'R.', '', 'Hauer', NULL, NULL),
  (248181, 'M.', '', 'Verstappen', NULL, NULL), (248182, 'S.', '', 'Appelmans', NULL, NULL),
  (248183, 'A.', 'van', 'Buren', NULL, NULL), (248184, 'D.', '', 'Kuyt', NULL, NULL),
  (248185, 'C.', 'van', 'Houten', NULL, NULL), (248186, 'M.', '', 'Messi', NULL, NULL),
  (248187, 'N.', '', 'Bonaparte', NULL, NULL), (248188, 'M.', 'van', 'Basten', NULL, NULL),
  (248189, 'F.', 'van der', 'Elst', NULL, NULL), (248190, 'R.', 'van', 'Persie', NULL, NULL),
  (248191, 'M.', '', 'Rutte', NULL, NULL), (248192, 'W.A.', 'van', 'Buren', NULL, NULL),
  (248193, 'J.', '', 'Verstappen', NULL, NULL), (248194, 'A.', '', 'Gerritse', NULL, NULL),
  (248195, 'D.', 'de', 'Munck', NULL, NULL), (248196, 'R.', 'de', 'Boer', NULL, NULL),
  (248197, 'S.', '', 'Kramer', NULL, NULL), (248198, 'I.', 'de', 'Bruijn', NULL, NULL),
  (248199, 'M.', 'van', 'Buren', NULL, NULL), (248200, 'E.', '', 'Gruyaert', NULL, NULL),
  (248201, 'Mw.', '', 'Hollande', NULL, NULL), (248202, 'G.', '', 'd\'Esting', NULL, NULL),
  (248203, 'F.', 'de', 'Boer', NULL, NULL), (248204, 'Mw.', '', 'Zoetemelk', NULL, NULL),
  (248205, 'F.', '', 'Mitterand', NULL, NULL), (248206, 'L..', '', 'Van', NULL, NULL),
  (248207, 'E.', '', 'Leyers', NULL, NULL), (248208, 'P.', 'van', 'den', NULL, NULL),
  (248209, 'E.', 'de', 'Munck', NULL, NULL), (248210, 'F.', 'van der', 'Sande', NULL, NULL),
  (248220, 'P.', '', 'Curie', NULL, NULL), (248221, 'R.', '', 'Hauer', NULL, NULL),
  (248222, 'M.', '', 'Verstappen', NULL, NULL), (248223, 'S.', '', 'Appelmans', NULL, NULL),
  (248224, 'A.', 'van', 'Buren', NULL, NULL), (248225, 'D.', '', 'Kuyt', NULL, NULL),
  (248226, 'C.', 'van', 'Houten', NULL, NULL), (248227, 'M.', '', 'Messi', NULL, NULL),
  (248228, 'N.', '', 'Bonaparte', NULL, NULL), (248229, 'M.', 'van', 'Basten', NULL, NULL),
  (248230, 'F.', 'van der', 'Elst', NULL, NULL), (248231, 'R.', 'van', 'Persie', NULL, NULL),
  (248232, 'M.', '', 'Rutte', NULL, NULL), (248233, 'W.A.', 'van', 'Buren', NULL, NULL),
  (248234, 'J.', '', 'Verstappen', NULL, NULL), (248235, 'A.', '', 'Gerritse', NULL, NULL),
  (248236, 'D.', 'de', 'Munck', NULL, NULL), (248237, 'R.', 'de', 'Boer', NULL, NULL),
  (248238, 'S.', '', 'Kramer', NULL, NULL), (248239, 'I.', 'de', 'Bruijn', NULL, NULL),
  (248240, 'M.', 'van', 'Buren', NULL, NULL), (248241, 'E.', '', 'Gruyaert', NULL, NULL),
  (248242, 'Mw.', '', 'Hollande', NULL, NULL), (248243, 'G.', '', 'd\'Esting', NULL, NULL),
  (248244, 'F.', 'de', 'Boer', NULL, NULL), (248245, 'Mw.', '', 'Zoetemelk', NULL, NULL),
  (248246, 'F.', '', 'Mitterand', NULL, NULL), (248247, 'L..', '', 'Van', NULL, NULL),
  (248248, 'E.', '', 'Leyers', NULL, NULL), (248249, 'P.', 'van', 'den', NULL, NULL),
  (248250, 'E.', 'de', 'Munck', NULL, NULL), (248251, 'F.', 'van der', 'Sande', NULL, NULL),
  (248252, 'L.', 'van', 'Gaal', NULL, NULL), (248253, 'T.', 'van', 'den', NULL, NULL),
  (248254, 'J.', '', 'Beton', NULL, NULL), (248255, 'A.', '', 'Duval', NULL, NULL),
  (248256, 'W.', '', 'Alberti', NULL, NULL), (248257, 'A.', '', 'Hermans', NULL, NULL),
  (248258, 'J.', 'de', 'Mol', NULL, NULL), (248259, 'F.', 'de', 'Jongh', NULL, NULL),
  (248260, 'G.', '', 'Platini', NULL, NULL), (248261, 'T', '', '.', NULL, NULL),
  (248262, 'L.', 'de', 'Mol', NULL, NULL), (248263, 'K.J.', '', 'Huntelaar', NULL, NULL),
  (248264, 'C.', '', 'Asnavour', NULL, NULL), (248265, 'N.', '', 'Smit-Kroes', NULL, NULL),
  (248266, 'A.', '', 'Pechthold', NULL, NULL), (248267, 'J.A.', '', 'Brink', NULL, NULL),
  (248268, 'Y.', 'van', '\'t', NULL, NULL), (248269, 'A.', '', 'Blaak', NULL, NULL),
  (248270, 'M.', '', 'Platini', NULL, NULL), (248271, 'A.', '', 'Vancourt', NULL, NULL),
  (248272, 'P.', '', 'Vancourt', NULL, NULL), (248273, 'A.', 'van', 'den', NULL, NULL),
  (248274, 'K.', '', 'Delhaize', NULL, NULL), (248275, 'A..', 'van', 'Vleuten', NULL, NULL),
  (248276, 'J.', 'de', 'Bruijn', NULL, NULL), (248277, 'P.', 'van der', 'Elst', NULL, NULL),
  (248278, 'J.', 'van', 'den', NULL, NULL), (248279, 'E.', '', 'deMeyer', NULL, NULL),
  (248280, 'M.', '', 'Duval', NULL, NULL), (248281, 'I..', '', 'Oyens', NULL, NULL),
  (248282, 'P.J.', 'de', 'Leeuw', NULL, NULL), (248283, 'Henk', NULL, 'Johan', NULL, 'Stefan.breetveld@hva.nl'),
  (248284, 'Ruud', '', 'Gullit', '', 'Stefan.breetveld@hva.nl');
/*!40000 ALTER TABLE `passengers`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id`       BIGINT(20)   NOT NULL                               AUTO_INCREMENT,
  `employee_nr`   TEXT,
  `first_name`    TEXT,
  `tussenvoegsel` TINYTEXT,
  `last_name`     TEXT,
  `email`         VARCHAR(255) NOT NULL,
  `password`      TEXT         NOT NULL,
  `airport`       VARCHAR(3)                                          DEFAULT NULL,
  `function`      ENUM ('ADMIN', 'ASSISTANT', 'DAMAGE', 'MANAGEMENT') DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`),
  KEY `users_airports_iata_code_fk` (`airport`),
  CONSTRAINT `users_airports_iata_code_fk` FOREIGN KEY (`airport`) REFERENCES `airports` (`iata_code`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 12
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users`
  DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `employee_nr`, `first_name`, `tussenvoegsel`, `last_name`, `email`, `password`, `airport`, `function`)
VALUES
  (1, 'admin@corendon.nl', 'Henk', NULL, 'Timmerman', 'admin@corendon.nl',
   '32310fea85442ebf59c22ccd42a85b450dabf4bd77b31c04b70393c3193dc8deece77e5606eec411d33f78d42bc91ada192f6e1b6de9c57e2cea40ad19e3e488',
   'AYT', 'ADMIN'),
  (2, '654896168', 'Henk', NULL, 'Smit', 'assistant@corendon.nl',
   '1330df1d8e9955cc0a80aab058e42a60d40c32893544fcc1286907956cc50cef4eff0031929e2585055af3c187e92b578d0aeaf671f1476eba21fa1ea53636eb',
   'AMS', 'ASSISTANT'),
  (3, '235iy2weur', 'Remi', 'de', 'Boer', 'damage@corendon.nl',
   '595e0770c43f51e485c132e47d3cb7751a28fa9cc3bac186179e62c2b4ef955ff1493dcbbecff5f62d0c0a012f987b3ef77418b421dc7e28662b51deed26836a',
   'BJV', 'DAMAGE'),
  (4, 'yRT97QW', 'Karel', 'van', 'Duin', 'management@corendon.nl',
   'fba3368519335dffaee3522cd69108e96f079bc5ca04af09189fbf966070cc5ea6681e51f2fb50bf669180d809e936c2c74fe860eabe261e25b090ef310d0da2',
   'AYT', 'MANAGEMENT');
/*!40000 ALTER TABLE `users`
  ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2018-01-15 21:53:57
