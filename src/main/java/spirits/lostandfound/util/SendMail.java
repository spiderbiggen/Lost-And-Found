/*
 * Class to send a mail
 */
package spirits.lostandfound.util;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.time.LocalDate;
import java.util.Properties;

/**
 * This class sends an email to the given email address.
 *
 * @author IS102-3
 */
public class SendMail {

    /**
     * Send an email to the mailto email address.
     *
     * @param mailTo         An email address
     * @param name           The name of the person that receives this mail.
     * @param currentUser    The name of the user that sends this mail.
     * @param lostAndFoundId The id of the luggage that was registered.
     */
    public SendMail(String mailTo, String name, String currentUser, long lostAndFoundId) {

        //Sending mails from
        //Also authentication from where we sending from
        final String MAILFROM = "CorendonTestSK@outlook.com";
        final String PASSWORD = "Corendon1!";

        //Outlook smtp
        String host = "smtp-mail.outlook.com";

        //Get system properties
        Properties properties = System.getProperties();

        //Set up the mail server
        //Clear authenication for outlook
        properties.put("mail.smtp.host", host);
        properties.setProperty("mail.smtp.user", "CorendonTestSK");
        properties.setProperty("mail.smtp.password", PASSWORD);
        properties.put("mail.smtp.auth", true);
        properties.put("mail.smtp.starttls.enable", true);
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.ssl.trust", "smtp-mail.outlook.com");

        Session session = Session.getInstance(properties,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(MAILFROM, PASSWORD);
                    }
                });

        //Create the message
        try {
            Message message = new MimeMessage(session);
            Multipart multipart = new MimeMultipart();
            MimeBodyPart textBodyPart = new MimeBodyPart();
            MimeBodyPart attachmentBodyPart = new MimeBodyPart();

            //From
            message.setFrom(new InternetAddress(MAILFROM));

            //To
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(mailTo));

            //subject
            message.setSubject("Registered lost lugagge " + lostAndFoundId);

            //Adding PDF attachment
            String filePath = "pdf/" + name + "_RegisteredLuggage_" + LocalDate.now() + ".pdf";
            String fileName = "RegisteredLostLuggagePDF.pdf";
            DataSource source = new FileDataSource(filePath);
            attachmentBodyPart.setDataHandler(new DataHandler(source));
            attachmentBodyPart.setFileName(fileName);

            //Mail message
            textBodyPart.setText("Dear " + name + ", \r\n"
                    + "\r\n"
                    + "Together we registered your luggage as lost. \r\n"
                    + "If we find your luggage, "
                    + "appropriated actions that were agreed upon with you and the assistant will be taken in effect.\r\n\n"
                    + "Your Lost and Found ID is: " + lostAndFoundId + "\r\n\n"
                    + "This mail will also include a pdf containing the information you gave us about your luggage.\r\n"
                    + "\r\n"
                    + "Kind regards, \r\n"
                    + currentUser);

            multipart.addBodyPart(textBodyPart);
            multipart.addBodyPart(attachmentBodyPart);
            message.setContent(multipart);

            Transport.send(message);
            System.out.println("Sent message successfully.");

        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }
}
