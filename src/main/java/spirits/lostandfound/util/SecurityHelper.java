package spirits.lostandfound.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

/**
 * This class provides helper methods to make password storage more secure.
 *
 * @author IS102-3
 */
public final class SecurityHelper {

    /**
     * Empty private Constructor. Static methods only.
     */
    private SecurityHelper() {
        // Empty
    }

    /**
     * Generate a salted password hash based on the eMail of the user.
     *
     * @param password The entered password.
     * @param eMail    The users eMail.
     * @return a salted Hash.
     * @throws NoSuchAlgorithmException thrown when the required algorithm doesn't exist on this computer.
     */
    public static String securePassword(final String password, String eMail) throws NoSuchAlgorithmException {
        byte[] salt = eMail.toLowerCase(Locale.ENGLISH).getBytes();
        return hashPassword(password, salt);
    }

    /**
     * Generate a salted password hash based on the eMail of the user.
     *
     * @param password The entered password.
     * @param salt     The salt
     * @return a salted Hash.
     * @throws NoSuchAlgorithmException thrown when the SHA-512 encryption algorithm doesn't exist on this computer.
     */
    private static String hashPassword(final String password, final byte[] salt) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(salt);
        byte[] bytes = md.digest(password.getBytes());
        StringBuilder sb = new StringBuilder();
        for (byte aByte : bytes) {
            sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
}
