package spirits.lostandfound.controllers.admin;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import spirits.lostandfound.database.SpiritsJDBC;
import spirits.lostandfound.models.User;
import spirits.lostandfound.reference.Airport;
import spirits.lostandfound.reference.Layouts;
import spirits.lostandfound.reference.UserType;
import spirits.lostandfound.util.LayoutHelper;
import spirits.lostandfound.util.SecurityHelper;

import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import static spirits.lostandfound.reference.Strings.EDIT_USER;
import static spirits.lostandfound.reference.Strings.MAIN_MENU;

/**
 * FXML Controller class for editing a user.
 *
 * @author IS102-3
 */
public class EditUserController extends AdminBaseController {


    @FXML
    private TextField firstNameField;
    @FXML
    private TextField tussenvoegselField;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField emailField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private ComboBox<UserType> functionCb;
    @FXML
    private TextField employeeNumberField;
    @FXML
    private ComboBox<Airport> airportCb;
    private User user;

    private ResourceBundle resourceBundle;

    /**
     * Set the user to edit.
     *
     * @param user new user.
     */
    public void setUser(User user) {
        this.user = user;

        firstNameField.setText(user.getFirstName());
        tussenvoegselField.setText(user.getTussenvoegsel());
        lastNameField.setText(user.getLastName());
        emailField.setText(user.getEmail());
        airportCb.setValue(user.getAirport());
        employeeNumberField.setText(user.getEmployeeNr());
        functionCb.setValue(user.getType());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        setBreadCrumbs(resources, MAIN_MENU, getCurrentScreen());
        this.resourceBundle = resources;
        airportCb.setItems(FXCollections.observableArrayList(Airport.values()));
        functionCb.setItems(FXCollections.observableArrayList(UserType.values()));
    }

    /**
     * Save the changes
     *
     * @param event
     */
    @FXML
    private void editAction(ActionEvent event) {
        String password = passwordField.getText();
        String email = emailField.getText();
        String employeeNr = employeeNumberField.getText();
        String firstName = firstNameField.getText();
        String tussenvoegsel = tussenvoegselField.getText();
        String lastName = lastNameField.getText();
        String function = functionCb.getValue().name();
        String airport = airportCb.getValue().getCode();
        long userId = user.getId();
        boolean emailFlag = validateEmail(emailField);
        if (!emailFlag) return;

        try (Connection connection = new SpiritsJDBC().createConnection()) {
                String sql;
                boolean passwordFlag = false;
            if (password.isEmpty() && emailField.getText().equals(user.getEmail())) {
                //language=MySQL
                sql = "UPDATE users SET employee_nr = ?, first_name = ?, tussenvoegsel = ?, last_name = ?, airport = ?, function = ? \n" +
                        "WHERE user_id = ?;";
            } else {
                passwordFlag = validatePassword(passwordField);
                if (passwordFlag) {
                    //language=MySQL
                    sql = "UPDATE users SET employee_nr = ?, first_name = ?, tussenvoegsel = ?, last_name = ?, airport = ?, function = ?, email = ?, password = ? \n" +
                            "WHERE user_id = ?";
                } else {
                    return;
                }
            }
            
            Optional<ButtonType> result = LayoutHelper.createDoubleActionPopup(resourceBundle.getString("popup_edit_user_title"),
                    resourceBundle.getString("popup_edit_user_message"), passwordField.getScene().getWindow());
            if (!result.isPresent() || result.get() != ButtonType.OK) return;
            
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setString(1, employeeNr);
                preparedStatement.setString(2, firstName);
                preparedStatement.setString(3, tussenvoegsel);
                preparedStatement.setString(4, lastName);
                preparedStatement.setString(5, airport);
                preparedStatement.setString(6, function);
                if(passwordFlag) {
                    preparedStatement.setString(7, email);
                    preparedStatement.setString(8, SecurityHelper.securePassword(password, email));
                    preparedStatement.setLong(9, userId);
                } else {
                    preparedStatement.setLong(7, userId);
                }
                
                preparedStatement.executeUpdate();
            }
        } catch (SQLException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        returnToHome();

    }

    /**
     * return to the homescreen of admin
     */
    private void returnToHome() {
        openScreen(employeeNumberField, Layouts.ADMIN_ROLE_MANAGEMENT);
    }

    @Override
    protected String getCurrentScreen() {
        return EDIT_USER;
    }
}
