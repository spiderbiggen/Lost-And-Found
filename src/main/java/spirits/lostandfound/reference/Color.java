package spirits.lostandfound.reference;

import spirits.lostandfound.util.LocalizedString;

import java.util.Arrays;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Represents Colors for the color combo boxes.
 *
 * @author IS102-3
 */
public enum Color {
    STANDARD("Colour"),
    RED("Red", "k\u0131rm\u0131z\u0131", "rojo", "\u0627\u0644\u0644\u0648\u0646"),
    GREEN("Green", "ye\u015Fil", "verde", "\u0623\u062D\u0645\u0631"),
    YELLOW("Yellow", "sar\u0131", "amarillo", "\u0623\u062E\u0636\u0631"),
    BLUE("Blue", "mavi", "azul", "\u0627\u0644\u0623\u0635\u0641\u0631"),
    ORANGE("Orange", "turuncu", "naranja", "\u0623\u0632\u0631\u0642"),
    CREAM("Cream", "krem", "crema", "\u0627\u0644\u0628\u0631\u062A\u0642\u0627\u0644\u064A"),
    BLUE_GREEN("Bluegreen", "\u00E7amurcun", "azul verde", "\u0643\u0631\u064A\u0645"),
    PINK("Pink", "pembe", "rosa", "rosado", "\u0623\u0632\u0631\u0642 \u0623\u062E\u0636\u0631"),
    OLIVE("Olive", "zeytin ye\u015Fili", "verde oliva", "aceituna", "\u0632\u0647\u0631\u064A"),
    BLACK("Black", "siyah", "zenci", "negro", "\u0632\u064A\u062A\u0648\u0646"),
    VIOLET("Violet", "menek\u015Fe", "violeta", "\u0623\u0633\u0648\u062F"),
    LIGHT_BROWN("Lightbrown", "a\u00E7\u0131k kahverengi", "marr\u00F3n claro", "\u0627\u0644\u0628\u0646\u0641\u0633\u062C\u064A"),
    PURPLE("Purple", "mor", "morado", "p\u00FArpura", "\u0627\u0644\u0628\u0646\u064A \u0627\u0644\u0641\u0627\u062A\u062D"),
    GRAY("Gray", "gri", "gris", "\u0623\u0631\u062C\u0648\u0627\u0646\u064A"),
    WHITE("White", "beyaz", "blanco", "\u0627\u0644\u0644\u0648\u0646 \u0627\u0644\u0631\u0645\u0627\u062F\u064A"),
    BROWN("Brown", "kahverengi", "kestane", "marr\u00F3n", "\u0623\u0628\u064A\u0636"),
    DARK_BLUE("Darkblue", "koyu mavi", "azul oscuro", "\u0628\u0646\u0649"),
    LIGHT_GREEN("Lightgreen", "a\u00E7\u0131k ye\u015Fil", "verde claro", "\u0623\u0632\u0631\u0642 \u063A\u0627\u0645\u0642"),
    LIGHT_BLUE("Lightblue", "a\u00E7\u0131k mavi", "azul claro", "\u0627\u062E\u0636\u0631 \u0641\u0627\u062A\u062D"),
    DARK_BROWN("Darkbrown", "koyu kahverengi", "marr\u00F3n oscuro", "\u0627\u0644\u0636\u0648\u0621 \u0627\u0644\u0623\u0632\u0631\u0642"),
    DARK_RED("Darkred", "koyu k\u0131rm\u0131z\u0131", "rojo oscuro", "\u0628\u0646\u064A \u063A\u0627\u0645\u0642"),
    DARK_GRAY("Darkgray", "koyu gri", "gris oscuro", "\u0627\u062D\u0645\u0631 \u063A\u0627\u0645\u0642"),
    LIGHT_GRAY("Lightgray", "a\u00E7\u0131k gri", "gris claro", "\u0627\u0644\u0631\u0645\u0627\u062F\u064A \u0627\u0644\u062F\u0627\u0643\u0646"),
    DARK_GREEN("Darkgreen", "koyu ye\u015Fil", "verde oscuro", "\u0631\u0645\u0627\u062F\u064A \u0641\u0627\u062A\u062D");

    private final LocalizedString color;
    private final String[] localNames;

    /**
     * Enum constructor.
     *
     * @param color Default unlocalized name.
     */
    Color(String... color) {
        this.color = new LocalizedString("color_" + name(), color[0]);
        this.localNames = color;
    }

    /**
     * Translate all colors to the new Locale.
     *
     * @param resourceBundle The resource bundle that has all the strings for the given locale.
     */
    public static void setResourceBundle(ResourceBundle resourceBundle) {
        Arrays.stream(values()).forEach(color -> color.color.setResourceBundle(resourceBundle));
    }

    /**
     * Converts a String to a Color if possible defaults to Standard color.
     *
     * @param string A string that represents a color
     * @return A {@link Color} object.
     */
    public static Color fromString(final String string) {
        Optional<Color> optionalColor = Arrays.stream(values()).filter(color -> Arrays.stream(color.localNames)
                .anyMatch(s -> s.equalsIgnoreCase(string))).findFirst();
        Color color = Color.STANDARD;
        if (optionalColor.isPresent()) {
            color = optionalColor.get();
        }
        return color;
    }

    @Override
    public String toString() {
        return color.toString();
    }
}
